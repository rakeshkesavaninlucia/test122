package org.birlasoft.thirdeye.constant;


/**
 * Interface for declare {@code Questionnaire constants} 
 */
public interface QuestionnaireConstants {
	
	public static final String QUESTIONNAIRE_TEMPLATE_URL = "questionnaire/questionnaireTemplate";
	public static final String QUESTIONNAIRE_ASSET_TYPES = "assetTypes";
	public static final String QUESTIONNAIRE_TYPES = "questionnaireType";
	public static final String QUESTIONNAIRE_ACTION = "action";
	public static final String QUESTIONNAIRE_ID = "questionnaireId";
	public static final String PARENT_PARAM = "parentParam";

	public static final String USER_WORKSPACE_LIST = "userWorkspaceList";
}
