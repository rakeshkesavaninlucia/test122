package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for asset template column.
 */
public interface AssetTemplateColumnRepository extends JpaRepository<AssetTemplateColumn, Serializable> {
	
	@EntityGraph(value = "AssetTemplateColumn.assetDatas", type = EntityGraphType.LOAD)
	public AssetTemplateColumn findById(Integer idOfTemplateColumn);
	
	@EntityGraph(value = "AssetTemplateColumn.assetTemplate", type = EntityGraphType.LOAD)
	@Query("SELECT atc FROM AssetTemplateColumn atc WHERE atc.id = (:id)")
	public AssetTemplateColumn findOneLoadedAssetTemplate(@Param("id") Integer idOfTemplateColumn);
	public List<AssetTemplateColumn> findByAssetTemplate(AssetTemplate assetTemplate);
}
