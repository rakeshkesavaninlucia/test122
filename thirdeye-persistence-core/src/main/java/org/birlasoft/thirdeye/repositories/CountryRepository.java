package org.birlasoft.thirdeye.repositories;



import java.io.Serializable;

import org.birlasoft.thirdeye.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Serializable>{

	
	public Country findBycountryName(String CountryName);
} 