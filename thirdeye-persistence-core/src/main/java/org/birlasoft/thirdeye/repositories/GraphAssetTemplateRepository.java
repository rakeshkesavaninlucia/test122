package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author samar.gupta
 *
 */
public interface GraphAssetTemplateRepository extends JpaRepository<GraphAssetTemplate, Serializable> {
	/**
	 * Find By graph.
	 * @param graph
	 * @return {@code List<GraphAssetTemplate>}
	 */
	public List<GraphAssetTemplate> findByGraph(Graph graph);	
	/**
	 * find list of GraphAssetTemplate object by asset template.
	 * @param assetTemplate
	 * @return {@code List<GraphAssetTemplate>}
	 */
	public List<GraphAssetTemplate> findByAssetTemplate(AssetTemplate assetTemplate);

}
