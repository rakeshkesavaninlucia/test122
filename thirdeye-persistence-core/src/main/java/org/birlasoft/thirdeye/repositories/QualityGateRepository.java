package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QualityGateRepository extends JpaRepository<QualityGate, Serializable> {
	
	public QualityGate findByNameAndWorkspace(String name, Workspace workspace);
	
	public List<QualityGate> findByWorkspace(Workspace workspace);

}
