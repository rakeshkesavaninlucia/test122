package org.birlasoft.thirdeye.repositories;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for questionnaire question.
 */
public interface QuestionnaireQuestionRepository extends JpaRepository<QuestionnaireQuestion, Integer> {
	/**
	 * Find list of QQ by questionnaire.
	 * @param questionnaire
	 * @param loaded
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	public List<QuestionnaireQuestion> findByQuestionnaire (Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:qe)")
	public List<QuestionnaireQuestion> findByQuestionnaireLoaded (@Param("qe") Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaireParameter = (:qp)")
	public List<QuestionnaireQuestion> findByQuestionnaireParameterLoaded (@Param("qp") QuestionnaireParameter questionnaireParameter);
	
	/**
	 * Find list of QQ by parameter and question and questionnaire.
	 * @param parameter
	 * @param question
	 * @param questionnaire
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	
	@Query("SELECT qq FROM QuestionnaireQuestion qq LEFT JOIN FETCH qq.questionnaireAsset qa LEFT JOIN FETCH qq.questionnaireParameter left join fetch qq.question WHERE qq.questionnaire = (:questionnaire) and qq.question = (:question) and qq.questionnaireParameter =(:qp)")
	public List<QuestionnaireQuestion> findByQuestionnaireParameterAndQuestionAndQuestionnaire(@Param("qp") QuestionnaireParameter questionnaireParameter, @Param("question") Question question, @Param("questionnaire") Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:questionnaire) and qq.question = (:question) and qq.questionnaireParameter =(:qp)")
	public List<QuestionnaireQuestion> findByQuestionnaireParameterAndQuestionAndQuestionnaireLoaded(@Param("qp") QuestionnaireParameter questionnaireParameter,@Param("question") Question question, @Param("questionnaire") Questionnaire questionnaire);
	
	@Query("SELECT qq FROM QuestionnaireQuestion qq LEFT JOIN FETCH qq.questionnaireAsset qa LEFT JOIN FETCH qq.questionnaireParameter left join fetch qq.question WHERE qq.questionnaire = (:questionnaire) and qq.question = (:question)")
	public List<QuestionnaireQuestion> findByQuestionAndQuestionnaire(@Param("question") Question question, @Param("questionnaire") Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq LEFT JOIN FETCH qq.questionnaireAsset qa LEFT JOIN FETCH qq.questionnaireParameter left join fetch qq.question WHERE qq.questionnaire = (:questionnaire) and qq.question = (:question)")
	public List<QuestionnaireQuestion> findByQuestionAndQuestionnaireLoaded(@Param("question") Question question, @Param("questionnaire") Questionnaire questionnaire);
	
	@Query("SELECT qq FROM QuestionnaireQuestion qq JOIN FETCH qq.questionnaireAsset WHERE qq.questionnaire = (:questionnaire) and qq.questionnaireParameter =(:qp)")
	public void findByQuestionnaireParameterAndQuestionnaire(@Param("qp") QuestionnaireParameter questionnaireParameter, @Param("questionnaire") Questionnaire questionnaire);
	
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq = (:qq)")
	@EntityGraph(value = "QuestionnaireQuestion.responseDatas", type = EntityGraphType.LOAD)
	public Set<QuestionnaireQuestion> loadAllReponses(@Param("qq") QuestionnaireQuestion listToReturn);

	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.question = (:question) and qq.questionnaireParameter =(:qp)")
	public List<QuestionnaireQuestion> findByQuestionnaireParameterAndQuestionLoaded(@Param("qp") QuestionnaireParameter questionnaireParameter, @Param("question") Question question);
	
	public Set<QuestionnaireQuestion> findByQuestionnaireAssetIn(Set<QuestionnaireAsset> setOfQA);
	
	public Set<QuestionnaireQuestion> findByQuestionnaireParameterIn(Set<QuestionnaireParameter> setOfQP);
	
	/**
	 * find by Questionnaire Loaded ResponseDatas.
	 * @param qe
	 * @return {@code List<QuestionnaireQuestion>}
	 */
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:qe)")
	@EntityGraph(value = "QuestionnaireQuestion.responseDatas", type = EntityGraphType.LOAD)
	public List<QuestionnaireQuestion> findByQuestionnaireLoadedResponseDatas(@Param("qe") Questionnaire qe);

	public Set<QuestionnaireQuestion> findByQuestionnaireAndQuestionnaireParameterIn(Questionnaire questionnaire,List<QuestionnaireParameter> questionnaireParameters);
	
	/**
	 * @param questionnaireAsset
	 * @param question
	 * @param questionnaire
	 * @param questionnaireParameter
	 * @return
	 */
	public Set<QuestionnaireQuestion> findByQuestionnaireAssetAndQuestionAndQuestionnaireAndQuestionnaireParameter(QuestionnaireAsset questionnaireAsset, Question question, Questionnaire questionnaire,QuestionnaireParameter questionnaireParameter);
	
	/** Find {@link QuestionnaireQuestion} based on {@code Questionnaire, QuestionnaireAsset, QuestionnaireParameter}
	 * @param questionnaire
	 * @param question
	 * @param questionnaireAsset
	 * @param questionnaireParameter
	 * @return
	 */
	public QuestionnaireQuestion findByQuestionnaireAndQuestionAndQuestionnaireAssetAndQuestionnaireParameterIn(Questionnaire questionnaire,
			Question question, QuestionnaireAsset questionnaireAsset, List<QuestionnaireParameter> questionnaireParameter);
	/**
	 * Fetch all QQs by QPs and QAs.
	 * @param listOfQp
	 * @param listOfQa
	 * @return {@code Set<QuestionnaireQuestion>}
	 */
	@Query("SELECT qq FROM QuestionnaireQuestion qq LEFT JOIN FETCH qq.questionnaireAsset qa WHERE qq.questionnaireParameter in (:qps) and qq.questionnaireAsset in (:qas)")
	public Set<QuestionnaireQuestion> findByQuestionnaireParameterInAndQuestionnaireAssetIn(@Param("qps") List<QuestionnaireParameter> listOfQp, @Param("qas") Set<QuestionnaireAsset> listOfQa);
	
	
	
	/**@author dhruv.sood
	 * @param question
	 * @param questionnaireParameter
	 * @param questionnaireAsset
	 * @return
	 */
	public QuestionnaireQuestion findByQuestionAndQuestionnaireParameterAndQuestionnaireAsset(Question question, QuestionnaireParameter questionnaireParameter,	QuestionnaireAsset questionnaireAsset);

	/**Find QQ by qe, qa
	 * @author dhruv.sood
	 * @param question	
	 * @param questionnaireAsset
	 * @return
	 */
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:qe) and qq.questionnaireAsset = (:qas)")
	public Set<QuestionnaireQuestion> findByQuestionnaireAndQuestionnaireAsset(@Param("qe")Questionnaire questionnaire,@Param("qas") QuestionnaireAsset questionnaireAsset);
	
	/**@author 
	 * Fetch QQ by questionnaireId loaded excluding duplicate questionID per QuestionnaireAssetID
	 * @param questionnaire
	 * @return
	 */
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:qe) group by qq.question, qq.questionnaireAsset")
	public List<QuestionnaireQuestion> findByQuestionnaireIdloaded (@Param("qe") Questionnaire questionnaire);
	
	/**@author 
	 * Fetch QQ by questionnaireId excluding duplicate questionID per QuestionnaireAssetID
	 * @param questionnaire
	 * @return
	 */
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.questionnaire = (:qe) group by qq.question, qq.questionnaireAsset")
	public List<QuestionnaireQuestion> findByQuestionnaireId (@Param("qe") Questionnaire questionnaire);
	

	/**@author 
	 * Fetch QQ by questionId and QuestionnaireAssetId 
	 * @param question
	 * @param questionnaireAsset
	 * @return
	 */
	@EntityGraph(value = "QuestionnaireQuestion.fullyLoaded", type = EntityGraphType.LOAD)
	@Query("SELECT qq FROM QuestionnaireQuestion qq WHERE qq.question = (:ques) and qq.questionnaireAsset = (:qa)")
	public List<QuestionnaireQuestion> findByQuestionIdAndQuestionnaireAssetId (@Param("ques") Question question, @Param("qa") QuestionnaireAsset questionnaireasset);
	
}
