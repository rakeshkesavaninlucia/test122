package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for response.
 */
public interface ResponseRepository extends JpaRepository<Response, Serializable> {
	/**
	 * Find list of response by questionnaire.
	 * @param questionnaire
	 * @return {@code List<Response>}
	 */
	public List<Response> findByQuestionnaire(Questionnaire qe);
	public List<Response> findByQuestionnaireIn(List<Questionnaire> qes);
	
	@EntityGraph(value = "Response.responseDatas", type = EntityGraphType.LOAD)
	@Query("select r from Response r WHERE r.id = (:id)")
	public Response findByIdLoadedResponseDatas(@Param("id") Integer id);
}
