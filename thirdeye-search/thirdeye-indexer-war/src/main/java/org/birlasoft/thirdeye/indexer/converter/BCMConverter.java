package org.birlasoft.thirdeye.indexer.converter;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.constant.ElasticSearchTags;
import org.birlasoft.thirdeye.search.api.constant.IndexBCMAssetValueTags;
import org.birlasoft.thirdeye.search.api.constant.IndexBCMLevelTags;
import org.birlasoft.thirdeye.search.api.constant.IndexBCMParameterTags;
import org.birlasoft.thirdeye.search.api.constant.IndexBCMTags;
import org.birlasoft.thirdeye.search.api.constant.IndexDataTags;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**Converter class for BCM mapping
 * @author dhruv.sood
 *
 */
@Component
public class BCMConverter implements JSONConverter<IndexBCMBean>{

	private static Logger logger = LoggerFactory.getLogger(BCMConverter.class);
	
	@Override
	public Optional<XContentBuilder> convertForWrite(IndexBCMBean oneBCM) {
		
		try {
			XContentBuilder contentBuilder = jsonBuilder().startObject();
					
			contentBuilder.field(ElasticSearchTags.WORKSPACE.getTagKey(), oneBCM.getWorkspaceId());
			
			// Build parameter list for the index
			// Build the quality gates for the index
			// Any other date for update / putting into the system.
			
			contentBuilder.endObject();
			
			return Optional.of(contentBuilder);
			
		} catch (IOException e) {
			logger.error("Error in BCMConverter.java :: convertForWrite "+e);
		}
		
		return Optional.empty();
	}
	
	@Override
	public XContentBuilder generateMappingForObject(IndexBCMBean t) {
		// This method is to given the JSon so that you can 
		// create the put mapping into the BCM type index.
		
		XContentBuilder contentBuilder = null;
		try {
			contentBuilder = jsonBuilder().startObject();
			contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());
			
			contentBuilder.startObject(ElasticSearchTags.WORKSPACE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
			
			logger.info("Creating BCM mapping");
			indexBCMMapping(contentBuilder);
		
			contentBuilder.endObject();
			contentBuilder.endObject();
			logger.info("Creating new mapping : " + contentBuilder.string());
		} catch (IOException e) {
			logger.error("Unable to create the mapping JSON for the asset " , e);
			return null;
		}	
		
		return contentBuilder;
	}

	/**Mapping for BCM Bean
	 * @author dhruv.sood
	 * @param contentBuilder
	 * @throws IOException
	 */
	private void indexBCMMapping(XContentBuilder contentBuilder) throws IOException {
		
		contentBuilder.startObject(IndexBCMTags.BCMID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMTags.BCMNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		
		logger.info("Creating BCM Level mapping");
		indexBCMLevelMapping(contentBuilder);
	}

	/**Mapping for BCM Level Bean
	 * @author dhruv.sood
	 * @param contentBuilder
	 * @throws IOException
	 */
	private void indexBCMLevelMapping(XContentBuilder contentBuilder) throws IOException {
		
		contentBuilder.startObject(IndexBCMTags.BCMLEVELS.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());		
		contentBuilder.startObject(IndexBCMLevelTags.BCMLEVELID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.BCMLEVELNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.LEVELNUMBER.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.PARENTBCMLEVELID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.CATEGORY.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.DEFAULTBCMCOLOR.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMLevelTags.SEQUENCENUMBER.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		
		logger.info("Creating BCM Parameter Mapping");		
		indexBCMParameters(contentBuilder);	
		
		contentBuilder.endObject();
		contentBuilder.endObject();
	}
	
	/**Mapping for BCM Parameter Bean
	 * @author dhruv.sood
	 * @param contentBuilder
	 * @throws IOException
	 */
	private void indexBCMParameters(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexBCMLevelTags.BCMEVALUATEDVALUE.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());		
		contentBuilder.startObject(IndexBCMParameterTags.PARAMETERID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMParameterTags.QUESTIONNAIREID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMParameterTags.DISPLAYNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMParameterTags.UNIQUENAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMParameterTags.TYPE.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).endObject();
		
		logger.info("Creating BCM Asset Mapping");
		indexBCMAssets(contentBuilder);
		
		contentBuilder.endObject();
		contentBuilder.endObject();
	}

	/**Mapping for BCM Asset Bean
	 * @author dhruv.sood
	 * @param contentBuilder
	 * @throws IOException
	 */
	private void indexBCMAssets(XContentBuilder contentBuilder) throws IOException {
		contentBuilder.startObject(IndexBCMParameterTags.VALUES.getTagKey());
		contentBuilder.field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.NESTED.getTagKey());
		contentBuilder.startObject(IndexDataTags.PROPERTIES.getTagKey());		
		contentBuilder.startObject(IndexBCMAssetValueTags.ASSETID.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.LONG.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMAssetValueTags.DISPLAYNAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.startObject(IndexBCMAssetValueTags.UNIQUENAME.getTagKey()).field(IndexDataTags.TYPE.getTagKey(),IndexDataTags.STRING.getTagKey()).field(IndexDataTags.INDEX.getTagKey(),IndexDataTags.NOT_ANALYZED.getTagKey()).endObject();
		contentBuilder.endObject();
		contentBuilder.endObject();
	}

	@Override
	public Optional<List<IndexBCMBean>> readFromResponse(SearchResponse searchResponse) {
		// TODO Auto-generated method stub
		return null;
	}
}
