package org.birlasoft.thirdeye.indexer.service.impl;



import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.birlasoft.thirdeye.beans.index.IndexRequestBean;
import org.birlasoft.thirdeye.beans.index.IndexResponseBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableScheduling
public class ThirdEyeIndexSchedular {
	
	private static Logger logger = LoggerFactory.getLogger(ThirdEyeIndexSchedular.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	@Autowired
	private TenantRepository tenantRepository;
		
	@Autowired
	private Environment env;	
	
	private Map<String, DataSource> map;
	
	
	Connection conn ;
	Statement stmt ;
	ResultSet rs ;
	List<String> wsids;
	int i=0 ; 
	
	
	
	@Scheduled(cron = "0 0 0 1/1 * ?")
	public void executeindex()  throws PropertyVetoException
	{	
		
		try
		{
				logger.info("Start Of Thirdeye Index Schedular Job");
				
				map = new HashMap<>();
				for (Tenant tenant : tenantRepository.findAll())
				{
					
					ComboPooledDataSource dataSource = new ComboPooledDataSource();
					dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
					dataSource.setJdbcUrl(tenant.getTenantDbUrl() + "?noAccessToProcedureBodies=true");
					dataSource.setUser(tenant.getTenantDbUserName());
					dataSource.setPassword(tenant.getTenantDbPassword());
					dataSource.setPreferredTestQuery("SELECT 1");
					dataSource.setTestConnectionOnCheckout(true);
					map.put(tenant.getTenantUrlId(), dataSource);
					
			
				}
		
				// iterate through map and get the workspaceid's 
				
		
			for(Map.Entry<String, DataSource> entry : map.entrySet())
			{
								
				conn =  entry.getValue().getConnection() ;				
				stmt =  conn.createStatement();
				wsids = new ArrayList<>();
				
				//logic to index
				
				rs  = stmt.executeQuery("Select id from workspace") ;
				
				while ( rs.next())
				{
						wsids.add( rs.getString(1));
						i++ ;
				}
			
				String uri = elasticSearchHost + "thirdeye-search-web/api/buildFullIndex/tenant/{tenantURL}";
				String tenantURL=entry.getKey();
								
				Map<String, String> uriParams = new HashMap<>();
				uriParams.put("tenantURL", tenantURL);
				
			    IndexRequestBean requestBean = new IndexRequestBean();
			    requestBean.setTenantURL(tenantURL);
			    requestBean.setWsIds(wsids.toArray(new String[wsids.size()]));
			    
			    RestTemplate restTemplate = new RestTemplate(); 
			    IndexResponseBean responseBean = restTemplate.postForObject(uri,requestBean,IndexResponseBean.class,uriParams);		    
			    logger.info(responseBean.getMessage());	
			    logger.info("Index complete for tenant " + tenantURL);
			    conn.close();
			}
			
				
		}
		catch( Exception ex )
		{
			logger.error("Exception " + ex) ;
		}
		
		logger.info("End of ThirdEye index Schedular Job.." );
	}
	
		

}
