package org.birlasoft.thirdeye.search.api.beans;

import java.util.Set;

public class AssetClassSearchBean {
	
	private String name;
	private long count;
	private Set<ParameterSearchBean> parameters;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getCount() {
		return count;
	}
	
	public void setCount(long l) {
		this.count = l;
	}
	
	public Set<ParameterSearchBean> getParameters() {
		return parameters;
	}
	
	public void setParameters(Set<ParameterSearchBean> parameters) {
		this.parameters = parameters;
	}

}
