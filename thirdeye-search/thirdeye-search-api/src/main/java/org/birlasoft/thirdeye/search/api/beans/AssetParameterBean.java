package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class AssetParameterBean {
	
	private String assetName;
	private BigDecimal parameterValue;
	private Integer assetId;
	private String color;
	private String description;
	
	public String getAssetName() {
		return assetName;
	}
	
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	
	public BigDecimal getParameterValue() {
		return parameterValue;
	}
	
	public void setParameterValue(BigDecimal parameterValue) {
		this.parameterValue = parameterValue;
	}
	
	public Integer getAssetId() {
		return assetId;
	}
	
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssetParameterBean other = (AssetParameterBean) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		return true;
	}

}
