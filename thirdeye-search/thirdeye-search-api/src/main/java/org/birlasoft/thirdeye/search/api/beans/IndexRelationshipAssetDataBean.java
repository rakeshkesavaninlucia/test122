package org.birlasoft.thirdeye.search.api.beans;

/**
 * Bean class for asset relationship data indexing
 * 
 * @author samar.gupta
 *
 */
public class IndexRelationshipAssetDataBean {

	private String relationshipName;
	private String dataType;
	private String frequency;
	private String relationshipDisplayName;
	private String direction;

	public String getRelationshipName() {
		return relationshipName;
	}

	public void setRelationshipName(String relationshipName) {
		this.relationshipName = relationshipName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getRelationshipDisplayName() {
		return relationshipDisplayName;
	}

	public void setRelationshipDisplayName(String relationshipDisplayName) {
		this.relationshipDisplayName = relationshipDisplayName;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
}
