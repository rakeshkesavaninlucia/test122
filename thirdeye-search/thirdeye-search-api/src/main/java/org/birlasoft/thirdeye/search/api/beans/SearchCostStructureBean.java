package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class SearchCostStructureBean {

	private Integer id;
	private String name;
	private BigDecimal aggregate;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public BigDecimal getAggregate() {
		return aggregate;
	}
	
	public void setAggregate(BigDecimal aggregate) {
		this.aggregate = aggregate;
	}	
}
