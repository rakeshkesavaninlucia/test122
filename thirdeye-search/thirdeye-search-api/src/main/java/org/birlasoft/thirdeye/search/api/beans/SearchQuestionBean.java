package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class SearchQuestionBean {
	
	private Integer id;
	private String name;
	private Integer questionnaireParameterId;
	private Integer questionnaireId;
	private BigDecimal value;
	private String color;
	private String colorDesc;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getQuestionnaireParameterId() {
		return questionnaireParameterId;
	}
	public void setQuestionnaireParameterId(Integer questionnaireParameterId) {
		this.questionnaireParameterId = questionnaireParameterId;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getColorDesc() {
		return colorDesc;
	}
	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

}
