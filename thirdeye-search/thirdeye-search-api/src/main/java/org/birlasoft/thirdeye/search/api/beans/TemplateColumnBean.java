package org.birlasoft.thirdeye.search.api.beans;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.DataType;


/**
 * 
 * @author tej.sarup
 *
 */
public class TemplateColumnBean <T> implements Sequenced {
	
	private String name;
	private T value;
	private DataType columnDataType;
	
	private int sequenceNumber;
	private boolean filterable;

	public TemplateColumnBean() {}
	
	public TemplateColumnBean(String name, T value,
			DataType columnDataType, int sequenceNumber) {
		super();
		this.name = name;
		this.value = value;
		this.columnDataType = columnDataType;
		this.sequenceNumber = sequenceNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public DataType getColumnDataType() {
		return columnDataType;
	}

	public void setColumnDataType(DataType columnDataType) {
		this.columnDataType = columnDataType;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public boolean isFilterable() {
		return filterable;
	}

	public void setFilterable(boolean filterable) {
		this.filterable = filterable;
	}	
}
