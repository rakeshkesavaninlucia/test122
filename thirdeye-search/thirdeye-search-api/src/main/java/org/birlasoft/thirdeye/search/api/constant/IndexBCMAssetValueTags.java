package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author dhruv.sood
 *
 */
public enum IndexBCMAssetValueTags {

	ASSETID("assetId"),	
	DISPLAYNAME("shortName"),
	UNIQUENAME("evaluatedPercentage");
	

	String tagKey;

	IndexBCMAssetValueTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}	
}