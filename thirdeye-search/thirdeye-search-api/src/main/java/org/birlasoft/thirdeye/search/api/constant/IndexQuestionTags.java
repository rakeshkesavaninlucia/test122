package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexQuestionTags {

	ID("id"),
	DISPLAYNAME("displayName"),
	UNIQUENAME("uniqueName"),
	VALUES("values");	
	
	String tagKey;
	
	IndexQuestionTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
