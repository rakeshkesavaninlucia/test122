package org.birlasoft.thirdeye.search.controller;

import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.HomeSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeSearchController extends BaseSearchController {
	
	@Autowired
	private HomeSearchService homeSearchService;
	
	@RequestMapping( value = "/home/parameter", method = RequestMethod.POST )
	public ParameterSearchBean searchHome(@RequestBody SearchConfig searchConfig){		
		return homeSearchService.getParameterAggregateValue(searchConfig);
	}
	
	@RequestMapping( value = "/param/click", method = RequestMethod.POST )
	public AssetParameterWrapper clickThroughHome(@RequestBody SearchConfig searchConfig){
		return homeSearchService.getAssetValueForParameter(searchConfig);
	}
	
	@RequestMapping( value = "/assetClass", method = RequestMethod.POST )
	public AssetClassSearchBean getAssetClassDetails(@RequestBody SearchConfig searchConfig){
		return homeSearchService.getAssetClassDetailsForHomeScreen(searchConfig);
	}
	
}
