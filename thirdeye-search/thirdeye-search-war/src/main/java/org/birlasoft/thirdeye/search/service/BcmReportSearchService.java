package org.birlasoft.thirdeye.search.service;

import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

/**
 * @author samar.gupta
 *
 */
public interface BcmReportSearchService {

	/**
	 * Get index bcm bean by bcm.
	 * @param searchConfig
	 * @return {@code IndexBCMBean}
	 */
	public IndexBCMBean getBcmLevels(SearchConfig searchConfig);

}
