package org.birlasoft.thirdeye.search.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.AssetRelationshipSearchService;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service implementation class for asset relationship from elastic search.
 * @author samar.gupta
 *
 */
@Service
public class AssetRelationshipSearchServiceImpl implements AssetRelationshipSearchService {
	
	@Autowired
	private Client client;
	@Autowired
	private FilterSearchService filterSearchService;
	
	@Override
	public List<IndexAssetBean> getAssetsRelationship(SearchConfig searchConfig) {
		List<IndexAssetBean> indexAssetParentChildBean = new ArrayList<>();
		int size = 10;
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getAssets(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getAssets(searchConfig, tenant, size);
			}

			indexAssetParentChildBean = extractIndexAssetsParentChildBean(response);
		}
		return indexAssetParentChildBean;
	}

	private SearchResponse getAssets(SearchConfig searchConfig, String tenant, int size) {
		QueryBuilder postFilterQuery = QueryBuilders.boolQuery()
				.must(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace()));
		if (!searchConfig.getFilterMap().isEmpty()) {
			postFilterQuery = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap()).must(postFilterQuery);
		}
		
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.termsQuery("id", searchConfig.getListOfIds()))
				.setPostFilter(postFilterQuery)
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}
	
	private List<IndexAssetBean> extractIndexAssetsParentChildBean(SearchResponse response) {
		List<IndexAssetBean> listOfAssets = new ArrayList<>();

		for (SearchHit oneHit : response.getHits()) {
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			listOfAssets.add(indexAssetBean);
		}
		return listOfAssets;
	}
}
