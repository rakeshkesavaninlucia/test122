package org.birlasoft.thirdeye.search.service.impl;

import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.constant.IndexBCMTags;
import org.birlasoft.thirdeye.search.service.BcmReportSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service implementation class for Bcm reporting from elastic search.
 * @author samar.gupta
 *
 */
@Service
public class BcmReportSearchServiceImpl implements BcmReportSearchService {
	
	@Autowired
	private Client client;

	@Override
	public IndexBCMBean getBcmLevels(SearchConfig searchConfig) {
		IndexBCMBean bcmLevelBeans = new IndexBCMBean();
		int size = 10;
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getBcmLevels(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getBcmLevels(searchConfig, tenant, size);
			}

			bcmLevelBeans = extractIndexBCMLevelBean(response);
		}
		return bcmLevelBeans;
	}

	private SearchResponse getBcmLevels(SearchConfig searchConfig, String tenant, int size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.termQuery(IndexBCMTags.BCMID.getTagKey(), searchConfig.getBcmId()))
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}
	
	private IndexBCMBean extractIndexBCMLevelBean(SearchResponse response) {
		IndexBCMBean indexBCMBean = new IndexBCMBean();

		if (response.getHits() != null && response.getHits().getTotalHits() == 1) {
			indexBCMBean = Utility.convertJSONStringToObject(response.getHits().getAt(0).getSourceAsString(), IndexBCMBean.class);
		}
		return indexBCMBean;
	}
}
