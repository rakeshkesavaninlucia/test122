/**
 * 
 */
package org.birlasoft.thirdeye.search.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.service.impl.DataSourceBasedMultiTenantConnectionProviderImpl;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * The tenant identifier resolver is called by <put link>.
 * 
 * This will happen only in those classes which are marked with the {@link TransactionManagers#TENANTTRANSACTIONMANAGER}.
 * 
 * The purpose of this class is to identify the tenant database which the repository classes will hit.
 * 
 * This is done in two ways:
 * <ol>
 * 	<li>Extract it from the authentication details that are stored in the {@link SecurityContextHolder#getContext()} as a part of the user session.</li>
 * 	<li> From the request attribute - this is only done in the case of the login process where the request attribute is set in
 *   {@link CustomDetailsFilter} which is only during the login process and is before spring security fires up</li>
 * </ol>
 * 
 * 
 * @author shaishav.dixit
 *
 */
@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

	/**
	 * If this method returns {@link DataSourceBasedMultiTenantConnectionProviderImpl#DEFAULT_TENANT_ID} you will get
	 * a sure shot Null pointer. 
	 */
	@Override
	public String resolveCurrentTenantIdentifier() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes != null) {
			String identifier = (String) requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER",RequestAttributes.SCOPE_REQUEST);
			if (identifier != null) {
				return identifier;
			}
		}
		return DataSourceBasedMultiTenantConnectionProviderImpl.DEFAULT_TENANT_ID;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}

}
