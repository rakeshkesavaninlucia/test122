/**
 * 
 */
package org.birlasoft.thirdeye.search.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.FacetBean;
import org.birlasoft.thirdeye.search.api.beans.FilterBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.search.api.constant.ElasticSearchTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shaishav.dixit
 *
 */
@Service
public class FilterSearchServiceImpl implements FilterSearchService {
	
	@Autowired
	private Client client;

	@Override
	public List<FacetBean> getFacetAndFilter(SearchConfig searchConfig) {
		int size = 10;
		List<FacetBean> facetBeans = new ArrayList<>();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForFilter(tenant, size, searchConfig);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForFilter(tenant, size, searchConfig);
			}

			facetBeans.addAll(extractFacetAndFilter(response));
		}
		return facetBeans;
	}
	
	private List<FacetBean> extractFacetAndFilter(SearchResponse response) {
		
		Map<String, Set<String>> mapOfFacet = new HashMap<>();// Map of Facet name and Its filter set
		Map<String,Integer> countMap = new HashMap<>();
		
		
		for (SearchHit oneHit : response.getHits()) {
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			Map<String, TemplateColumnBean> map = indexAssetBean.getTemplateCols();
			map.entrySet().stream()
					.filter(m -> m.getValue().isFilterable())
					//.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue().getValue()))
					.forEach(s -> {
						if(mapOfFacet.containsKey(s.getKey())){
							mapOfFacet.get(s.getKey()).add(s.getValue().getValue().toString());
							if(countMap.containsKey(s.getValue().getValue().toString())){
								int count= countMap.get(s.getValue().getValue().toString());
								count++;
								countMap.put(s.getValue().getValue().toString(), count);
							}else{
								countMap.put(s.getValue().getValue().toString(), 1);
							}
							
						} else {
							Set<String> set = new HashSet<>();
							set.add(s.getValue().getValue().toString());
							mapOfFacet.put(s.getKey(), set);
							countMap.put(s.getValue().getValue().toString(), 1);
						}
					});
		}
		
		return prepareFacetBeansToReturn(mapOfFacet, countMap);
	}

	private List<FacetBean> prepareFacetBeansToReturn(Map<String, Set<String>> mapOfFacet, Map<String, Integer> countMap) {
		List<FacetBean> facetBeans = new ArrayList<>();
		for (Entry<String, Set<String>> entry : mapOfFacet.entrySet()) {
			FacetBean facetBean = new FacetBean();
			facetBean.setName(entry.getKey());
			for (String filterValue : entry.getValue()) {
				if(!filterValue.isEmpty()){					
					FilterBean filterBean = new FilterBean();
					filterBean.setName(filterValue);					
					if(countMap.containsKey(filterValue)){
						filterBean.setCount(countMap.get(filterValue));
					}
					facetBean.addFilterBean(filterBean);
				}
			}
			if(!facetBean.getListOfFilterBean().isEmpty()) {				
				facetBeans.add(facetBean);
			}
		}
		return facetBeans;
	}

	private SearchResponse getResponseForFilter(String tenant, int size, SearchConfig searchConfig) {
		
		Map<String,List<String>> filterMap = searchConfig.getFilterMap();
		
		BoolQueryBuilder boolForAssetClassAndFacet = extractBoolFilterQuery(filterMap);
		
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(boolForAssetClassAndFacet)
				.setPostFilter(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace()))
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}

	@Override
	public BoolQueryBuilder extractBoolFilterQuery(Map<String, List<String>> filterMap) {
		BoolQueryBuilder boolForAssetClassAndFacet = QueryBuilders.boolQuery();
		if(filterMap.containsKey(ElasticSearchTags.ASSETCLASS.getTagKey())){
			BoolQueryBuilder boolForAssetClass = QueryBuilders.boolQuery();
			for (String oneClass : filterMap.get(ElasticSearchTags.ASSETCLASS.getTagKey())) {
				boolForAssetClass.should(QueryBuilders.matchQuery(ElasticSearchTags.ASSETCLASS.getTagKey(), oneClass));
			}
			boolForAssetClassAndFacet.must(boolForAssetClass);
		}
		
		if(!filterMap.isEmpty()){
			
			BoolQueryBuilder boolForFacet = QueryBuilders.boolQuery();
			for (Entry<String, List<String>> entry : filterMap.entrySet()) {
				if(!entry.getKey().equals(ElasticSearchTags.ASSETCLASS.getTagKey())){					
					BoolQueryBuilder boolForFilter = QueryBuilders.boolQuery();
					boolForFilter.should(QueryBuilders.termsQuery("templateCols." + entry.getKey() + ".value", entry.getValue()));
					NestedQueryBuilder query = QueryBuilders.nestedQuery("templateCols." + entry.getKey(), boolForFilter, ScoreMode.Avg);
					boolForFacet.filter(query);
				}
			}
			NestedQueryBuilder nestedForTemplate = QueryBuilders.nestedQuery(IndexAssetTags.TEMPLATECOLS.getTagKey(), boolForFacet, ScoreMode.Avg);
			boolForAssetClassAndFacet.must(nestedForTemplate);
		}
		return boolForAssetClassAndFacet;
	}
}
