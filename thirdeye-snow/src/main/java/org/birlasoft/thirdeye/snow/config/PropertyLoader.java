/**
 * 
 */
package org.birlasoft.thirdeye.snow.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**Load all property files.
 * @author shaishav.dixit
 *
 */
@Configuration
@PropertySources({
	@PropertySource("file:${app.home}/config/application-${spring.profiles.active}.properties")
})
public class PropertyLoader {

}
