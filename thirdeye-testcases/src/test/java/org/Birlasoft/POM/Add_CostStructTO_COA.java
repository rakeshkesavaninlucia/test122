package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Add_CostStructTO_COA extends Util{
	


		
		public  Add_CostStructTO_COA (WebDriver driver) 
		{
				
				PageFactory.initElements(driver,this);
				
			}
			
		//objects of Question Page
			
			
		     
		@FindBy(xpath = "//span[contains(text(),'TCO Cost Structure')]")
	    public WebElement verify_page_title;       
		
		@FindBy(xpath = "//span[contains(text(),'Add Cost Structures')]")
	    public WebElement Add_Chart_of_account_click;
		
		//@FindBy(xpath = "//*[@id='name']")
	    //public WebElement Chart_of_account_name_text;
		              
		       
		       
	public String get_PageTitle() {
	  	    	   
	  	    	   String pageTitle = verify_page_title.getText();
	  	   		
	  	   		return pageTitle ;
	  	   		
	  	       }
		       
		       
		    //To verify whether the user is landing on the proper page or not
		   	public void pageetitle_verify()
		   	{
		   		try{
		   			String expectedTitle ="TCO Cost Structure";
		   			if(expectedTitle.equals(get_PageTitle()))
		   			{
		   				System.out.println("Page Title is "+expectedTitle);
		   			}
		   			
		   		}catch(Exception e){

		   	      throw new AssertionError("A clear description of the failure", e);
		   		}
		   		}
		       
		   	
		   	public void Chart_of_account_creation() {
					click_Method(Add_Chart_of_account_click);
		   		
		   		
		   	}
		
}
