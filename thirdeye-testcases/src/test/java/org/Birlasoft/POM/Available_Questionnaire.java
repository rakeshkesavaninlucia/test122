package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Available_Questionnaire extends Util
{

	public  Available_Questionnaire (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	
	
	        @FindBy(xpath = "//span[contains(text(),'Available Questionnaire')]")
             public WebElement Available_Questionnaire;
	
	        @FindBy(xpath = "//*[@id='templateColumnsofview_filter']/label/input")
		    public WebElement Click_Search_Question;
	       
	        @FindBy(xpath = "//tr[1]/td[1]/a")
		   public WebElement Click_Question_link;
	        
	          @FindBy(xpath = "//tr[1]/td[4]/a")
			  public WebElement Click_Question_edit;
	      
	       

	       
             public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Available_Questionnaire.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle = "Available Questionnaire";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is"+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	       
	   
	   //To search the question
	   	
	      public void Click_Input_Search(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(Click_Search_Question,Search);
	    	   
	       }
	    	  
	      
	       
	      
	    		       
	      
	  public void get_clickQuestion_Name() throws InterruptedException {
	    	   
	    	   
	click_Method(Click_Question_link);
		        }
	  
	  public void get_clickQuestion_Edit() throws InterruptedException {
   	   
   	   
			click_Method(Click_Question_edit);
				        }
	       
	       
}
