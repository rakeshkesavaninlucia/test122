package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChartOfAcount_Status extends Util {

	public  ChartOfAcount_Status  (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	
	
	        @FindBy(xpath = "//h1/div/div/span")
             public WebElement Publishing_TCO;
	
             @FindBy(xpath = "//button[contains(text(),'Publish Now')]")
		     public WebElement Click_Publish_TCO;
	       
	       

	       
             public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Publishing_TCO.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify(String expectedTitle)
	   	{
	   		try{
	   			
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is"+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	       
	   
	       public void get_clickPubCOA() {
	    	   
		    	  //Webtable_Element_check(Click_Tree,Tree);
		        click_Method(Click_Publish_TCO);
		        
		        }	
}
