package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Chart_of_account extends Util{

	
	public  Chart_of_account(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Reports - Chart of Account - List of Chart of Account')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//span[contains(text(),'Create Chart Of Account')]")
    public WebElement Chart_of_account_click;
	
	@FindBy(xpath = "//table[contains(@id,'templateColumnsofview')]/tbody/tr")
    public List<WebElement> Chart_of_account_click_edit;
	
	//@FindBy(xpath = "//*[@id='name']")
    //public WebElement Chart_of_account_name_text;
	              
	       
	       
public String get_PageTitle() throws InterruptedException {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	    	   
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Reports - Chart of Account - List of Chart of Account";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	public void Chart_of_account_creation() throws InterruptedException {
				click_Method(Chart_of_account_click);
				
	   		}
	   	
	   	//Method for clicking on edit icon
	   	
	   	public void Chart_of_account_table(String cost_ele_table1) throws InterruptedException {
   			
	   		Web_Table_Click(Chart_of_account_click_edit,cost_ele_table1);
	   		
		   	}
	   	//Method for cliking on first column link
	   	
      public void Chart_of_account_link(String cost_str_link) throws InterruptedException {
   			
    	  Web_Table_Click_link(Chart_of_account_click_edit,cost_str_link);
    	  }
	   	
	
	
}
