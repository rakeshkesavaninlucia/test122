package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class Create_Color_Scheme_Add_EditCondition extends Util {

	public  Create_Color_Scheme_Add_EditCondition(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
	
	//objects of color scheme
	  @FindBy(xpath="//*[@id='colorScale']")
	    public WebElement Color_Scale_Dropdown;
	    
	  @FindBy(xpath="//*[@id='colorScale']")
		public WebElement Color_Scale_Dropdown1;
	  
	  @FindBy(xpath = "//*[@id='mod-module-qualityGateCondition-1']/div[1]/div/table/tbody/tr[2]/td[2]/span/span[1]/span")
	    public WebElement Parameter_dropdown;
		
	  @FindBy(id = "select2-parameterId-results")  
	    public WebElement Parameter_dropdown1;
	  
	  @FindBy(xpath="//*[@id='qualityGateCondition']/tbody/tr/td[2]/input[2]")
	    public WebElement Description;
	  
	  @FindBy(xpath="//*[@id='qualityGateCondition']/tbody/tr/td[3]/input[2]")
	    public WebElement Description1;
	  
	  @FindBy(xpath="//*[@id='qualityGateCondition']/tbody/tr/td[4]/input[2]")
	    public WebElement Description2;
	  
	  @FindBy(xpath="//*[@id='addRowButton']")
	  	 public WebElement Add_Row;
	  
	  @FindBy(xpath="//*[@id='weight']")
	  	 public WebElement Add_weight;
	   
	  
	  @FindBy(xpath="//*[@class='saveRow fa fa-check-square-o']")
	  	 public WebElement Actions_Save_Button1;
	  
	  @FindBy(xpath="//*[@class='pull-left fa fa-chevron-left btn btn-default']")
	  	 public WebElement Actions_Pull_Left;
	  
	  @FindBy(xpath="//a[@class='fa fa-trash-o']")
	  	 public WebElement Actions_Delete;
	  
	 
	  
	  //calling of objects
	
	  public void Color_Scale_Dropdown() {
			click_Method(Color_Scale_Dropdown); 
		} 
	  
		public void Color_Scale_Dropdown(String value) {
			Dropdownselect(Color_Scale_Dropdown,value);
   		}
	  
		
		public void Parameter_Dropdown() {
			click_Method(Parameter_dropdown);
   		}
		
		public void Parameter_Dropdown1(String value) throws InterruptedException {
			
				
			Parameter_dropdown.click();
			//calling the method ajax auto select
			AjaxAuto_select(Parameter_dropdown1,value);
				
	    	}
		
	       public void get_Text(String Description_Text) {
	    	   Description.sendKeys(Description_Text);
	       }
	       
	
	       public void get_Name(String Description1_Text) {
	    	   Description1.sendKeys(Description1_Text);
	       }
	       
	       public void get_Value(String Description2_Text) {
	    	   Description2.sendKeys(Description2_Text);
	       }
	       
	       public void Add_Row()
	       {
	       	Util.fn_CheckVisibility_Button(Add_Row);
	       }
	       
	       public void Actions_Save_Button1()
	       {
	       	Util.fn_CheckVisibility_Button(Actions_Save_Button1);
	       }

	       public void Actions_Pull_Left()
	       {
	       	Util.fn_CheckVisibility_Button(Actions_Pull_Left);
	       }
		
	       public void Actions_Delete()
	       {
	       	Util.fn_CheckVisibility_Button(Actions_Delete);
	       }
		 
	       public void get_Weight(String Add_weight_Text) {
	    	   Add_weight.sendKeys(Add_weight_Text);
	       }
}
