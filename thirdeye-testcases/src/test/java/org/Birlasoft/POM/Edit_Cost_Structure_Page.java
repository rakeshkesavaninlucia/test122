package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Edit_Cost_Structure_Page extends Util{

	public  Edit_Cost_Structure_Page(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Edit Cost Structure')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//ul/li[@role='treeitem']/a[1]")
    public WebElement Edit_cost_structure_Rclick;
	
	//add child
	@FindBy(xpath = "/html/body/ul/li/a")
    public WebElement Click_add_child_COA;
	
	@FindBy(xpath= "//div[@role='tree']/ul/li/ul/li/a")
    public WebElement Click_child_name;
	              
	@FindBy(xpath = "//a[contains(text(),'Add Cost Element')]")
   public WebElement Click_add_cost_element;       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Edit Cost Structure";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	public void Cost_structure_tree_creation() {
			Right_click_Method(Edit_cost_structure_Rclick);
   		}
		
		public void Cost_structure_tree_addchild() {
			click_Method(Click_add_child_COA);
   		}
		
		public void Cost_structure_tree_addCE() throws InterruptedException {
			Thread.sleep(1000);
			Right_click_Method(Click_child_name);
   		}
		
		public void Cost_structure_tree_addCE1() throws InterruptedException {
			Thread.sleep(1000);
			click_Method(Click_add_cost_element);
   		}
	   	
	   	   			
			
	
	
}
