package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Filter_Check extends Util{

	public Filter_Check(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath = "//span[contains(text(),'Portfolio Home')]")
    public WebElement verify_page_title;
	
	@FindBy(xpath = "//div/span[2]/i")
    public WebElement filter_image;
	
	@FindBy(xpath = "//div/aside/div")
    public WebElement filterbar;
	
	@FindBy(xpath = "//*[@id='mod-module-assetTypeFacet-1']/div/div/div[1]/label/text()")
    public WebElement Checkbox;
	
	
	public String get_PageTitle() {
   	   
   	   String pageTitle = verify_page_title.getText();
  		
  		return pageTitle ;
  		
      }
    
    
 //To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
		try{
			String expectedTitle ="Portfolio Home";
			if(expectedTitle.equals(get_PageTitle()))
			{
				System.out.println("Page Title is "+expectedTitle);
			}
			
		}catch(Exception e){
 
	      throw new AssertionError("A clear description of the failure", e);
		}
		}

	
	public void  Click_on_filter_image()
	{
		
		click_Method(filter_image);
		
	}
	
    public void CheckingApplication() throws InterruptedException
    {
		
		Popup_table_close(filterbar);
	}
	
    public void ClickOnCheckbox() throws InterruptedException
    {
		
    	click_Method(Checkbox);
	}
	
}
