package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

;

public class Login_page_object extends Util{
	
public  Login_page_object(WebDriver driver){
		
		PageFactory.initElements(driver,this);
		
	} 
	//objects of Login Page
	
	@FindBy(xpath = "//title[contains(text(),'Welcome to the 3rdi')]")
	public WebElement Login_title;
	
    @FindBy(id = "username")
	public WebElement Login_Username;
	
	@FindBy(id = "password")
	public WebElement Login_Password;
	
	@FindBy(id = "tenantCode")
	public WebElement Login_Accountid;
	
	@FindBy(xpath = "//button[@type='submit']")
	public WebElement Login_Submit;
	
	@FindBy(xpath = "//title[contains(text(),'3rdEy')]")
	public WebElement Login_Homepage_title;
	
	@FindBy(xpath = "//h1[contains(text(),'HTTP Status 404 - /')]")
	public WebElement Login_404error;
	
	//objects Intialization
	
	public String getLogInPageTitle() {
		String pageTitle = Login_title.getText();
		return pageTitle;
		
	}
	public void verifyLogInPageTitle() {
		try{
		String expectedTitle = "Welcome to the 3rdi";
		if(expectedTitle.equals(getLogInPageTitle())){
			System.out.println("Page Title is"+expectedTitle);
		}
		
	}catch(Exception e){

      throw new AssertionError("A clear description of the failure", e);
	}
	}
	
	public void Login_username(String username )
	{
		System.out.println("enter value is"+username);
		if(Login_Username.isDisplayed())
			Login_Username.sendKeys(username);
		
	}
	
	public void Login_Password(String password){
		System.out.println("enter value is"+password);
		if(Login_Password.isDisplayed())
			Login_Password.sendKeys(password);
			
	}
	
	public void Login_Accountid(int Accountid){
		int i = Double.valueOf(Accountid).intValue();
		
		System.out.println("enter value is"+i);
		if(Login_Accountid.isDisplayed())
			Login_Accountid.sendKeys(String.valueOf(i));
		
	}
	
	public void Login_Accountid1(String Accountid){
		System.out.println("enter value is"+Accountid);
		if(Login_Accountid.isDisplayed())
			Login_Accountid.sendKeys(String.valueOf(Accountid));
		}
		
		
	
	public void Login_Submit(){
		if(Login_Submit.isDisplayed())
			Login_Submit.click();
	}
	
	public void Login_ErrorMsg(){
		try{
			String ea = "3rdi";
			
			Login_Homepage_title.getText().contains(ea);
			System.out.println("User successfully login to the application");
			
			
		}catch (Exception q){
			
			System.out.println("Error is"+Login_404error);
			throw new AssertionError("A clear description of the failure", q);
			
		}
		
		
	}

}
