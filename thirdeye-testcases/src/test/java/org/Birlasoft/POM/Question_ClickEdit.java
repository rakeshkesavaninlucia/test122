package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Question_ClickEdit extends Util{

	public  Question_ClickEdit(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       
	       @FindBy(xpath = "//a[@class='fa fa-pencil-square-o']")
		     public WebElement ClickEdit;
	       @FindBy(xpath = "//a[@title='Create Question Variant']")
		     public WebElement Create_varience;
	       
	       
	       
	        // @FindBy(id = "questionList")
	        // public WebElement ViewQuestion_table;
	       
	      public void ClickOn_Edit()
	       {
	    	   click_Method(ClickEdit);
           }
	      
	      public void ClickOn_Varience()
	       {
	    	   click_Method(Create_varience);
          }
	              
	    /*   
	     public void Click_input(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(ViewQuestion_Search_input,Search);
	    	   Webtable_element_click(ViewQuestion_click,Search);
           }
	       */
	
}
