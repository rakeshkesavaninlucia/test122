package org.Birlasoft.POM;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Questionnaire_Submit_the_Response_Pageobject extends Util

{
	
	public  Questionnaire_Submit_the_Response_Pageobject(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
	}
		//objects of Question Page
		
		
	     
	@FindBy(xpath = "/html/body/div/div/section[1]/h1/div/span")
    public WebElement verify_page_title;
	
	@FindBy(xpath = "//input[@id='1535'][@class='form-control editableParameter']")
    public WebElement verify_text_questionnaire;  
	
	@FindBy(xpath = "//input[@id='1539'][@class='form-control editableParameter']")
    public WebElement verify_text_questionnaire1;
	
	@FindBy(xpath = "//*[@id='mod-module-questionnaireResponse-1']/div/div[4]/div/div/form/div/div/div[3]/label/input")
    public WebElement verify_multipleChoice;
	
	@FindBy(xpath = "//*[@id='mod-module-questionnaireResponse-1']/div/div[5]/div/div/form/div/div/div[1]/label/input")
    public WebElement verify_multipleChoice1;
	       
	@FindBy(xpath = "//*[@id='1537'][@name='qq_1537']")
    public WebElement verify_number;
	
	@FindBy(xpath = "//*[@id='1541'][@name='qq_1541']")
    public WebElement verify_number1;
	
	@FindBy(xpath = "//input[@id='1538']")
    public WebElement verify_Date;
	
	@FindBy(xpath = "//input[@id='1542']")
    public WebElement verify_Date1;
	
	@FindBy(xpath = "/html/body/div/div/section[2]/div/div[3]/div[1]/a")
    public WebElement click_submit;
	 
	//click on Functional coverage link
	@FindBy(xpath = "//a[contains(@href,'functionalCoverage')]")
	public WebElement Questionnaire_Submit_the_Response_FCLink;
	       

	       
	       
	       
	public String get_PageTitle() 
	{
  	    	   
  	    String pageTitle = verify_page_title.getText();
  	   		
  	    return pageTitle ;
  	   		
  	 }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try
	   		{
	   			String expectedTitle ="Submit the Response";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e)
	   		{

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   	}
	       
	   	
	   	
	   	public void Text_type_question(String text) 
	   	{
	   		verify_text_questionnaire.sendKeys(text);
	   		
	    }
	   	
	   	public void Text_type_question1(String text) 
	   	{
	   		verify_text_questionnaire1.sendKeys(text);
	   		
	    }

	   	
	   	
	   	public void multiple_type_question()  
	   	{
	   		click_Method(verify_multipleChoice);
	   		
	    }
	   	
	   	public void multiple_type_question1()  
	   	{
	   		click_Method(verify_multipleChoice1);
	   		
	    }
	
	
	   	public void Num_type_question(String text) 
	   	{
	   		
	   		verify_number.sendKeys(text);
	    }
	   	
	   	public void Num_type_question1(String text) 
	   	{
	   		verify_number1.sendKeys(text);
	    }
	   	
		public void Date_type_question(String text)
		{
			verify_Date.sendKeys(text);
	    }
		
		public void Date_type_question1(String text)
		{
			verify_Date1.sendKeys(text);
	    }
		
		public void click_on_submit()  
		{
	   		click_Method(click_submit);
	    }
		
		//To verify whether the user able to click on Fc link or not
		public void Questionnaire_Submit_the_Response_FCLink()
		{
			click_Method(Questionnaire_Submit_the_Response_FCLink);
			
			System.out.println("User click on the Export/Import FC Link");
		}


}
