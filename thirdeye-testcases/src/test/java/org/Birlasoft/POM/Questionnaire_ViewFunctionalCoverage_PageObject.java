package org.Birlasoft.POM;



import java.awt.AWTException;
import java.io.IOException;
import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Questionnaire_ViewFunctionalCoverage_PageObject 
{

	public  Questionnaire_ViewFunctionalCoverage_PageObject(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
	}
	

	//Functional coverage Page title
	@FindBy(xpath = "//h1/div/span[contains(text(),'View Functional Coverage')]")
	public WebElement ExportImportFCpage_Fmap_Pagetitle;
	
	//Search Box in the available FunctionalCoverage
	@FindBy(xpath = "//div[@id='DataTables_Table_0_filter']/label/input[@type='search']")
	public WebElement ExportImportFCpage_FunctionalMap_SearchBox;
	
	//Click on Export/Import Icon
	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a[@title = 'Export/Import']")
	public WebElement ExportImportFCpage_ExportImport_Icon;
	
	//Available BCM level get answered through excel
	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a[@title = 'Functional Map Eye View ']")
	public WebElement ExportImportFCpage_EyeView_Icon;
	
	//click on Export button
	@FindBy(xpath = "//div/a[contains(text(),'Export')]")
	public WebElement ExportImportFCpage_ExportBtn;
	
	//click on Import tab
	@FindBy(xpath = "//li/a[contains(text(),'Import')]")
	public WebElement ExportImportFCpage_Importtab;
	
	//click on choose file
	@FindBy(name = "fileImport")
	public WebElement ExportImportFCPage_ChooseFile;
	
	//click on upload button
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement ExportImportFCPage_UploadFile;
	
	@FindBy(xpath = "//h4[contains(text(),'Functional Map Data Import/Export')]")
	public WebElement ExportImportFCpage_popup_verify;
	
	//public WebElement ExportImportFCpage_Import_Filechoose;
	
	
	
	
	
	public String get_pagetitle()
	{
		String pageTitle = ExportImportFCpage_Fmap_Pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	
	//To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
		
			try
			{
				String expectedTitle = "View Functional Coverage";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e)
			{

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	

	
	
	//click on Eyeview BCM Map icon
	public void ExportImportFCpage_AvailableBCMPage_Click(String FMapname) throws InterruptedException
	{
		Util.Webtable_Search(ExportImportFCpage_FunctionalMap_SearchBox,FMapname);
		System.out.println("User click on the Export/Import Icon");
		Util.fn_CheckVisibility_Button(ExportImportFCpage_ExportImport_Icon);
		
		
	}
	
	
	//click on Export/import Icon
	
	public void ExportImportFCpage_ExportImportIcon_Click(String FMapname) throws InterruptedException
	{
		
		Util.Webtable_Search(ExportImportFCpage_FunctionalMap_SearchBox,FMapname);
		System.out.println("User click on the Export/Import Icon");
		Util.fn_CheckVisibility_Button(ExportImportFCpage_ExportImport_Icon);
		
		
	}
	
	
	public void ExportImportFCpage_ExcelExport() throws InterruptedException
	{
		Thread.sleep(1000);
		Util. Popup_title_verify(ExportImportFCpage_popup_verify,"Functional Map Data Import/Export");
		
		// try with this method , if it s not working plz start with modal popup
		//Browser.fn_CheckVisibility_Button(ExportImportFCpage_ExportBtn);
		//Browser.Download_File(ExportImportFCpage_ExportBtn);
		//Browser.Popup_Error_verify();
		
		
		
	}
	
	
	public void ExportImportFCpage_ExcelImport(String filepath) throws InterruptedException, IOException, AWTException
	{
		Thread.sleep(1000);
		
		// try with this method , if it s not working plz start with modal popup
		Util.fn_CheckVisibility_Button(ExportImportFCpage_Importtab);
		//Browser.Popup_Error_verify();
		Thread.sleep(1000);
		Util.AutoIt_FileUpload(ExportImportFCPage_ChooseFile, filepath );
		//Browser.fn_CheckVisibility_Button(ExportImportFCPage_ChooseFile);
		Thread.sleep(1000);
		//Browser.File_Upload_Page(filepath);
		Thread.sleep(2000);
		Util.fn_CheckVisibility_Button(ExportImportFCPage_UploadFile);
		
		
	}
	
	
	
	
	
	
	
	
	

	
	
	
}

