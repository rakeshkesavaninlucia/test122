package org.Birlasoft.POM;



import java.util.List;
import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Select_Cost_Elements extends Util{

	public  Select_Cost_Elements (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//Handling Select Cost Structure modal popup
		
		     
	
	@FindBy(xpath = "//h4[contains(text(),'Select Cost Elements')]")
    public WebElement Select_Cost_element_title;
	
	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr")
    public List<WebElement> Select_Cost_element_table;
	              
	@FindBy(xpath = "//input[@value='Done']")
    public WebElement Click_done;
	

/*	@FindBy(xpath = "//*[@id='displayName']")
    public WebElement Display_name_text;   
	   
	@FindBy(xpath = "//*[@id='description']")
    public WebElement Desc_text;
	
	@FindBy(xpath = "//input[@value='Create Cost Structure']")
    public WebElement Create_button;*/
	
	
	
	 	
	public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Select_Cost_element_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Select Cost Elements";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	   	
	   	public void Cost_StructureModal() throws InterruptedException {
	   		Popup_table_close(Select_Cost_element_title);
	   		
	   		}
	  	
	   	
	  	public void Cost_element_table_Checkbox(String Cost_Element_Name) throws InterruptedException {
	   			
	  		Popup_Table_CheckBox(Select_Cost_element_table, Cost_Element_Name );
		   	}
	  	
	  	public void Create_Cost_element_button_click() {
	  		click_Method(Click_done);
	   	}
	   		
	   			      
	  	 /*   	public void Dname_Cost_Struct(String cost_str) {
	   		Display_name_text.sendKeys(cost_str);
	   		
	   	}

	   	public void Desc_Cost_Struct(String cost_str) {
	   		Desc_text.sendKeys(cost_str);
	   	}
	   	
	   	public void Create_Cost_Struct_button_click() {
	   		click_Method(Create_button);
	   	}*/
	
}
