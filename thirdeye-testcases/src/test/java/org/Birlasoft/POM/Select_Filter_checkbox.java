package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Select_Filter_checkbox extends Util{

	
	public Select_Filter_checkbox(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath = "//span[contains(text(),'Portfolio Home')]")
    public WebElement verify_page_title;
	
	@FindBy(xpath = "//*[@id='assetClassServer']")
    public WebElement server_checkbox;
		
	@FindBy(xpath = "//div/span[contains(text(),'ServerType')]")
    public WebElement facet1;
	
	@FindBy(xpath = "//div/span[contains(text(),'ServerVersion')]")
    public WebElement facet2;
	
	@FindBy(xpath = "//*[@id='mod-module-facetUpdate-1']/div/div[1]/span")
    public WebElement FacetClick;
	
	@FindBy(xpath = "//*[@id='mod-module-assetTypeFacet-1']/div/div/div[1]/label/text()")
    public WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id='facet_1']/div[1]/label")
    public WebElement FacetFilter1;
	
	@FindBy(xpath = "//*[@id='facet_1']/div[2]/label")
    public WebElement FacetFilter2;
	
	public String get_PageTitle() {
   	   
   	   String pageTitle = verify_page_title.getText();
  		
  		return pageTitle ;
  		
      }
    
    
 //To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
		try{
			String expectedTitle ="Available Cost Elements";
			if(expectedTitle.equals(get_PageTitle()))
			{
				System.out.println("Page Title is "+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
		}

	
	public void  Click_On_Server()
	{
		
		click_Method(server_checkbox);
		
	}
	
	/**
	 * Getting the facet text using Xpath
	 **/
    public void Check_facets(String P,String F) throws InterruptedException
    {
    	    
    	Wait_sleep();
    	div_List_Click(P,F,facet1,facet2);
    	    	
   }
    
    public void Click_on_facet() throws InterruptedException
    {
    	    
    	Wait_sleep();
    	click_Method(FacetClick);
    	System.out.println("Clicked on facet");
    	
    	
  }
    
    public void Click_on_facet_filter(String P,String F) throws InterruptedException
    {
    	    
    	Wait_sleep();
    	div_List_Click(P,F,FacetFilter1,FacetFilter2);
    	System.out.println("Clicked on facet filter");
    	
    	
  }
    
    }
	

	

	
	
	
	

