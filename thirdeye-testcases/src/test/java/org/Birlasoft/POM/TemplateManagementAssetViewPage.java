package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TemplateManagementAssetViewPage extends Util
{
	public  TemplateManagementAssetViewPage(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath = "//h1/div/span")
	public WebElement assetNameVerify;
	
	@FindBy(xpath="//h1/small/div/span")
	public WebElement templateNameandTemplateTypeVerify;
	
	@FindBy(xpath="//a[1][contains(text(),'Create')]")
	public WebElement redirectAddAssetPage;
	
	@FindBy(xpath="//a[2][contains(text(),'Edit')]")
	public WebElement EditAsset;
	
	@FindBy(xpath="//a[3][contains(text(),'Delete')]")
	public WebElement deleteAsset;
	
	@FindBy(xpath="//button[contains(text(),'Save')]")
	public WebElement saveAsset;
	
	@FindBy(xpath="//a[@data-type='leftButton']")
	public WebElement leftAssetNavigation;
	
	@FindBy(xpath="//a[@data-type='rightButton']")
	public WebElement rightAssetNavigation;

	@FindBy(id ="select2-assetselection-container")	
	public WebElement assetDropdownClk;
	
	@FindBy(id ="select2-assetselection-results")	
	public WebElement assetDropdownListSelect;
	
	
	public void Assetform(String actionColumnType,String columnNameSearch,Object olddata,Object runtimedata)
	{
		int flag=0;
		List<WebElement> form = driver.findElements(By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr"));
		List<WebElement> rows = form;
		for (WebElement row : rows) 
		{

			  switch (actionColumnType) 
			  {
			  case "TextTypecolumn":
				  WebElement columnName = row.findElement(By.xpath(".//td"));
				  if (columnName.getText().equals(columnNameSearch))
				  {
					  WebElement columnOperation = row.findElement(
							  By.xpath(".//td[last()]/input[@type='TEXT']"));
					 if( columnOperation.getText().equals(olddata))
					 {
						 columnOperation.sendKeys((CharSequence[]) runtimedata);
					 }
					  flag=1;
					  break;
				  }
				  else
				  {
					  flag=0;
				  }
				  
			  case "NumberTypecolumn":
				  WebElement numberColumnName = row.findElement(By.xpath(".//td"));
				  if (numberColumnName.getText().equals(columnNameSearch))
				  {
					  WebElement columnOperation = row.findElement(
							  By.xpath(".//td[last()]/input[@type='NUMBER']"));
					  Util.Set_Number(columnOperation, (String) runtimedata);
					  flag=1;
					  break;
				  }
				  else
				  {
					  flag=0;
				  }  
				  }
		
	}
	}
	
	public void deleteAsset()
	{
		deleteAsset.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	
	
	

}
