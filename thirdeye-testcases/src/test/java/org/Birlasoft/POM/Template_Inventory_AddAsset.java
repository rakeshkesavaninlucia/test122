package org.Birlasoft.POM;


import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Template_Inventory_AddAsset extends Util 
{
	public Template_Inventory_AddAsset(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}

	
	@FindBy(xpath = "//span[contains(text(),'Asset Data Entry')]")
	public WebElement Inventory_addasset_pagetitle;
	
	@FindBy(xpath = "//h1/small/div/span")
	public WebElement Inventory_addasset_Page_subtitle;
	
	@FindBy(xpath = "//input[@type='TEXT']")
	public WebElement Inventory_addasset_textbox;
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement Inventory_addasset_submit_btn;
	
	@FindBy(xpath = "//a[@class='fa fa-chevron-left btn btn-default']")
	public WebElement Inventory_addasset_back_btn;
	
	//get the invnetory title
		public String get_inventory_addasset_pagetitle()
		{
	        String pageTitle = Inventory_addasset_pagetitle.getText();
			
			return pageTitle ;
		}
		
		
		public void Inventory_addasset_title()
		{
			try{
				String expectedTitle = "Asset Data Entry";
				if(expectedTitle.equals(get_inventory_addasset_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
		}
		
		
		public String get_invnetory_name()
		{
	        String pageTitle = Inventory_addasset_Page_subtitle.getText();
			
			return pageTitle ;
			
			
		}
		
		public void Inventory_Page_inventory_name(String Inventory_Pass_name)
		{
			try{
				String expectedTitle = Inventory_Pass_name;
				if(expectedTitle.equals(get_invnetory_name()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
		}
		
		
		public void Inventory_addasset_textbox(String AssetName)
		{
			
			Set_Text(Inventory_addasset_textbox,AssetName);
			
		}
		
		public void Inventory_addasset_submit_btn()
		{
			String startUrl = driver.getCurrentUrl();
			
			click_Method(Inventory_addasset_submit_btn);
			if(driver.getCurrentUrl()!=startUrl)
			{
			assetRedundancycheck();
			WaitforUrl(5,startUrl);
			}
			else
			{
			 System.out.println("Asset name created Successfully");
			}
			
			
			
			//assetRedundancycheck();
		}
		
		public void Inventory_addasset_back_btn()
		{
			click_Method(Inventory_addasset_back_btn);
		}
		
		
		public void Assetform(String actionColumnType,String columnNameSearch,String runtimedata)
		{
			int flag=0;
			List<WebElement> form = driver.findElements(By.xpath("//table[@class='table table-bordered table-condensed table-hover']/tbody/tr"));
			List<WebElement> rows = form;
			for (WebElement row : rows) 
			{

				  switch (actionColumnType) 
				  {
				  case "TextTypecolumn":
					  WebElement columnName = row.findElement(By.xpath(".//td"));
					  if (columnName.getText().equals(columnNameSearch))
					  {
						  WebElement columnOperation = row.findElement(
								  By.xpath(".//td[last()]/input[@type='TEXT']"));
						  columnOperation.sendKeys(runtimedata);
						  flag=1;
						  break;
					  }
					  else
					  {
						  flag=0;
					  }
					  
				  case "NumberTypecolumn":
					  WebElement numberColumnName = row.findElement(By.xpath(".//td"));
					  if (numberColumnName.getText().equals(columnNameSearch))
					  {
						  WebElement columnOperation = row.findElement(
								  By.xpath(".//td[last()]/input[@type='NUMBER']"));
						  Util.Set_Number(columnOperation,runtimedata);
						  flag=1;
						  break;
					  }
					  else
					  {
						  flag=0;
					  }  
					  }
			
		}
		}
		
		@FindBy(xpath="//span[contains(text(),'Name already exists')]")
		public WebElement assetRedundancyCheck;
		
		
		public void assetRedundancycheck()
		{
			fn_WaitForVisibility(assetRedundancyCheck,2);
			if(assetRedundancyCheck.isDisplayed())
			{
				String Msg=assetRedundancyCheck.getText();
				System.out.println(""+Msg);
			}else
			{
				System.out.println("Asset saved Successfully");
			}
			
		}
		
		


}

