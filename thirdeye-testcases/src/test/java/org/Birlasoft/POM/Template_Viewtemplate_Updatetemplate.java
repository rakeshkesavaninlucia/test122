package org.Birlasoft.POM;


import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Template_Viewtemplate_Updatetemplate {

	
	public Template_Viewtemplate_Updatetemplate ( WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
    
	@FindBy(xpath = "//span[contains(text(),'Update the Asset Template')]")
	public WebElement Updatetemplate_Pagetitle;
	
	@FindBy(id = "assetTemplateName")	
	public WebElement Update_template_name;

	@FindBy(id = "description")	
	public WebElement Update_template_description;


	@FindBy(xpath = "//input[@type='submit']")	
	public WebElement Update_template_save;
	
	public String get_pagetitle()
	{
		String pageTitle = Updatetemplate_Pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	public void pageetitle_verify()
	{
		try{
			String expectedTitle = "Update the Asset Template";
			if(expectedTitle.equals(get_pagetitle()))
			{
				System.out.println("Page Title is"+expectedTitle);
			}
			
		}catch(Exception e){

	      throw new AssertionError("A clear description of the failure", e);
		}
		
	}
	
	//enter the template name

	public void Update_template_name(String textbox) 

	{
		try 
		{
			
			if(Update_template_name.isDisplayed())
			{
			
				//calling the method settext	
				Util.Set_Text(Update_template_name, textbox);
		
				System.out.println("Entered Template name is" +textbox);
			}
		
		}catch(Exception e)
		{

		      throw new AssertionError("A clear description of the failure", e);
		}
		
	 }

	// enter the template description
	public void Update_template_description(String desc)
	{
		try
		{
			if(Update_template_description.isDisplayed())
			{
				
			
				//	Login_Accountid.sendKeys(String.valueOf(i));
				Update_template_description.sendKeys(desc);
			  System.out.println("Template Description is "+desc);
			}
			
		}catch(Exception e)
		{

		     throw new AssertionError("A clear description of the failure", e);
		}

		
	}

	//save the template 
	public void Update_template_save()

	{
		try
		{
			if(Update_template_save.isDisplayed())
			{
				//calling click method
				Util.fn_CheckVisibility_Button(Update_template_save);
				System.out.println("User click on save button");
			
			}
		
		}catch(Exception e)
		{

		     throw new AssertionError("A clear description of the failure", e);
		}

	}
}