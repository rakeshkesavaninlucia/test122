package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class View_Question extends Util{

	public  View_Question(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       @FindBy(xpath = "//span[contains(text(),'Available Questions')]")
	        public WebElement verify_page_title;
	
	       @FindBy(xpath = "//*[@id='questionList_filter']/label/input")
		     public WebElement ViewQuestion_Search;
	       
	       @FindBy(xpath = "//*[@id='questionList_filter']/label/input")
		     public WebElement ViewQuestion_Search_input;
	       
	       @FindBy(xpath = "//tr[1]/td[6]/div/div/a[1]")
			 WebElement ViewQuestion_click;
	       
	       @FindBy(xpath = "//a[contains(text(),'Create Question')]")
			 WebElement CreateQuestion;
	       
	        // @FindBy(id = "questionList")
	      // public WebElement ViewQuestion_table;
	       
	    
	       
	       
	       public String get_PageTitle() 
	   	{
	   	    	   
	   	    String pageTitle = verify_page_title.getText();
	   	   		
	   	   	return pageTitle ;
	   	   		
	   	}
	         
	         
	      //To verify whether the user is landing on the proper page or not
	     	public void pageetitle_verify()
	     	{
	     		try
	     		{
	     			String expectedTitle ="Available Questions";
	     			if(expectedTitle.equals(get_PageTitle()))
	     			{
	     				System.out.println("Page Title is "+expectedTitle);
	     			}
	     			
	     		}catch(Exception e)
	     		
	     		{

	     	      throw new AssertionError("A clear description of the failure", e);
	     		}
	     	}
	       public void search_Question()
	       {
	    	   click_Method(ViewQuestion_Search);
           }
	       
	     
	      /* public void Type_input(String Search)
	       {
	    	   Webtable_Search(ViewQuestion_Search_input,Search);
           }*/
	       
	       public void Click_input(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(ViewQuestion_Search_input,Search);
	    	   
           }
	       
	       public void Click_input1() throws InterruptedException
	       {
	       
	    	   click_Method(ViewQuestion_click);
	       }
	       
	       public void Click_create() throws InterruptedException
	       {
	       
	    	   click_Method(CreateQuestion);
	       }
	       
	       public void Search_question(String questiontitle) throws InterruptedException
	       {
	       
	    	   tableActionColumnValidation(questiontitle,"View Question",2,"Search" );
	       }
	    	   
	
}
