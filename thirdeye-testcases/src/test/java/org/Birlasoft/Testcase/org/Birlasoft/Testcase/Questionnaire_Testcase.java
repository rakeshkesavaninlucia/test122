package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Available_Questionnaire;
import org.Birlasoft.POM.Create_Questionnaire;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Publish_Questionnaire;
import org.Birlasoft.POM.Question_Ordering;
import org.Birlasoft.POM.Questionnaire_status;
import org.Birlasoft.POM.Qustionnaire_parameter;
import org.Birlasoft.POM.Response_Status;
import org.Birlasoft.POM.Score_Questionnaire;
import org.Birlasoft.POM.SelectAssetType_Questionnaire;
import org.Birlasoft.POM.Select_Asset_Type;
import org.Birlasoft.POM.Submit_Response;
import org.Birlasoft.POM.Submit_the_Response;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Update_Questionnaire;
import org.Birlasoft.POM.View_Questionnaire;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Questionnaire_Testcase extends Util{
	
	
	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
		 
	
  @Test
  (priority = 0)
  public void f() throws InterruptedException  {
	
	  
	
		//login page
		Login_page_object obj1 = new Login_page_object(driver);
		obj1.verifyLogInPageTitle();
		
		Thread.sleep(1000);
		
		obj1.Login_username("manoj.shrivas@birlasoft.com");
		
		Thread.sleep(2000);
		obj1.Login_Password("welcome12#");
		Thread.sleep(2000);
		obj1.Login_Accountid1("qadd");
		Thread.sleep(1000);
		obj1.Login_Submit();
		//driver.navigate().to("http://130.1.4.252:8080/home"); 
		obj1.Login_ErrorMsg();
		Thread.sleep(1000);
		
		
}
  

  @Test(priority = 1)
  public void create_template() throws InterruptedException{
	  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		obj2.Homepage_titleverify("Portfolio Home");
		obj2.Homepage_QuestionnaireManagement();
		obj2.Homepage_QuestionnaireManagement_CreateQuestionnaire();
		//obj2.Homepage_QuestionnaireManagement_ViewQuestionnaire();
        //obj2.Homepage_QuestionnaireManagement_SubmitResponse();
       //obj2.Homepage_QuestionnaireManagement_ResponseStatus();
  }
  
  
	//--------------------------------------------------------------------------------------------------
	  //Create Questionnaire 
	   
	@Test( priority = 2 )
	  public void CreateQuestionnaire() throws InterruptedException{
	  
		  Create_Questionnaire  objQ = new Create_Questionnaire (driver); 
//		  objQ.get_PageTitle();
//		  objQ.pageetitle_verify();
		  objQ.get_Name("Wonderland");
		  objQ.get_Desc("Description");
		  objQ.select_asset_type("Application");
		  Thread.sleep(2000);
		  objQ.get_Next();
	  }
	  
	  
	  //selecting the parameter
	  
	  @Test( priority = 3 )
	  public void Select_param() throws InterruptedException
	  {
	  
		  Select_Asset_Type  obju = new Select_Asset_Type (driver);
		 
		  obju.Select_Search("DB");
		  obju.Select_Click();
		  Thread.sleep(2000);
		  obju.Delete_Click();
		  obju.Select_Search1("Mysql");
		  obju.Select_Click1();
		  Thread.sleep(2000);
		  obju.Delete_Click();
		  obju.Click_Next(); 
	  }
	  
  
	  
	  
	  
	  //Creating the parameter
	  
	    @Test(priority = 4 )
	  public void Create_param() throws InterruptedException, AWTException
	  {
	  
		  Qustionnaire_parameter  obju = new Qustionnaire_parameter (driver);
		  obju.get_param();
		  Thread.sleep(1000);
		  obju.Search_Param("DB_Auto_Test");
		  obju.get_clickcheck();
		  obju.get_clickdone();
		  Thread.sleep(1000);
		 obju.get_clickTree();
		  Thread.sleep(1000);
		  obju.get_clickClose();
		  Thread.sleep(1000);
		  obju.get_clickNext();
		  
		  
		 	}  
//-------------------------------------------------------------	  
      
	 //Question Ordering
//	----------------------------------------------------------------------------------  
	 	 @Test(priority = 5 )
	 public void QuestionOrdering() throws InterruptedException, AWTException
	 {
	 
		 Question_Ordering  objQ = new Question_Ordering (driver);
		  
		 objQ.get_PageTitle();
		 objQ.pageetitle_verify();
		 objQ.get_clickNext();
		  
		 	}
			
		//Publishing the question	

	 @Test(priority = 6 )
	 public void Publish_question() throws InterruptedException, AWTException
	 {
	 
		 Publish_Questionnaire  objStat = new Publish_Questionnaire (driver);
		  
		 objStat.get_PageTitle();
		 objStat.pageetitle_verify();
		 objStat.get_clickPubQue();
		  
		 	}
	 
	 //======================================================================================================
	//Clicking on view Questionnaire tab
	  
	/*    @Test( priority = 7 )
	  public void ViewQuestionnaire() throws InterruptedException{
	  
		  View_Questionnaire objQ = new View_Questionnaire (driver); 
		  
	  }
	  
	  //Searching available Question
		 
		 @Test(priority = 8 )
		 public void Available_question_Search() throws InterruptedException, AWTException
		 {
		 
			 Available_Questionnaire  objStat = new Available_Questionnaire (driver);
			  
			 objStat.get_PageTitle();
			 objStat.pageetitle_verify();
		     objStat.Click_Input_Search("Butterfly_effect");
			 objStat.get_clickQuestion_Name();
			  
			 	}
		 
		//Verifying questionnaire Status
		 
			@Test(priority = 9 )
			 public void Questionnaire_cycle_status() throws InterruptedException, AWTException
			 {
			 
				 Questionnaire_status  objStat = new Questionnaire_status (driver);
				  
				 objStat.get_PageTitle();
				 objStat.pageetitle_verify("Butterfly_effect");
			     objStat.Check_status();
				 objStat.Print_status("PUBLISHED");
				 objStat.Change_Questionnaire_status();
				 Thread.sleep(1000);
				 objStat.Check_change_Questionnaire_status();
				 Thread.sleep(1000);
				 objStat.Print_status1("EDITABLE");
				 Thread.sleep(1000);
				 objStat.Questionnaire_status_Published();
						  
				 	}
			
			
			//Click on the edit_Question
			
			@Test(priority = 10 )
			 public void Available_question_Search1() throws InterruptedException, AWTException
			 {
			 
				 Available_Questionnaire  objStat = new Available_Questionnaire (driver);
				  
				 objStat.get_PageTitle();
				 objStat.pageetitle_verify();
			     objStat.Click_Input_Search("Butterfly_effect");
			     objStat.get_clickQuestion_Edit();
				 }
		 
		  
	  //Updating the Questionnaire
	  
	     @Test( priority =11 )
	  public void UpdateQuestionnaire() throws InterruptedException{
	  
		  Update_Questionnaire  objU = new Update_Questionnaire (driver); 
		  objU.ClickOn_Done();
		  
	  }
	    	  
	  //Selecting asset type
	  
	     @Test( priority = 12 )
	  public void TableSearch() throws InterruptedException{
	  
		  SelectAssetType_Questionnaire objT = new SelectAssetType_Questionnaire (driver); 
		  
		  objT.get_PageTitle_SAT();
		  objT.pagetitle_verify();
		  Thread.sleep(10000);
		  objT.Select_Search("DB");
		  objT.Select_Click();
		  Thread.sleep(2000);
		  objT.Delete_Click();
		  objT.Select_Search1("Mysql");
		  objT.Select_Click1();
		  Thread.sleep(2000);
		  objT.Delete_Click();
		  objT.Click_Next();
		  
	  } */
	     
	 //----------------------------------------------------------------------------
	 
			
	
	//Submitting the response
	  
	/* @Test(priority = 12 )
	  public void Submitting_response() throws InterruptedException, AWTException
	  {
	  
		  Submit_Response  objQ = new Submit_Response (driver);
	 	  
	 	 objQ.get_PageTitle();
	 	 objQ.pageetitle_verify();
	 	 objQ.ClickOn_Search("Close_encounter_of_third_kind");
	 	 objQ.ClickOn_Edit(); 
	 	 	}
			
		//Submitting the response
	  
	  @Test(priority = 13 )
	  public void Submit_Response_page() throws InterruptedException, AWTException
	  {
	  
		  Submit_the_Response  objB = new Submit_the_Response (driver);
	 	  
		  objB.get_PageTitle();
		  objB.pageetitle_verify();
		  objB.Text_type_question("selenium");
		  objB.Text_type_question1("Webdriver");
		  objB.multiple_type_question();
		  objB.multiple_type_question1();
		  objB.Num_type_question("4");
		  objB.Num_type_question1("5");
		  objB.Date_type_question("09/20/2016");
		  objB.Date_type_question1("09/20/2016");
		  objB.click_on_submit();
	 	 	}*/
	  
	  //Response Status
	  
	  @Test(priority = 14 )
	  public void Response_status() throws InterruptedException, AWTException
	  {
	  
		  Response_Status  objR = new Response_Status (driver);
	 	  
		  objR.get_PageTitle();
		  objR.pageetitle_verify();
		  objR.search_questionnaire("Questionnaire_DB_DB");
		  objR.click_on_edit();    
		  }
	  
	  //
	  
	/*  @Test(priority = 15 )
	  public void Response_Score() throws InterruptedException, AWTException
	  {
	  
		  Score_Questionnaire  objR = new Score_Questionnaire (driver);
	 	  
		  objR.get_PageTitle();
		  objR.pageetitle_verify("Questionnaire_DB_DB");
		  objR.Score_Questionnaire("5");
		     
		  }*/
}
