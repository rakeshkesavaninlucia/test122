package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.View_Question;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TCViewQuestion extends Util{

	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}

  @Test
  (priority = 0)
  public void f() throws InterruptedException  {
	
	   //logging in Application
		Login_page_object obj1 = new Login_page_object(driver);
		obj1.verifyLogInPageTitle();
	    Thread.sleep(1000);
		obj1.Login_username("manoj.shrivas@birlasoft.com");
		Thread.sleep(2000);
		obj1.Login_Password("welcome12#");
		Thread.sleep(2000);
		obj1.Login_Accountid1("qadd");
		Thread.sleep(1000);
		obj1.Login_Submit();
		//driver.navigate().to("http://130.1.4.252:8080/home"); 
		obj1.Login_ErrorMsg();
		Thread.sleep(1000);
	}
  
  /**
   * Selecting question management from side bar
   *
   **/
  
  @Test(priority =1)
  public void click_item_on_sidebar() throws InterruptedException
  {
	    Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		obj2.Homepage_titleverify("Portfolio Home");
	    obj2.Homepage_QuestionManagement();
	    obj2.Homepage_QuestionManagement_ViewQuestion();
		
  }
  
 /**
  * Checking view question functionality and its flows 
  **/
  
 @Test(priority=2)
 public void Check_Question_Availibility() throws InterruptedException
{
 
   View_Question obj5 = new View_Question(driver);
   obj5.get_PageTitle();
   obj5.search_Question();
   obj5.Click_input("What is end date2017-04-07T13:01:54.815");
   obj5.Click_input1();

 }
	
}
