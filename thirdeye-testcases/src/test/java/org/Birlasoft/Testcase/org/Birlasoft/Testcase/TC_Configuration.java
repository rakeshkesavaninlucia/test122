package org.Birlasoft.Testcase.org.Birlasoft.Testcase;


import java.io.IOException;

import org.Birlasoft.POM.Configuration_Relationship_Type;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Configuration extends Util{
	
	
	@BeforeClass
	public void browserinit() throws IOException
	{
		//Initialization of the browser
		Util.browser();
		
	}

	@Test(priority = 0, description="Clicking on Dashboard")
	public void f() throws InterruptedException  {

		//login page
		
		Login_page_object objq = new Login_page_object(driver);
		objq.verifyLogInPageTitle();
		Thread.sleep(2000);
		objq.Login_username("meghna1.agarwal@birlasoft.com");
		Thread.sleep(2000);
		objq.Login_Password("juhi@08");
		Thread.sleep(2000);
		objq.Login_Accountid1("qadm"); 
		Thread.sleep(2000);
		objq.Login_Submit();
        objq.Login_ErrorMsg();
		Thread.sleep(2000);
		
		
	     }	


	//calling all objects
	@Test(priority = 1, description="Clicking on Configuration")
	public void Navigate_Configuration() throws InterruptedException
	{
		Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		 obj2.Homepage_titleverify("Portfolio Home");
		 obj2.Homepage_Workspace_change("Sprint_1.14");
		 Thread.sleep(1000);
		 obj2.Homepage_Configuration();
		 Thread.sleep(1000);
		 obj2.Homepage_Configuration_RelationshipTypes();
		 Thread.sleep(1000);
	}
	
	

	@Test(priority = 2, description="Clicking on Relationship Type")
	public void Naviagate_To_Relationship_Type() throws Exception
	{
		Configuration_Relationship_Type obj2 = new Configuration_Relationship_Type (driver);

		  obj2.get_PageTitle();
		  obj2.pageetitle_verify();
		  obj2.Create_Relationship_Type();
		  Wait_sleep();
		  obj2.Workspace_Dropdown("Sprint_1.14");
		  Wait_sleep();
		  obj2.get_Parent("Robo_10");
		  Wait_sleep();
		  obj2.get_Child("Robo_11");
		  Wait_sleep();
		  obj2.Direction_Dropdown("Bi-directional");
		  Wait_sleep();
		  obj2.Relationship_Type_Button();
		  Wait_sleep();
		  obj2.Click_Input_Search("Robo_10");
		  Wait_sleep();
		  obj2.Edit_Button();
		  Wait_sleep();
		  obj2.Direction_Dropdown1("One Way");
		  Wait_sleep();
          obj2.Edit_Relationship_Button();
		  
	}

}
