package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Cost_structure_search;
import org.Birlasoft.POM.CreateCStruct_Modal;
import org.Birlasoft.POM.Create_cost_structure;
import org.Birlasoft.POM.Edit_Cost_Structure_Page;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Select_Cost_Elements;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Create_Cost_Structure extends Util{

	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
	
	
	  @Test
	  (priority = 0)
	  public void login_page() throws InterruptedException  {
		
		  
		
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
		    obj1.Login_username("manoj.shrivas@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
  }
	
	  @Test(priority = 1)
	  public void Cost_structure_tab() throws InterruptedException
	  {
		  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			obj2.Homepage_titleverify("Portfolio Home");
			
			obj2.Homepage_TCOManagement();
			obj2.Homepage_TCOManagement_CostStructure();
		}
	  
	  @Test(priority = 2 )
	  public void Create_CostStructure() throws InterruptedException, AWTException
	  {
	  
		  Create_cost_structure  objCstruct = new Create_cost_structure (driver);
	 	  
		  objCstruct.get_PageTitle();
		  objCstruct.pageetitle_verify();
		  objCstruct.Click_Cost_Struct();

		  //objCstruct.AvaialbleCostStructure_ActionIcon("13_CostStructure1");
		  }
	  
	  @Test(priority = 3 )
	  public void Create_CostStructure_Modal() throws InterruptedException, AWTException
	  {
	  
		  CreateCStruct_Modal  objCstructMod = new CreateCStruct_Modal (driver);
	 	  
		  //objCstructMod.Cost_StructureModal();
		  Thread.sleep(2000);
		  objCstructMod.get_PageTitle();
		  Thread.sleep(2000);
		  objCstructMod.pageetitle_verify();
		  Thread.sleep(2000);
		  objCstructMod.Cost_StructureModal();
		  objCstructMod.Uname_Cost_Struct("Automation_CStest_4");
		  objCstructMod.Dname_Cost_Struct("CostStructure_name_40");
		  objCstructMod.Desc_Cost_Struct("desc");
		  Thread.sleep(1000);
		  objCstructMod.Create_Cost_Struct_button_click();
	 	 	}
	   
	  
	  @Test(priority = 4 )
	  public void Edit_CostStructure_tree() throws InterruptedException, AWTException
	  {
	  
	 	 Edit_Cost_Structure_Page  objCstructEdit = new Edit_Cost_Structure_Page (driver);
	 	  
	 	  //objCstructMod.Cost_StructureModal();
	 	  Thread.sleep(2000);
	 	  objCstructEdit.get_PageTitle();
	 	  Thread.sleep(2000);
	 	  objCstructEdit.pageetitle_verify();
	 	  objCstructEdit.Cost_structure_tree_creation();
	 	 Thread.sleep(2000);
	 	 objCstructEdit.Cost_structure_tree_addchild();
	 	
	 	 //calling methods from the class CreateCStruct_Modal;
	 	 CreateCStruct_Modal c;
	  	 c= new CreateCStruct_Modal( driver);
	  	c.Cost_StructureModal();
	  	c.Uname_Cost_Struct("UniqueCostStructure_Name59");
		c.Dname_Cost_Struct("CostStructure_name_57");
		c.Desc_Cost_Struct("desc");
		Thread.sleep(1000);
		c.Create_Cost_Struct_button_click();
	 	Thread.sleep(2000);
	 	objCstructEdit.Cost_structure_tree_addCE();
	 	Thread.sleep(1000);
	 	objCstructEdit.Cost_structure_tree_addCE1();
	  }

	  @Test(priority = 5)
	  public void Search_CostElement_table() throws InterruptedException, AWTException
	  {
	  
		  Select_Cost_Elements  objCstructEdit = new Select_Cost_Elements (driver);
	 	  
	 	  //objCstructMod.Cost_StructureModal();
		  Thread.sleep(2000);
	 	  objCstructEdit.get_PageTitle();
	 	 Thread.sleep(2000);
	 	  objCstructEdit.pageetitle_verify();
	 	 Thread.sleep(2000);
	 	 objCstructEdit.Cost_StructureModal();
	 	 objCstructEdit.Cost_element_table_Checkbox("Accounting_cost");
	 	 objCstructEdit.Create_Cost_element_button_click();
	 	   
	  }


}
