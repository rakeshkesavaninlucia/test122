package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Create_Question;
import org.Birlasoft.POM.Create_Questionnaire;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Parameter_CreateParameter;
import org.Birlasoft.POM.Parameter_CreateParameterpage;
import org.Birlasoft.POM.Publish_Questionnaire;
import org.Birlasoft.POM.Qustionnaire_parameter;
import org.Birlasoft.POM.Select_Asset_Type;
import org.Birlasoft.POM.Submit_Response;
import org.Birlasoft.POM.Submit_the_Response;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Template_Inventory;
import org.Birlasoft.POM.Template_Inventory_AddAsset;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Create_Template_Single_Flow extends Util{

	
	public String a = "text"+System.currentTimeMillis();
	
	@BeforeClass
	public void browserinit() throws IOException
	{
		//Initialization of the browser
		Util.browser();
		
	}
	
	 @Test(priority = 0, description="Clicking on Dashboard")
	    public void f() throws InterruptedException  {
	    
			//login page
	    	
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
			obj1.Login_username("meghna1.agarwal@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("juhi@08");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadm");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
			
	 }	
	 
	 
	 
		//logging in Application
	    //Create template
	    @Test(priority = 1, description="Clicking on Template Management")
		public void Naviagate_To_CreateTemplate_Page() throws InterruptedException
		{
				 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
				 
				 obj2.Homepage_titleverify("Portfolio Home");
				 obj2.Homepage_Workspace_change("Sprint_1.14");
				 Thread.sleep(1000);
				 obj2.Homepage_TemplateManagement();
				 obj2.Homepage_TemplateManagement_CreateTemplate();		
		}
		
	
	 		
	 		@Test(priority = 2, description="Clicking on Create Template ")
	 		public void Create_Asset_Template() throws InterruptedException
	 		{
	 			Template_CreateTemplate_PageObject obj3 = new Template_CreateTemplate_PageObject(driver);
	 			obj3.CreateTemplate_title_verify();
	 			obj3.CreateTemplate_Assettype_click("Database");
	 			Thread.sleep(1000);
	 			obj3.CreateTemplate_template_name("TemplatePOM2222");
	 			Thread.sleep(1000);
	 			obj3.CreateTemplate_template_description("TemplatePOM2222");
	 			obj3.CreateTemplate_template_save();
	 			
	 		}
	 		

	 	   
	 	   @Test(priority = 3, description="Clicking on Inventory List")
	 	   public void Navigate_To_Inventory() throws InterruptedException 
	 	   {
	 		   Home_PageObjecet obj4 = new Home_PageObjecet(driver);
	 		   obj4.Homepage_TemplateManagement_Inventory();
	 		   Thread.sleep(1000);
	 		   obj4.Homepage_TemplateManagement_Inventorylist("TemplatePOM2222");
	 		   
	 	   }
	 	   
	 		
	 	   @Test(priority = 4, description="Adding Asset")
	 	   public void Add_Asset_To_Inventory()
	 	   {
	 		   Template_Inventory obj5 = new Template_Inventory(driver);
	 		   obj5.Inventory_title();
	 		   obj5.Inventory_AddAsset_Btn_clk(); 
	 		   
	 	   }
	 	   
	 	   @Test(priority = 5, description="Adding Asset For template")
	 	   public void Add_Asset_Page() throws InterruptedException
	 	   {
	 		
	 		   Template_Inventory_AddAsset obj6 = new Template_Inventory_AddAsset(driver);
	 		   obj6.Inventory_addasset_title();
	 		   obj6.Inventory_addasset_textbox("Invent2222");
	 	   	   Thread.sleep(1000);
	 		   obj6.Inventory_addasset_submit_btn();
//	 		   Thread.sleep(1000);
//	 		   obj6.Inventory_addasset_textbox("Vent2");
//	 		   obj6.Inventory_addasset_submit_btn();
//	 		   Thread.sleep(1000);
	 		   obj6.Inventory_addasset_back_btn();   
	 		   
	 	   }

	   
 	   // Create Question
	 	   
//	 	  @Test(priority = 6)
//	 	  public void create_Question() throws InterruptedException{
//	 		  Home_PageObjecet obj7 = new Home_PageObjecet(driver);
//	 			
//	 		    obj7.Homepage_QuestionManagement();
//	 		    obj7.Homepage_QuestionManagement_CreateQuestion();
//	 			
//	 	  }
//	 	  
//	 	 
//	 	  //Question
//	 	
//	 	 @Test( priority = 7 )
//	 	  public void Create() throws InterruptedException{
//	 	  
//	 	  Create_Question obj8 = new Create_Question(driver);
//	 	  obj8.getWrkSpc();
//	 	  obj8.getWrkSpcSelect("Sprint_1.14");
//	 	  obj8.getMode();
//	 	  obj8.getMode1("FIXED");
//	 	  obj8.getcategory();
//	 	  obj8.getcategory1("Application Evolution");
//	 	  obj8.getdisName("Category2");
//	 	  obj8.get_title("Category2");
//	 	  obj8.getHelpText("Category2");
//	 
//	 	  obj8.getQuestion_type1("Paragraph text");
//	 	  obj8.complete_creation(); 
//	 	 Thread.sleep(2000);
//	 	  }
//	 	  
//	 	   
//	 	 @Test( priority = 8 )
//	 	  public void Create2() throws InterruptedException{
//	 	  
//	 	  Create_Question obj9 = new Create_Question(driver);
//	 	  obj9.Create_button();
//	 	  obj9.getWrkSpc();
//	 	  obj9.getWrkSpcSelect("Sprint_1.14");
//	 	  obj9.getMode();
//	 	  obj9.getMode1("MODIFIABLE");
//	 	  obj9.getcategory();
//	 	  obj9.getcategory1("Functional Assessment");
//	 	  obj9.getdisName("Description2");
//	 	  obj9.get_title("Description2");
//	 	  obj9.getHelpText("Description2");
//	 	  
//	 	  obj9.getQuestion_type1("Paragraph text");
//	 	  obj9.complete_creation(); 
//	  	 Thread.sleep(2000);
//	 	  }
//	 	  
	 	   
	     //Create_Parameter 	   
	 	   
	 	   
	 	 @Test(priority = 6, description="Clicking on Parameter Management")
	     public void Naviagate_To_Parameter()
	 	{
	 		 Home_PageObjecet obj10 = new Home_PageObjecet(driver);
	 		 obj10.Homepage_ParameterManagement();
	 		 obj10.Homepage_parameterManagement_ViewParameter();
	 	}
	     
	      @Test(priority = 7)
	 	public void Create_Parameter() throws InterruptedException
	 	{
	 		Parameter_CreateParameter obj11 = new Parameter_CreateParameter(driver);
	 		Thread.sleep(1000);
	 		obj11.pageetitle_verify();
		  	Thread.sleep(2000);

	 		obj11.Create_Parameter_CrtParamBtn();
	 			
	 	}
	 	

	    
	      @Test(priority = 8, description="Create Parameter")
	      
	      public void Create_AP_Paramter() throws InterruptedException
	  	{
	  		Parameter_CreateParameterpage obj12 = new Parameter_CreateParameterpage(driver);
	  		obj12.CreateParameter_pagetitle_verify();
	  		obj12.CreateParameter_Parametertype_click("Aggregate Parameter - Parameter of Parameters");
	  		Thread.sleep(1000);
	  		obj12.CreateParameter_Uniquename("AP_Automation_july_2222");
	  		Thread.sleep(1000);
	  		obj12.CreateParameter_Displayname("AP_Automation_july_2222");
	  		Thread.sleep(1000);
	  		obj12.CreateParameter_Description("Create AP Parameter_2222");
	  		Thread.sleep(1000);
	  		obj12.CreateParameter_AP_Btn();
	  		
	  		//obj4.CreateParameter_AP_Parameter_Popup_pagetitle_verify();
	  		obj12.CreateParameter_Allpopup_Radiobutton1("BusCrit_Application Category");
	  		obj12.CreateParameter_AP_Submit();
	  		Thread.sleep(1000);
	  		obj12.CreateParameter_AP_Btn();
	  		obj12.CreateParameter_Allpopup_Radiobutton1("Business Criticality");
	  		obj12.CreateParameter_AP_Submit();
	  		Thread.sleep(1000);
	  		obj12.CreateParamete_Weight1strow("0.5");
	  		obj12.CreateParamete_Weight2ndrow("0.5");
	  		obj12.CreateParameter_submitbtn();
	  	}
	  	
	  	

//////--------------------------------------------------------------------------------------------------
	  //Create Questionnaire 
	      

	      @Test(priority = 9, description="Clicking on Questionnaire Management")
	      public void create_questionnaire() throws InterruptedException{
	    	  Home_PageObjecet obj13 = new Home_PageObjecet(driver);
	    		
	    		obj13.Homepage_QuestionnaireManagement();
	    		obj13.Homepage_QuestionnaireManagement_CreateQuestionnaire();
//	    	    obj13.Submit_AP_Response();
	      }
	      
		   
	      @Test( priority = 10 , description="Creating Questionnaire" )
		  public void CreateQuestionnaire() throws InterruptedException{
		  
			  Create_Questionnaire  obj14 = new Create_Questionnaire (driver); 
//			  objQ.get_PageTitle();
//			  objQ.pageetitle_verify();
			  obj14.get_Name("Questionnaire2222");
			  obj14.get_Desc("Description");
			  obj14.select_asset_type("Database");
			  Thread.sleep(2000);
			  obj14.get_Next();
		  }
		  
		  
		  
		  
		  //selecting the parameter
		  
		  @Test( priority = 11, description="selecting the parameter")
		  public void Select_param() throws InterruptedException
		  {
		  
			  Select_Asset_Type  obj15 = new Select_Asset_Type (driver);
			 
			  obj15.Select_Search("Invent2222");
			  obj15.Select_Click();
			  Thread.sleep(2000);
//			  obj15.Delete_Click();
//			  obj15.Select_Search1("Data222222");
//			  obj15.Select_Click1();
//			  Thread.sleep(2000);
			  obj15.Click_Next(); 
		  }
		  
	  
		  //Creating the parameter
		  
		    @Test(priority = 12, description="Creating the parameter")
		    public void Create_param() throws InterruptedException, AWTException
		  {
		  
			  Qustionnaire_parameter  obju = new Qustionnaire_parameter (driver);
			  obju.get_param();
			  Thread.sleep(1000);
			  obju.Search_Param("AP_Automation_july_2222");
			  Thread.sleep(1000);
			  obju.get_clickcheck();
			  Thread.sleep(1000);
			  obju.get_clickdone();
			  Thread.sleep(1000);
			  obju.get_clickTree();
			  Thread.sleep(1000);
			  obju.get_clickClose();
			  Thread.sleep(1000);
			  obju.get_clickNext();
			  obju.get_clickNext();
			 
			 	}  

			//Publishing the question	

			 @Test(priority = 13, description="Publishing the question")
			 public void Publish_question() throws InterruptedException, AWTException
			 {
			 
				 Publish_Questionnaire  objStat = new Publish_Questionnaire (driver);
				  
				 objStat.get_PageTitle();
				 objStat.pageetitle_verify();
				 objStat.get_clickPubQue();
				  
				 	}
		

//				//Submitting the response
//				  
				@Test(priority = 14 , description="Submitting the response")
				  public void Submitting_response() throws InterruptedException, AWTException
				  {
				  
					  Submit_Response  objQ = new Submit_Response (driver);
				 	  
				 	 objQ.get_PageTitle();
				  	 Thread.sleep(2000);
				 	 objQ.pageetitle_verify();
				  	 Thread.sleep(2000);
				 	 objQ.ClickOn_Search("Questionnaire222");
				  	 Thread.sleep(2000);
//				 	 objQ.ClickOn_Edit(); 
				 	 	}
//				
				//Submitting the response
				  
//				  @Test(priority = 15 description="Submitting the response" )
//				  public void Submit_Response_page() throws InterruptedException, AWTException
//				  {
//				  
//					  Submit_AP_Response  objB = new Submit_AP_Response (driver);
////				 	  
////				  objB.get_PageTitle();
////				  objB.pageetitle_verify();
////				  objB.Text_type_question("selenium");
////				  objB.Text_type_question1("Webdriver");
//					  objB.multiple_type_question();
//					  objB.multiple_type_question1();
////				
////					  objB.click_on_submit();
////				 	 	}
//				  }		  
}
	 	
	 
