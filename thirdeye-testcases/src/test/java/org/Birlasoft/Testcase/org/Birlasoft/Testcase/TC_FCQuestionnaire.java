
package org.Birlasoft.Testcase.org.Birlasoft.Testcase;



import java.awt.AWTException;
import java.io.IOException;


import org.Birlasoft.POM.Home_PageObjecet;

import org.Birlasoft.POM.Login_page_object;
//import org.Pom.Questionnaire_Avialble_SubmitResponse_Pageobject;
import org.Birlasoft.POM.Questionnaire_Avialble_SubmitResponse_Pageobject;
import org.Birlasoft.POM.Questionnaire_Submit_the_Response_Pageobject;
import org.Birlasoft.POM.Questionnaire_ViewFunctionalCoverage_PageObject;
import org.Birlasoft.Utility.Util;
//import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_FCQuestionnaire extends Util
{
	//public WebDriver driver;
	@BeforeClass
	public void browserinit() throws IOException
	{
		//intialization of the browser
		browser();
		
	}
	
	
	   @Test(priority = 0)
	   public void Login() throws InterruptedException  {
			
			  
			
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			
			Thread.sleep(1000);
			
			obj1.Login_username("rakesh1.k@birlasoft.com");
			
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
			
			
	}
	
	 
	@Test(priority = 1)
	public void Naviagate_To_Questionnaire() throws InterruptedException
	{
			 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			 
			 obj2.Homepage_titleverify("Portfolio Home");
			 obj2.Homepage_Workspace_change("Workspace 1.2");
			 Thread.sleep(5000);
			 obj2.Homepage_QuestionnaireManagement();
			 obj2.Homepage_QuestionnaireManagement_SubmitResponse();
			
	}
	
	
	@Test(priority = 2)
	public void Available_Submit_Response_Page()
	{
		
		Questionnaire_Avialble_SubmitResponse_Pageobject obj3 = new Questionnaire_Avialble_SubmitResponse_Pageobject(driver);
		obj3.pageetitle_verify();
		obj3.ClickOn_Search("Sep29_Qn03 _ with Functional Coverage FC002");
		obj3.ClickOn_Edit();
		
		
	}
	
	
	@Test(priority = 3)
	public void Navigate_To_FCParameter_Page()
	{
		Questionnaire_Submit_the_Response_Pageobject obj4 = new Questionnaire_Submit_the_Response_Pageobject(driver);
		obj4.pageetitle_verify();
		obj4.Questionnaire_Submit_the_Response_FCLink();

	}
	   
	
	@Test(priority = 4)
	public void Fmap_Import_Export() throws InterruptedException, IOException, AWTException
	{
		
		Questionnaire_ViewFunctionalCoverage_PageObject obj5 = new Questionnaire_ViewFunctionalCoverage_PageObject(driver);
		obj5.pageetitle_verify();
		obj5.ExportImportFCpage_ExportImportIcon_Click("Sep02_FcParameter 02");
		obj5.ExportImportFCpage_ExcelExport();
		//obj5.ExportImportFCpage_ExcelImport("D:\\3RDI\\Hybrid\\3rdeye-Hybrid\\Sep29_Fmap02_ITService__Asset.xlsx");
		obj5.ExportImportFCpage_ExcelImport("D:\\3RDI\\Upload_file.exe");
	}
	    
	    
	@AfterClass(alwaysRun=true)
	public void Parameter_CloseBrowser()
	{
		
		closeBrowser();
		QuitBrowser();
	} 
	    
    
}
	   

