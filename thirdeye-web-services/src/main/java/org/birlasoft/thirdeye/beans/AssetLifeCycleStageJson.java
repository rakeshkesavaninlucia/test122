package org.birlasoft.thirdeye.beans;

import java.util.HashMap;
import java.util.Map;
@SuppressWarnings("rawtypes")
public  class AssetLifeCycleStageJson {

	private Map lifecyc ;

	public Map getLifecyc() {
		return lifecyc;
	}
	public void setLifecyc(Map lifecyc) {
		this.lifecyc = lifecyc;
	}
	@SuppressWarnings("unchecked")
	public void setCycleValues( String lifeName, String value) {
		lifecyc.put(lifeName, value);
	}
	public AssetLifeCycleStageJson(){
		lifecyc= new HashMap<String,String>();
	}

}
