package org.birlasoft.thirdeye.beans;

public class AssetLifeCycleStagesBean {
	private String id;
	private String lifeCycle_s;
	private String lifeCycle_t;
	private String lifeCycle_f;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLifeCycle_s() {
		return lifeCycle_s;
	}
	public void setLifeCycle_s(String lifeCycle_s) {
		this.lifeCycle_s = lifeCycle_s;
	}
	public String getLifeCycle_t() {
		return lifeCycle_t;
	}
	public void setLifeCycle_t(String lifeCycle_t) {
		this.lifeCycle_t = lifeCycle_t;
	}
	public String getLifeCycle_f() {
		return lifeCycle_f;
	}
	public void setLifeCycle_f(String lifeCycle_f) {
		this.lifeCycle_f = lifeCycle_f;
	}

}
