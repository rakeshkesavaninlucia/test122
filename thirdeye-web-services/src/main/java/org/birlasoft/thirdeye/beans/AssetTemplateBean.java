package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.springframework.stereotype.Component;

@Component
public class AssetTemplateBean extends AssetTemplate {
	
	private static final long serialVersionUID = -399350901886978621L;
	private Integer assetTypeId;
	private Integer parentAssetTypeId;
	private Integer childAssetTypeId;
	private Integer relationshipTypeId;
	private String displayName;
	private boolean hasTemplateColumn;
	private Integer relationshipTemplateId;
	
	public AssetTemplateBean(){}

	public AssetTemplateBean(AssetTemplate template) {
		this.assetTypeId = template.getAssetType().getId();
		super.setId(template.getId());
		super.setAssetTemplateName(template.getAssetTemplateName());
		super.setDescription(template.getDescription());
		super.setAssetType(template.getAssetType());
		super.setUpdatedDate(template.getUpdatedDate());
	}

	public Integer getAssetTypeId() {
		return assetTypeId;
	}

	public void setAssetTypeId(Integer assetTypeId) {
		this.assetTypeId = assetTypeId;
	}

	public Integer getParentAssetTypeId() {
		return parentAssetTypeId;
	}

	public void setParentAssetTypeId(Integer parentAssetTypeId) {
		this.parentAssetTypeId = parentAssetTypeId;
	}

	public Integer getChildAssetTypeId() {
		return childAssetTypeId;
	}

	public void setChildAssetTypeId(Integer childAssetTypeId) {
		this.childAssetTypeId = childAssetTypeId;
	}

	public Integer getRelationshipTypeId() {
		return relationshipTypeId;
	}

	public void setRelationshipTypeId(Integer relationshipTypeId) {
		this.relationshipTypeId = relationshipTypeId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isHasTemplateColumn() {
		return hasTemplateColumn;
	}

	public void setHasTemplateColumn(boolean hasTemplateColumn) {
		this.hasTemplateColumn = hasTemplateColumn;
	}

	public Integer getRelationshipTemplateId() {
		return relationshipTemplateId;
	}

	public void setRelationshipTemplateId(Integer relationshipTemplateId) {
		this.relationshipTemplateId = relationshipTemplateId;
	}
}
