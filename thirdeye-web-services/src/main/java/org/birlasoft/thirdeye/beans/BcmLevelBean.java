package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.colorscheme.bean.CSSBean;
import org.birlasoft.thirdeye.comparator.Sequenced;

public class BcmLevelBean extends CSSBean implements Sequenced {

	private Integer id;
	private String bcmLevelName;
	private String category;
	private int levelNumber;
	private Integer sequenceNumber;
	private String description;
	private String addIconTitle;
	private String titleOnEdit;
	
	private BcmLevelBean parent;
	private BcmLevel superNodeBcmLevel;
	private List<BcmLevelBean> childLevels = new ArrayList<>();
	
	
	
	/**
	 * Default constructor
	 */
	public BcmLevelBean() {
		//default constructor
	}
	
	public BcmLevelBean(int levelNo, BcmLevel bcmLevel) {
		this.addIconTitle = Utility.getTitleName(levelNo);
		this.levelNumber = levelNo;
		this.superNodeBcmLevel = bcmLevel;
	}

	public BcmLevelBean(Integer id, String name, String category) {
		this.id = id;
		this.setBcmLevelName(name);
		this.category = category;
	}
	
	public BcmLevelBean(BcmLevel bcmLevel) {
		this.id = bcmLevel.getId();
		this.setBcmLevelName(bcmLevel.getBcmLevelName());
		this.category = bcmLevel.getCategory();
		this.levelNumber = bcmLevel.getLevelNumber();
		this.sequenceNumber = bcmLevel.getSequenceNumber();
		this.description = bcmLevel.getDescription();
		this.addIconTitle = Utility.getTitleName(bcmLevel.getLevelNumber());
		this.titleOnEdit = Utility.getTitleOnEdit(bcmLevel.getLevelNumber());
		this.superNodeBcmLevel = bcmLevel.getBcmLevel();
	}

	

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public BcmLevelBean getParent() {
		return parent;
	}

	public void setParent(BcmLevelBean parent) {
		this.parent = parent;
	}

	public boolean addChildLevel(BcmLevelBean child) {
		if (child == null) {
			return false;
		}
		
		return this.childLevels.add(child);
	}

	public List<BcmLevelBean> getChildLevels() {
		return childLevels;
	}

	public boolean isParent(){
		return !childLevels.isEmpty();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBcmLevelName() {
		return bcmLevelName;
	}

	public void setBcmLevelName(String bcmLevelName) {
		this.bcmLevelName = bcmLevelName;
	}

	public int getLevelNumber() {
		return levelNumber;
	}

	public void setLevelNumber(int levelNumber) {
		this.levelNumber = levelNumber;
	}

	@Override
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getAddIconTitle() {
		return addIconTitle;
	}

	public void setAddIconTitle(String addIconTitle) {
		this.addIconTitle = addIconTitle;
	}

	public String getTitleOnEdit() {
		return titleOnEdit;
	}

	public void setTitleOnEdit(String titleOnEdit) {
		this.titleOnEdit = titleOnEdit;
	}

	public BcmLevel getSuperNodeBcmLevel() {
		return superNodeBcmLevel;
	}

	public void setSuperNodeBcmLevel(BcmLevel superNodeBcmLevel) {
		this.superNodeBcmLevel = superNodeBcmLevel;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public Set<BcmLevelBean> getLeavesOfTree(int targetDepth){
		Set<BcmLevelBean> setOfLeaves = new HashSet<>();
		int currentDepth = 1;
		
		if (targetDepth > currentDepth){
			if (!childLevels.isEmpty()){
				for (BcmLevelBean oneChild : childLevels){
					oneChild.getLeavesOfTree(setOfLeaves, targetDepth, currentDepth);
				}
			}
		} else { 
			setOfLeaves.add(this);
		}
		
		
		return setOfLeaves;
	}
	
	private Set<BcmLevelBean> getLeavesOfTree(Set<BcmLevelBean> setOfLeaves, int targetDepth, int currentDepth){
		// We have gone one level deeper
		currentDepth ++ ;
		
		if (currentDepth == targetDepth){
			setOfLeaves.add(this);
		} else {
			// Dig one more level assuming you will ask  
			// targetDepth <= depth available
			if (!childLevels.isEmpty()){
				for (BcmLevelBean oneChild : childLevels){
					oneChild.getLeavesOfTree(setOfLeaves, targetDepth, currentDepth);
				}
			}
			
		}
		
		return setOfLeaves;
		
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BcmLevelBean other = (BcmLevelBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}