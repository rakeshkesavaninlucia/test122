package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter Bean
 *
 */
public class ParameterBean implements Sequenced {
	
	private static Logger logger = LoggerFactory.getLogger(ParameterBean.class);

	public static final int NA = -1;
	public static final BigDecimal NA_BIGDECIMAL = new BigDecimal(NA);
	
	Integer id;
	String name;
	String description;
	
	private Integer workspaceId;
	private String displayName;
	private String uniqueName;
	private String paramType;
	private String error;
	private Integer baseParameterId;
	
	ParameterBean parent;
	List<ParameterBean> childParameters = new ArrayList<>();
	List<QuestionBean> childQuestions = new ArrayList<>();
	
	// We could use an object as a key but want to use integer
	// as it is easier.
	Map<Integer, ParameterFunction> parameterFunctionMapping = new HashMap<>();
	
	private Integer sequenceNumber;
	
	/**
	 * Constructor to initialize parameter bean with id, name and description
	 * @param id
	 * @param name
	 * @param description
	 */
	public ParameterBean(Integer id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	/**
	 * Constructor to initialize parameter bean from parameter entity
	 * @param p
	 */
	public ParameterBean(Parameter p) {
		this.id = p.getId();
		this.name = p.getUniqueName();
		this.description = p.getDescription();
		if(p.getWorkspace() != null){
			this.workspaceId = p.getWorkspace().getId();
		}
		this.paramType = p.getType();
		this.displayName = p.getDisplayName();
		this.uniqueName = p.getUniqueName();
		if(p.getParameter() != null){
			this.baseParameterId = p.getParameter().getId();			
		}
	}
	
	/**
	 * Constructor to create clone of parameter bean
	 * @param p
	 */
	public ParameterBean(ParameterBean p) {
			this.id = p.id;
			this.name = p.name;
			this.description = p.description;
			this.workspaceId = p.workspaceId;
			this.displayName = p.displayName;
			this.paramType = p.paramType;
			this.error = p.error;
			this.baseParameterId = p.baseParameterId;
			this.parent = p.parent;
			this.childParameters = p.childParameters;
			this.childQuestions = p.childQuestions;
			this.parameterFunctionMapping = p.parameterFunctionMapping;
	}

	public ParameterBean() {
		//Default constructor
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(Integer workspaceId) {
		this.workspaceId = workspaceId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	
	public String getUniqueName() {
		return uniqueName;
	}

	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

	public ParameterType getParameterType(){
		if (paramType != null) {			
			return ParameterType.valueOf(paramType);
		}
		return ParameterType.LP;
	}

	public ParameterBean getParent() {
		return parent;
	}

	public void setParent(ParameterBean parent) {
		this.parent = parent;
	}

	/**
	 * Add child to a parameter
	 * @param child
	 * @return
	 */
	public boolean addChildParameter(ParameterBean child) {
		if (child == null) {
			return false;
		}		
		child.setParent(this);
		return this.childParameters.add(child);
	}

	/**
	 * Add child question to parameter
	 * @param questionBean
	 * @return
	 */
	public boolean addChildQuestion(QuestionBean questionBean) {
		if (questionBean == null) {
			return false;
		}
		return this.childQuestions.add(questionBean);
	}

	public List<ParameterBean> getChildParameters() {
		return childParameters;
	}

	public List<QuestionBean> getChildQuestions() {
		return childQuestions;
	}
	
	public boolean isParent(){
		return !childParameters.isEmpty() || !childQuestions.isEmpty();
	}
	
	/**
	 * Return the list of associated question beans for the parameter.
	 * This will go and do a deep traversal of the entire parameter tree 
	 * navigating aggregate and leaf parameters to look for the questions.
	 * 
	 * @return
	 */
	public List<QuestionBean> fetchListOfAllQuestionsForParameter(){
		return populateQuestionsListForParameter(null);
	}
	
	/**
	 * Populate list of parametes with child also
	 * @return
	 */
	public List<ParameterBean> fetchListOfAllParameters(){
		return populateParameterListForParameter(null);
	}
	
	private List<ParameterBean> populateParameterListForParameter(List<ParameterBean> params){
		// Initialize the questions if required
		if (params == null){
			params = new ArrayList<>();
		}
		
		// Aggregate parameter situation
		if (childParameters != null && !childParameters.isEmpty()){
			for (ParameterBean pb : childParameters){
				pb.populateParameterListForParameter(params);
				params.add(pb);
			}
		}
		
		return params;
	}
	/**
	 * Adds the list of questions for this bean to the list which is provided.
	 * 
	 * @param questions
	 * @return
	 */
	private List<QuestionBean> populateQuestionsListForParameter(List<QuestionBean> questions){
		// Initialize the questions if required
		if (questions == null){
			questions = new ArrayList<>();
		}
		
		// Aggregate parameter situation
		if (childParameters != null && !childParameters.isEmpty()){
			for (ParameterBean pb : childParameters){
				pb.populateQuestionsListForParameter(questions);
			}
		} else if (childQuestions != null && !childQuestions.isEmpty()){
			for (QuestionBean qb : childQuestions){
				questions.add(qb);
			}
		}
		
		return questions;
	}
	
	/**
	 * Evaluates the complete tree for the parameter through a deep recursive
	 * traversal of all the aggregate and leaf parameters in the tree.
	 * 
	 * @return
	 */
	public BigDecimal evaluate(){
		// Evaluate all the children and then use the 
		// parameter function to evaluate.
		BigDecimal paramValue = new BigDecimal(0);
		BigDecimal divisor = new BigDecimal(0);
				
		if (getParameterType().equals(ParameterType.AP)){
			// Iterate through the child parameters
			for (ParameterBean oneChildParameter : childParameters){
				paramValue = paramValue.add(evaluateChildParameter(oneChildParameter));
			}
		} else {
			// Iterate through the child questions
			for (QuestionBean oneChildQuestion : childQuestions){
				ParameterFunction pf = parameterFunctionMapping.get(oneChildQuestion.getId());
				BigDecimal oneChildValue = evaluateChildQuestion(oneChildQuestion, pf);
				if(!oneChildValue.equals(new BigDecimal(NA))){					
					paramValue = paramValue.add(oneChildValue);
					divisor = divisor.add(pf.getWeight());
				}
			}
			if (divisor.equals(new BigDecimal(0))){
				divisor = new BigDecimal(1);
			}
			paramValue = paramValue.divide(divisor, 2, RoundingMode.HALF_UP);
		}
		
		return paramValue;
	}

	private BigDecimal evaluateChildQuestion(QuestionBean oneChildQuestion, ParameterFunction pf) {
		BigDecimal childQuestionValue = oneChildQuestion.evaluate(oneChildQuestion.getType());
		if (childQuestionValue.equals(new BigDecimal(NA))){
			return childQuestionValue;
		}
		if (pf != null){
			return childQuestionValue.multiply(pf.getWeight());
		}
		
		return new BigDecimal(0);
	}

	private BigDecimal evaluateChildParameter(ParameterBean oneChildParameter) {
		BigDecimal childParamValue = oneChildParameter.evaluate();
		ParameterFunction pf = parameterFunctionMapping.get(oneChildParameter.getId());
		if (pf != null){
			return childParamValue.multiply(pf.getWeight());
		}

		return new BigDecimal(0);
	}

	/**
	 * Based on the responses available for the child parameters aggregates into the value of the 
	 * AP. 
	 * 
	 * @param mapOfChildParameterResponse
	 * @return
	 */
	public BigDecimal evaluateAggregate(Map<ParameterBean, BigDecimal> mapOfChildParameterResponse) {
		BigDecimal paramValue = new BigDecimal(0);
		BigDecimal divisor = new BigDecimal(0);
		for (ParameterBean oneChildParameter : childParameters) {
			ParameterFunction pf = parameterFunctionMapping.get(oneChildParameter.getId());
			
			// Add the parameter value only if we have a response 
			// which is available in the map. This means that the child parameters have
			// been able to evaluate to a particular value.
			if (mapOfChildParameterResponse.get(oneChildParameter) != null && !new BigDecimal(NA).equals(mapOfChildParameterResponse.get(oneChildParameter))){
				// Ignore values that is NA
				paramValue = paramValue.add(mapOfChildParameterResponse.get(oneChildParameter).multiply(pf.getWeight()));
				divisor = divisor.add(pf.getWeight());
			}
		}
		if (divisor.equals(new BigDecimal(0))){
			divisor = new BigDecimal(1);
		}
		paramValue = paramValue.divide(divisor, 2, RoundingMode.HALF_UP);
		return paramValue;
	}

	/**
	 * Add parameter functions to parameter bean
	 * @param id2
	 * @param pfBean
	 */
	public void addParameterFunction(Integer id2, ParameterFunction pfBean) {
		parameterFunctionMapping.put(id2, pfBean);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterBean other = (ParameterBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getBaseParameterId() {
		return baseParameterId;
	}

	public void setBaseParameterId(Integer baseParameterId) {
		this.baseParameterId = baseParameterId;
	}
	
	@Override
	protected ParameterBean clone() throws CloneNotSupportedException {
		
		return new ParameterBean(this);
	}

	public ParameterBean getClone(){
		try {
			return clone();
		} catch (CloneNotSupportedException e) {
			logger.error("Error while cloning Parameter Bean", e);
		}
		return null;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	@Override
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

}
