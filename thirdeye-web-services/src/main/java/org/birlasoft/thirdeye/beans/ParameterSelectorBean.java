package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.constant.ParameterSelectorType;


/**
 * Bean for {@link ParameterSelectorType}
 *
 */
public class ParameterSelectorBean {

	private Integer workspaceId;
	private Integer questionnaireId;
	private String mode;
	private String questionnaireType;

	public ParameterSelectorBean() {
		// Default constructor
	}
	
	/**
	 * Constructor to initialize {@link ParameterSelectorBean} with workspace id and 
	 * questionnaire id
	 * @param workspaceId
	 * @param questionnaireId
	 */
	public ParameterSelectorBean(Integer workspaceId, Integer questionnaireId) {
		this.workspaceId = workspaceId;
		this.questionnaireId = questionnaireId;
	}
	public Integer getWorkspaceId() {
		return workspaceId;
	}
	public void setWorkspaceId(Integer workspaceId) {
		this.workspaceId = workspaceId;
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getQuestionnaireType() {
		return questionnaireType;
	}

	public void setQuestionnaireType(String questionnaireType) {
		this.questionnaireType = questionnaireType;
	}
}
