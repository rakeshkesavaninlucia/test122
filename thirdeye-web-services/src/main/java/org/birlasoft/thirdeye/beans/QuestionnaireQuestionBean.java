package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

public class QuestionnaireQuestionBean implements Sequenced{
	
	private Integer questionnaireId;
	private Integer questionnaireQuestionId;
	private Integer sequenceNumber;
	
	private QuestionBean question;
	private AssetBean asset;
	// Response
	private Map<String, List<String>> mapOfResponseData;

	private String responseText;
	private Double score;
	private Map<Integer, Integer> mapOfMandatoryQuestion;
	
	public QuestionnaireQuestionBean(Integer questionnaireQuestionId,
			QuestionBean question, AssetBean asset) {
		super();
		this.questionnaireQuestionId = questionnaireQuestionId;
		this.question = question;
		this.asset = asset;
		mapOfResponseData = new HashMap<String, List<String>>();
		mapOfMandatoryQuestion = new HashMap<Integer, Integer>();
	}

	public QuestionnaireQuestionBean(QuestionnaireQuestion qq) {
		this.questionnaireQuestionId = qq.getId();
		this.questionnaireId = qq.getQuestionnaire().getId();
		this.question = new QuestionBean(qq.getQuestion(), new ParameterBean (qq.getQuestionnaireParameter().getParameterByParameterId()));
		this.asset = new AssetBean(qq.getQuestionnaireAsset().getAsset());
		this.sequenceNumber = qq.getSequenceNumber();
		mapOfMandatoryQuestion = new HashMap<Integer, Integer>();
		mapOfResponseData = new HashMap<String, List<String>>();
		Set<ResponseData> responseDatas = qq.getResponseDatas();
		for (ResponseData responseData : responseDatas) {
			if(qq.getQuestion().getQuestionType().equals(QuestionType.MULTCHOICE.toString())){
				if(qq.getQuestion().getBenchmark() == null){
					JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONMultiChoiceResponseMapper.class);
					List<JSONQuestionOptionMapper> jsonMultiChoiceOptionResponseMappers = jsonMultiChoiceResponseMapper.getOptions();
					if(jsonMultiChoiceOptionResponseMappers != null && jsonMultiChoiceOptionResponseMappers.size() > 0){
						for (JSONQuestionOptionMapper jsonMultiChoiceQuestionOptionMapper : jsonMultiChoiceOptionResponseMappers) {
							List<String> optionTextAndQuantifier = new ArrayList<String>();
							String text = jsonMultiChoiceQuestionOptionMapper.getText();
							optionTextAndQuantifier.add(text);
							optionTextAndQuantifier.add(jsonMultiChoiceQuestionOptionMapper.getQuantifier().toString());
							mapOfResponseData.put(text, optionTextAndQuantifier);
						}
					}
					if(jsonMultiChoiceResponseMapper.getOtherOptionResponseText() != null){
						List<String> otherOptionTextAndQuantifier = new ArrayList<String>();
						otherOptionTextAndQuantifier.add(Constants.OTHER_TEXT);
						otherOptionTextAndQuantifier.add(jsonMultiChoiceResponseMapper.getOtherOptionResponseText());
						mapOfResponseData.put(Constants.OTHER_TEXT, otherOptionTextAndQuantifier);
						this.responseText = jsonMultiChoiceResponseMapper.getOtherOptionResponseText();
						this.score = jsonMultiChoiceResponseMapper.getScore();
					}
				} else {
					JSONBenchmarkMultiChoiceResponseMapper jsonBenchmarkMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONBenchmarkMultiChoiceResponseMapper.class);
					List<JSONBenchmarkQuestionOptionMapper> jsonBenchmarkMultiChoiceOptionResponseMappers = jsonBenchmarkMultiChoiceResponseMapper.getOptions();
					if(jsonBenchmarkMultiChoiceOptionResponseMappers != null && jsonBenchmarkMultiChoiceOptionResponseMappers.size() > 0){
						for (JSONBenchmarkQuestionOptionMapper jsonBenchmarkMultiChoiceQuestionOptionMapper : jsonBenchmarkMultiChoiceOptionResponseMappers) {
							List<String> optionTextAndQuantifier = new ArrayList<>();
							String text = jsonBenchmarkMultiChoiceQuestionOptionMapper.getText();
							optionTextAndQuantifier.add(text);
							optionTextAndQuantifier.add(jsonBenchmarkMultiChoiceQuestionOptionMapper.getQuantifier().toString());
							mapOfResponseData.put(text, optionTextAndQuantifier);
						}
					}
				}
			}else if(qq.getQuestion().getQuestionType().equals(QuestionType.NUMBER.toString()) || qq.getQuestion().getQuestionType().equals(QuestionType.CURRENCY.toString())){
				JSONNumberResponseMapper jsonNumberResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
				if (jsonNumberResponseMapper.getResponseNumber() !=null && !StringUtils.isEmpty(jsonNumberResponseMapper.getResponseNumber())){
					this.responseText = jsonNumberResponseMapper.getResponseNumber().toString();
				}
				this.score = jsonNumberResponseMapper.getQuantifier();
			}else if(qq.getQuestion().getQuestionType().equals(QuestionType.TEXT.toString())){
				JSONTextResponseMapper jsonTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONTextResponseMapper.class);
				this.responseText = jsonTextResponseMapper.getResponseText();
				this.score = jsonTextResponseMapper.getScore();
			}else if(qq.getQuestion().getQuestionType().equals(QuestionType.PARATEXT.toString())){
				JSONParaTextResponseMapper jsonParaTextResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONParaTextResponseMapper.class);
				this.responseText = jsonParaTextResponseMapper.getResponseParaText();
				this.score = jsonParaTextResponseMapper.getScore();
			}else if(qq.getQuestion().getQuestionType().equals(QuestionType.DATE.toString())){
				JSONDateResponseMapper jsonDateResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONDateResponseMapper.class);
				this.responseText = jsonDateResponseMapper.getResponseDate();
				this.score = jsonDateResponseMapper.getScore();
			}
		}
	}

	public Map<String, List<String>> getMapOfResponseData() {
		return mapOfResponseData;
	}

	public Integer getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public QuestionBean getQuestion() {
		return question;
	}

	public void setQuestion(QuestionBean question) {
		this.question = question;
	}

	public AssetBean getAsset() {
		return asset;
	}

	public void setAsset(AssetBean asset) {
		this.asset = asset;
	}

	public Integer getQuestionnaireQuestionId() {
		return questionnaireQuestionId;
	}

	public void setQuestionnaireQuestionId(Integer questionnaireQuestionId) {
		this.questionnaireQuestionId = questionnaireQuestionId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getResponseText() {
		return responseText;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	public Map<Integer, Integer> getMapOfMandatoryQuestion() {
		return mapOfMandatoryQuestion;
	}
}
