package org.birlasoft.thirdeye.beans;

import java.util.List;

public class QuestionnaireQuestionResponseBean {
	
	private String questionTitle;
	private List<QuestionnaireQuestionGridBean> questionnaireQuestionGridBean;
	
	public String getQuestionTitle() {
		return questionTitle;
	}
	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}
	public List<QuestionnaireQuestionGridBean> getQuestionnaireQuestionGridBean() {
		return questionnaireQuestionGridBean;
	}
	public void setQuestionnaireQuestionGridBean(List<QuestionnaireQuestionGridBean> questionnaireQuestionGridBean) {
		this.questionnaireQuestionGridBean = questionnaireQuestionGridBean;
	}
	

}
