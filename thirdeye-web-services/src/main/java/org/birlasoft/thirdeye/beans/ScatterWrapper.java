package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;


public class ScatterWrapper {

	private String xAxisLabel;
	private String yAxisLabel;
	private String zAxisLabel;
	private String graphName;
	private String message;
	
	List<ScatterSeries> series = new ArrayList<>();
	

	public String getxAxisLabel() {
		return xAxisLabel;
	}

	public void setxAxisLabel(String xAxisLabel) {
		this.xAxisLabel = xAxisLabel;
	}

	public String getyAxisLabel() {
		return yAxisLabel;
	}

	public void setyAxisLabel(String yAxisLabel) {
		this.yAxisLabel = yAxisLabel;
	}

	public String getzAxisLabel() {
		return zAxisLabel;
	}

	public void setzAxisLabel(String zAxisLabel) {
		this.zAxisLabel = zAxisLabel;
	}

	public List<ScatterSeries> getSeries() {
		return series;
	}

	public void setSeries(List<ScatterSeries> series) {
		this.series = series;
	}

	public void addSeries(ScatterSeries oneSeries){
		this.series.add(oneSeries);
	}
	public String getmessage() {
		return message;
	}

	public void setmessage(String message) {
		this.message = message;
	}
	
	public String getGraphName(){
		if (StringUtils.isEmpty(graphName)){
			return getxAxisLabel() + " v/s " + getyAxisLabel() + (StringUtils.isEmpty(getzAxisLabel()) ? "" : " v/s " + getzAxisLabel());
		}
		
		return graphName;
	}
	
}
