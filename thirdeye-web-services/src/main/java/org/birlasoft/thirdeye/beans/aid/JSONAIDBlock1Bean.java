package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidSubBlockBean;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JSONAIDBlock1Bean extends JSONAIDBlockConfig {

	private enum Block1_SubBlocks {subBlock1, subBlock2,subBlock3,subBlock4,subBlock5,subBlock6}
	
	private JSONAIDSubBlockConfig subBlock1 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock2 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock3 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock4 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock5 = new JSONAIDSubBlockConfig();
	private JSONAIDSubBlockConfig subBlock6 = new JSONAIDSubBlockConfig();
	
	public JSONAIDBlock1Bean(){
		super();
	}
	
	public JSONAIDBlock1Bean(AidBlock oneEntity){
		super(oneEntity);
		
		
		if (!StringUtils.isEmpty(oneEntity.getBlockJsonconfig())){
			JSONAIDBlock1Bean deserialized = Utility.convertJSONStringToObject(oneEntity.getBlockJsonconfig(), JSONAIDBlock1Bean.class);
			if (deserialized != null){
				this.subBlock1 = deserialized.getSubBlock1();
				subBlock1.setSubBlockIdentifier(Block1_SubBlocks.subBlock1.name());
				this.subBlock2 = deserialized.getSubBlock2();
				subBlock2.setSubBlockIdentifier(Block1_SubBlocks.subBlock2.name());
				this.subBlock3 = deserialized.getSubBlock3();
				subBlock3.setSubBlockIdentifier(Block1_SubBlocks.subBlock3.name());
				this.subBlock4 = deserialized.getSubBlock4();
				subBlock4.setSubBlockIdentifier(Block1_SubBlocks.subBlock4.name());
				this.subBlock5 = deserialized.getSubBlock5();
				subBlock5.setSubBlockIdentifier(Block1_SubBlocks.subBlock5.name());
				this.subBlock6 = deserialized.getSubBlock6();
				subBlock6.setSubBlockIdentifier(Block1_SubBlocks.subBlock6.name());
			}
		}
		
	}
	
	/**
	 * Constructor to configure values in BLOCK_1.
	 * @param indexAidBlockBean
	 */
	public JSONAIDBlock1Bean(IndexAidBlockBean indexAidBlockBean) {
		super(indexAidBlockBean);
		
		List<IndexAidSubBlockBean> listOfAidSubBlockBeans = indexAidBlockBean.getSubBlocks();
		for (IndexAidSubBlockBean indexAidSubBlockBean : listOfAidSubBlockBeans) {
			if(indexAidSubBlockBean.getSubBlockName() != null){
				Block1_SubBlocks subBlockId = Block1_SubBlocks.valueOf(indexAidSubBlockBean.getSubBlockName());
				switch (subBlockId) {
				case subBlock1:
					subBlock1.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock1.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock2:
					subBlock2.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock2.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock3:
					subBlock3.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock3.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock4:
					subBlock4.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock4.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock5:
					subBlock5.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock5.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				case subBlock6:
					subBlock6.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock6.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				default:
					break;
				}
			}
		}
	}

	public JSONAIDSubBlockConfig getSubBlock1() {
		return subBlock1;
	}

	public void setSubBlock1(JSONAIDSubBlockConfig subBlock1) {
		this.subBlock1 = subBlock1;
	}

	public JSONAIDSubBlockConfig getSubBlock2() {
		return subBlock2;
	}

	public void setSubBlock2(JSONAIDSubBlockConfig subBlock2) {
		this.subBlock2 = subBlock2;
	}

	public JSONAIDSubBlockConfig getSubBlock3() {
		return subBlock3;
	}

	public void setSubBlock3(JSONAIDSubBlockConfig subBlock3) {
		this.subBlock3 = subBlock3;
	}

	public JSONAIDSubBlockConfig getSubBlock4() {
		return subBlock4;
	}

	public void setSubBlock4(JSONAIDSubBlockConfig subBlock4) {
		this.subBlock4 = subBlock4;
	}

	public JSONAIDSubBlockConfig getSubBlock5() {
		return subBlock5;
	}

	public void setSubBlock5(JSONAIDSubBlockConfig subBlock5) {
		this.subBlock5 = subBlock5;
	}

	public JSONAIDSubBlockConfig getSubBlock6() {
		return subBlock6;
	}

	public void setSubBlock6(JSONAIDSubBlockConfig subBlock6) {
		this.subBlock6 = subBlock6;
	}
	
	
	@Override
	public JSONAIDSubBlockConfig getSubBlockFromIdentifier(String subBlockIdentifier) {
		Block1_SubBlocks subBlockId = Block1_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			return subBlock1;
		case subBlock2:
			return subBlock2;
		case subBlock3:
			return subBlock3;
		case subBlock4:
			return subBlock4;
		case subBlock5:
			return subBlock5;
		case subBlock6:
			return subBlock6;
			
		default:
			return new JSONAIDSubBlockConfig();
		}
	}
	
	@Override
	public void setSubBlockFromIdentifier(String subBlockIdentifier, JSONAIDSubBlockConfig subBlockConfig) {
		Block1_SubBlocks subBlockId = Block1_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			this.subBlock1 = subBlockConfig;
			break;
		case subBlock2:
			this.subBlock2 = subBlockConfig;
			break;
		case subBlock3:
			this.subBlock3 = subBlockConfig;
			break;
		case subBlock4:
			this.subBlock4 = subBlockConfig;
			break;
		case subBlock5:
			this.subBlock5 = subBlockConfig;
			break;
		case subBlock6:
			this.subBlock6 = subBlockConfig;
			break;
		}
	}
	
	@JsonIgnore
	@Override
	public List<JSONAIDSubBlockConfig> getListOfBlocks() {
		List<JSONAIDSubBlockConfig> listOfSubBlocks = new ArrayList<JSONAIDSubBlockConfig>();
		
		for (Block1_SubBlocks subBlockId : Block1_SubBlocks.values()){
			listOfSubBlocks.add(getSubBlockFromIdentifier(subBlockId.name()));
		}
		return listOfSubBlocks;
	}
}
