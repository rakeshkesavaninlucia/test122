package org.birlasoft.thirdeye.beans.aid;

import java.util.List;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.aid.AIDBlockType;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.interfaces.SubBlockUser;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true,value={"sequenceNumber","aidBlockType","blockId","aidId","id"})
public class JSONAIDBlockConfig implements Sequenced, SubBlockUser {

	private int id;
	private int aidId;
	private int sequenceNumber;
	private AIDBlockType aidBlockType;
	
	private String blockTitle;
	
	
	public JSONAIDBlockConfig() {
	}

	public JSONAIDBlockConfig(AidBlock oneBlock){
		this.id = oneBlock.getId();
		this.aidBlockType = AIDBlockType.valueOf(oneBlock.getAidBlockType());
		this.sequenceNumber = oneBlock.getSequenceNumber();
		if (!StringUtils.isEmpty(oneBlock.getBlockJsonconfig())){
			JSONAIDBlockConfig deserialized = Utility.convertJSONStringToObject(oneBlock.getBlockJsonconfig(), JSONAIDBlockConfig.class);
			if (deserialized != null){
				this.blockTitle = deserialized.blockTitle;
			}
		}
	}
	
	/**
	 * Constructor to set block details.
	 * @param indexAidBlockBean
	 */
	public JSONAIDBlockConfig(IndexAidBlockBean indexAidBlockBean) {
		this.id = indexAidBlockBean.getId();
		this.aidBlockType = AIDBlockType.valueOf(indexAidBlockBean.getAidBlockType());
		this.sequenceNumber = indexAidBlockBean.getSequenceNumber();
		this.blockTitle = indexAidBlockBean.getTitle();
	}
	
	public AIDBlockType getAidBlockType() {
		return aidBlockType;
	}
	
	public void setAidBlockType(AIDBlockType aidBlockType) {
		this.aidBlockType = aidBlockType;
	}
	public String getBlockTitle() {
		return blockTitle;
	}
	public void setBlockTitle(String blockTitle) {
		this.blockTitle = blockTitle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAidId() {
		return aidId;
	}

	public void setAidId(int aidId) {
		this.aidId = aidId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	
	@Override
	public List<JSONAIDSubBlockConfig> getListOfBlocks() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public JSONAIDSubBlockConfig getSubBlockFromIdentifier(String subBlockIdentifier) {
		return null;
	}
	
	@Override
	public void setSubBlockFromIdentifier(String subBlockIdentifier, JSONAIDSubBlockConfig subBlockConfig) {
		
	}
}
