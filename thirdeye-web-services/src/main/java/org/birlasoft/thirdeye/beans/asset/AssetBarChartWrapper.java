package org.birlasoft.thirdeye.beans.asset;

import java.math.BigDecimal;

public class AssetBarChartWrapper {
	private String label;
	private BigDecimal value;
	private Integer assetId;
	private Integer parameterId;
	private Integer questionnaireId;
	private String questionnaireName;

	public AssetBarChartWrapper() {

	}
	public AssetBarChartWrapper(String label, BigDecimal value, Integer assetId, Integer parameterId,
			Integer questionnaireId,String questionnaireName) {
    	this.label = label;
		this.value = value;
		this.assetId = assetId;
		this.parameterId = parameterId;
		this.questionnaireId = questionnaireId;
		this.questionnaireName = questionnaireName;
	}


	public Integer getAssetId() {
		return assetId;
	}


	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}


	public Integer getParameterId() {
		return parameterId;
	}


	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}


	public Integer getQuestionnaireId() {
		return questionnaireId;
	}


	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public String getQuestionnaireName() {
		return questionnaireName;
	}


	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	} 

}
