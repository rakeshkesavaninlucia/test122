package org.birlasoft.thirdeye.beans.index;

import java.util.List;

public class IncrementAssetIndexRequestBean {
	
	private List<Integer> assetIdlst ;
	private String tenantURL ;
	private Integer aidId ;
	private Integer assetTemplateId ;
	private Integer assetId ;

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public Integer getAssetTemplateId() {
		return assetTemplateId;
	}

	public void setAssetTemplateId(Integer assetTemplateId) {
		this.assetTemplateId = assetTemplateId;
	}

	public Integer getAidId() {
		return aidId;
	}

	public void setAidId(Integer aidId) {
		this.aidId = aidId;
	}

	public List<Integer> getAssetIdlst() {
		return assetIdlst;
	}

	public void setAssetIdlst(List<Integer> assetIdlst) {
		this.assetIdlst = assetIdlst;
	}
	
	public String getTenantURL() {
		return tenantURL;
	}

	public void setTenantURL(String tenantURL) {
		this.tenantURL = tenantURL;
	}

		
}
