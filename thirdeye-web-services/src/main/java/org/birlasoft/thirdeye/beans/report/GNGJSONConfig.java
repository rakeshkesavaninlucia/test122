package org.birlasoft.thirdeye.beans.report;

import java.util.Set;

import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.util.Utility;

/**
 * @author sunil1.gupta
 *
 */
public class GNGJSONConfig {
	
	private Integer questionnaireId;
	private Set<Integer> parameterIds ;
	private Set<Integer> assetTemplateIds;
	
    public GNGJSONConfig() {
		
	}
    public GNGJSONConfig(Report r) {
    	GNGJSONConfig gngJSONConfig = Utility.convertJSONStringToObject(r.getReportConfig(), GNGJSONConfig.class);
		if (gngJSONConfig != null){
			this.questionnaireId = gngJSONConfig.getQuestionnaireId();
			this.assetTemplateIds = gngJSONConfig.getAssetTemplateIds();
			this.parameterIds = gngJSONConfig.getParameterIds();
		}	
	}

	public Integer getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public Set<Integer> getParameterIds() {
		return parameterIds;
	}

	public void setParameterIds(Set<Integer> parameterIds) {
		this.parameterIds = parameterIds;
	}

	public Set<Integer> getAssetTemplateIds() {
		return assetTemplateIds;
	}

	public void setAssetTemplateIds(Set<Integer> assetTemplateIds) {
		this.assetTemplateIds = assetTemplateIds;
	}	
}
