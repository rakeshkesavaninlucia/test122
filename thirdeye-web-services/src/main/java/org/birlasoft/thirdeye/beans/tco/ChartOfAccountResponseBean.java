package org.birlasoft.thirdeye.beans.tco;

import java.util.List;

public class ChartOfAccountResponseBean {
	
	private String costElement;
	private List<ChartOfAccountGridBean> costResponses;
	
	public String getCostElement() {
		return costElement;
	}
	public void setCostElement(String costElement) {
		this.costElement = costElement;
	}
	public List<ChartOfAccountGridBean> getCostResponses() {
		return costResponses;
	}
	public void setCostResponses(List<ChartOfAccountGridBean> costResponses) {
		this.costResponses = costResponses;
	}
	

}
