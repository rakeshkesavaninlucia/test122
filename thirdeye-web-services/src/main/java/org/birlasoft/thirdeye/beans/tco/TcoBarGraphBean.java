package org.birlasoft.thirdeye.beans.tco;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;

public class TcoBarGraphBean {
	
	private List<TcoAssetBean> tcoAssetBeans;

	public List<TcoAssetBean> getTcoAssetBeans() {
		return tcoAssetBeans;
	}

	public void setTcoAssetBeans(List<TcoAssetBean> tcoAssetBeans) {
		this.tcoAssetBeans = tcoAssetBeans;
	}
	
}
