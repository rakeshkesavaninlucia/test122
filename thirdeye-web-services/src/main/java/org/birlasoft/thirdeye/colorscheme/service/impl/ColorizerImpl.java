package org.birlasoft.thirdeye.colorscheme.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.JSONQualityGateConditionValueMapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.colorscheme.bean.CSSBean;
import org.birlasoft.thirdeye.colorscheme.service.Colorizer;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateConditionService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} of {@code Colorizer} to color {@code Beans} extending {@link CSSBean} and 
 * color parameter based on its value.
 * 
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ColorizerImpl implements Colorizer {
	
	private static final String DEFAULT_COLOR = "#99ddff";

	@Autowired
	private ParameterService parameterService;
	
	@Autowired
	private QualityGateConditionService qualityGateConditionService;
	
	@Autowired
	private QualityGateConditionService conditionService;
	
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private ParameterSearchService parameterSearchService;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<? extends CSSBean> getParameterColor(ParameterBean param, Map<? extends CSSBean, BigDecimal> mapOfBeanAndValue){
		List<CSSBean> listToReturn = new ArrayList<>();
		
		List<JSONQualityGateConditionValueMapper> listOfCondition = getConditionsForParameter(param.getId());
		
		if(!listOfCondition.isEmpty()){
			//using iterator to avoid ConcurrentModificationException
			Iterator<CSSBean> iterator = (Iterator<CSSBean>) mapOfBeanAndValue.keySet().iterator();
			while(iterator.hasNext()){
				CSSBean oneBean = iterator.next();
				String color = getColorBasedOnValue(listOfCondition, mapOfBeanAndValue.get(oneBean));
				oneBean.setHexColor(color);
				listToReturn.add(oneBean);
			}
		}
		
		return listToReturn;
	}

	/**
	 * Extract the list of {@link JSONQualityGateConditionValueMapper} from {@link QualityGate}.<br/>
	 * If there is no quality gate present or there are no conditions defined in that quality gate,
	 * default color scheme will be returned.
	 * @param qualityGate
	 * @return
	 */
	private List<JSONQualityGateConditionValueMapper> generateListOfConditionMapper(QualityGate qualityGate) {
		List<JSONQualityGateConditionValueMapper> listOfCondition;
		if(qualityGate != null && qualityGate.getQualityGateConditions() != null){
			List<QualityGateConditionBean> listOfQualityGateCondition = qualityGateConditionService.getAllConditions(qualityGate.getId());
			//assuming that parameter quality will always have only one condition
			listOfCondition = listOfQualityGateCondition.get(0).getValueMapper();
		} else {
			//get default values
			String defaultValue = qualityGateConditionService.getDefaultConditionValues();
			listOfCondition = qualityGateConditionService.getGateConditions(defaultValue);
		}
		return listOfCondition;
	}
	
	private String getColorBasedOnValue(List<JSONQualityGateConditionValueMapper> listOfCondition, BigDecimal parameterValue) {
		String color = null;
		String maxConditionColor = null;
		for (JSONQualityGateConditionValueMapper jsonQualityGateConditionValueMapper : listOfCondition) {
			if (jsonQualityGateConditionValueMapper.getValue() != null && parameterValue.compareTo(new BigDecimal(jsonQualityGateConditionValueMapper.getValue())) <= 0){				
				color = jsonQualityGateConditionValueMapper.getColor();
				break;
			}
			if(jsonQualityGateConditionValueMapper.getValue() == null){
				maxConditionColor = jsonQualityGateConditionValueMapper.getColor();
			}
		}
		if (color == null){
			color = maxConditionColor;
		}
		return color;
	}
	
	@Override
	public List<? extends CSSBean> getQualityGateColor(QualityGate qualityGate, List<AssetBean> listOfBeans){
		Map<String, Integer> mapOfSeries = conditionService.getConditionScale(qualityGate);
		
		
		Map<Integer, QualityGateCondition> mapOfParameterAndCondition = conditionService.getMapOfParameterIdAndCondition(qualityGate);
		
		// get a list of parameters in the quality gate
		// evaluate those parameters - take the latest questionnaire for parameter evaluation
		List<AssetParameterWrapper> assetParameterWrappers = evaluateQualityGateParameters(listOfBeans,
				mapOfParameterAndCondition);

		// 3. put color to asset
		addColorToBean(listOfBeans, mapOfSeries, mapOfParameterAndCondition, assetParameterWrappers);

		return listOfBeans;
	}

	/**
	 * Based on the value of parameters, add color to that asset
	 * @param listOfBeans
	 * @param mapOfSeries
	 * @param mapOfParameterAndCondition
	 * @param assetParameterWrappers
	 */
	
	private void addColorToBean(List<AssetBean> listOfBeans, Map<String, Integer> mapOfSeries,
			Map<Integer, QualityGateCondition> mapOfParameterAndCondition,
			List<AssetParameterWrapper> assetParameterWrappers) {
		Iterator<AssetBean> iterator = listOfBeans.iterator();
		while(iterator.hasNext()){
			AssetBean oneAsset = iterator.next();
			Map<QualityGateCondition, BigDecimal> mapOfConditionAndParameterValue = new HashMap<>();
			for (AssetParameterWrapper oneWrapper : assetParameterWrappers) {
				mapOfConditionAndParameterValue.put(mapOfParameterAndCondition.get(oneWrapper.getParameterId()), oneWrapper.fetchParamValueForAsset(oneAsset.getId()));
			}
			String color = this.calculateAssetColor(mapOfSeries, mapOfConditionAndParameterValue);
			oneAsset.setHexColor(color);
		}
	}

	/**
	 * Evaluate parameters present in quality gate
	 * @param listOfBeans
	 * @param mapOfParameterAndCondition
	 * @return
	 */
	
	private List<AssetParameterWrapper> evaluateQualityGateParameters(List<AssetBean> listOfBeans,
			Map<Integer, QualityGateCondition> mapOfParameterAndCondition) {
		List<AssetParameterWrapper> assetParameterWrappers = new ArrayList<>();
		Map<String, List<String>> filterMap = new HashMap<>();
		for (Integer oneParameter : mapOfParameterAndCondition.keySet()) {
			List<Questionnaire> listOfQuestionnaires = questionnaireParameterService.findByParameterId(oneParameter);
			if(!listOfQuestionnaires.isEmpty()){				
				Collections.sort(listOfQuestionnaires, 
						(o1, o2) -> o1.getUpdatedDate().compareTo(o2.getUpdatedDate()));
				Collections.reverse(listOfQuestionnaires);
				Questionnaire latestQuestionnaire = listOfQuestionnaires.get(0);
				Set<Integer> setOfParams = new HashSet<>();
				setOfParams.add(oneParameter);
				AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(oneParameter, latestQuestionnaire.getId(), new HashSet<AssetBean>(listOfBeans), filterMap);
	        	assetParameterWrappers.add(assetParameterWrapper);
			}
		}
		return assetParameterWrappers;
	}
	
	private String calculateAssetColor(Map<String, Integer> mapOfSeries, Map<QualityGateCondition, BigDecimal> mapOfConditionAndParameterValue) {
		String colorToReturn = null;
		BigDecimal weightedSum = new BigDecimal(0);
		BigDecimal divisor = new BigDecimal(0);
		for (Entry<QualityGateCondition, BigDecimal> entry : mapOfConditionAndParameterValue.entrySet()) {
			if(!entry.getValue().equals(ParameterBean.NA_BIGDECIMAL)){				
				String oneColor = getColorBasedOnValue(qualityGateConditionService.getGateConditions(entry.getKey().getConditionValues()), entry.getValue());
				weightedSum = weightedSum.add(entry.getKey().getWeight().multiply(new BigDecimal(mapOfSeries.get(oneColor))));
				divisor = divisor.add(entry.getKey().getWeight());
			}
		}
		
		if(weightedSum.equals(new BigDecimal(0))){
			return DEFAULT_COLOR;
		}
		BigDecimal avgColor = weightedSum.divide(divisor, 0, RoundingMode.HALF_DOWN);
		for (Entry<String, Integer> entry : mapOfSeries.entrySet()) {
			if(avgColor.intValue() == entry.getValue().intValue()){				
				colorToReturn = entry.getKey();
			}
		}
		return colorToReturn;
	}

	@Override
	public String getHomePageParameterColor(Integer parmeterId, BigDecimal aggregateValue) {
		
		List<JSONQualityGateConditionValueMapper> listOfCondition = getConditionsForParameter(parmeterId);
		
		return getColorBasedOnValue(listOfCondition,aggregateValue);
	}

	/**
	 * @param parmeterId
	 * @return
	 */
	private List<JSONQualityGateConditionValueMapper> getConditionsForParameter(Integer parmeterId) {
		//1. get quality gate for the parameter
		QualityGate qualityGate =  parameterService.getQualityGate(parmeterId);
		
		//2. get condition for this quality gate
		return generateListOfConditionMapper(qualityGate);
	}

}
