package org.birlasoft.thirdeye.constant;

import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.util.Utility;


/**
 * Enum for {@code question type} {@code selection}  
 */
public enum QuestionType {

	TEXT("Text", false),
	PARATEXT("Paragraph text", false),
	MULTCHOICE("Multiple choice", true),
	NUMBER("Number", true),
	DATE("Date", false),
	CURRENCY("CURRENCY",false);

	private final String value;
	private final boolean quantifiable;

	QuestionType(String value, boolean quantifiable){
		this.value = value;
		this.quantifiable = quantifiable;
	}

	public String getValue() {
		return value;
	}

	public boolean isQuantifiable() {
		return quantifiable;
	}

	public Object convertResponseFromJSONString(String jsonResponseString){

		switch (this){
		case MULTCHOICE:
			JSONMultiChoiceResponseMapper objectToReturn2 = Utility.convertJSONStringToObject(jsonResponseString, JSONMultiChoiceResponseMapper.class);
			return objectToReturn2;
		case DATE:
			JSONDateResponseMapper objectToReturn3 = Utility.convertJSONStringToObject(jsonResponseString, JSONDateResponseMapper.class);
			return objectToReturn3;
		case NUMBER:
			JSONNumberResponseMapper objectToReturn4 = Utility.convertJSONStringToObject(jsonResponseString, JSONNumberResponseMapper.class);
			return objectToReturn4;
		case PARATEXT:
			JSONParaTextResponseMapper objectToReturn5 = Utility.convertJSONStringToObject(jsonResponseString, JSONParaTextResponseMapper.class);
			return objectToReturn5;
		case TEXT:
			JSONTextResponseMapper objectToReturn6 = Utility.convertJSONStringToObject(jsonResponseString, JSONTextResponseMapper.class);
			return objectToReturn6;
		}

		return null;
	}
}

