package org.birlasoft.thirdeye.interfaces;

import java.util.List;

import org.birlasoft.thirdeye.beans.aid.JSONAIDSubBlockConfig;
/**
 * 
 * SubBlockUser Class
 *
 */
public interface SubBlockUser {
	
	/**
	 * 
	 * @param subBlockIdentifier
	 * @return
	 */
	public JSONAIDSubBlockConfig getSubBlockFromIdentifier (String subBlockIdentifier);
	
	/**
	 * 
	 * @param subBlockIdentifier
	 * @param subBlockConfig
	 */
	public void setSubBlockFromIdentifier(String subBlockIdentifier, JSONAIDSubBlockConfig subBlockConfig);
	
	/**
	 * @return 
	 */
	public List<JSONAIDSubBlockConfig> getListOfBlocks();
	
}
