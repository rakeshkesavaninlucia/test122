package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.entity.AssetTemplate;

public interface AssetLifeCycleService {
	
	public List<AssetTemplate> getListOfAssetTemplate();
	
	
	public Map<String,Map<String,List<String>>> getAssetlifeCycle(Integer assetTemplateId);
	
	public List<String> getYearRange (Integer assetTemplateId);

}
