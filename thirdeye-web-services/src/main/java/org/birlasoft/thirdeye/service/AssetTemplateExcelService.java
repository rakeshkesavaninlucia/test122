package org.birlasoft.thirdeye.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;

/**
 * Service {@code interface} for Asset Template Excel
 * @author shaishav.dixit
 *
 */
public interface AssetTemplateExcelService {
	
	public void readExcel(AssetTemplate assetTemplate, Workbook workbook, List<Asset> listOfAssets, List<ExcelCellBean> listOfErrorCells);
	/**
	 * method will check the hash code security on uploaded workbook
	 * for sheet 2
	 * @param workbook
	 * @param assetTemplate
	 * @param listOfErrorCells
	 * @return
	 */
	public boolean validateSheetSecurity(Workbook workbook,AssetTemplate assetTemplate, List<ExcelCellBean> listOfErrorCells);
	public void exportTemplate(HttpServletResponse response,AssetTemplate assetTemplate);

}
