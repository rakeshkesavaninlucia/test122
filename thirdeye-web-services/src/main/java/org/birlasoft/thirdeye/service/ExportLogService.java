package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.entity.ExportLog;

/**
 * @author sunil1.gupta * 
 */
public interface ExportLogService {	
	
	public ExportLog save(ExportLog exportLog);	
	public ExportLog findByHash(String hash);
	public void saveExportLog(Integer refId, String hash, String purpose);
	public void updateExportLog(ExportLog exportLog);

}
