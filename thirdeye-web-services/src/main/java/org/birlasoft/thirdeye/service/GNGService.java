package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.gng.GNGWrapper;


/**
 * Service interface for go/no-go report .
 * @author mehak.guglani
 */
public interface GNGService {

	public GNGWrapper getGNGWrappertoPlotReport(Set<Integer> idsOfParameters,
			Set<Integer> idsOfAssetTemplate, Integer idOfQuestionnaire,Map<String, List<String>> filterMap);

	
}
