package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;

/**
 * @author dhruv.sood
 *
 */
public interface HomePageService {


	/**@author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @param filterMap 
	 * @return
	 */
	public List<AssetClassSearchBean> home(String tenantId, Integer activeWorkspaceId, Map<String, List<String>> filterMap);


	/**@author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @param assetTypeName
	 * @param parameterName
	 * @param parameterId
	 * @param filterMap 
	 * @return
	 */
	public ParameterSearchBean homeParameterTiles(String tenantId, Integer activeWorkspaceId, String assetTypeName, Integer parameterId, Map<String, List<String>> filterMap);


	/**@author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @param filterMap 
	 * @param parameterName
	 * @return
	 */
	public AssetParameterWrapper assetParameterWrapper(String tenantId, Integer activeWorkspaceId, Integer parameterId, Map<String, List<String>> filterMap);

}
