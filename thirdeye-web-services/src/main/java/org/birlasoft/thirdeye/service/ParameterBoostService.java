package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.parameter.ParameterValueBoostBean;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;

/**Service class for Parameter Value Boost.
 * @author dhruv.sood
 *
 */
public interface ParameterBoostService {


	/**Method to generate the PVB bean list based on questionnaire and asset
	 * @author: dhruv.sood
	 * @param questionnaire
	 * @param assetId
	 * @return
	 */
	public List<ParameterValueBoostBean> generatetListOfPVBbean(Questionnaire questionnaire, Integer assetId);
	

	/** Method to validate the new PVB bean and create its object
	 * @author: dhruv.sood
	 * @param questionnaireId
	 * @param parameterId
	 * @param assetId
	 * @return
	 */
	public QuestionnaireParameterAsset validateNewBoostValue(Integer questionnaireId, Integer parameterId, Integer assetId);
	

	
	/**Method to boost the parameter evaluated value
	 * @author: dhruv.sood
	 * @param questionnaire
	 * @param listOfParameterResponses
	 * @return
	 */
	public List<ParameterEvaluationResponse> boostEvaluatedResponse(Questionnaire questionnaire, List<ParameterEvaluationResponse> listOfParameterResponses);
}


