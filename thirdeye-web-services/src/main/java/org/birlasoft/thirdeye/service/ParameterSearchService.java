package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;

public interface ParameterSearchService {
	
	/**
	 * Get value of parameter from elasticsearch
	 * @param parameterId
	 * @param questionnaireId
	 * @param assetList
	 * @param filterMap 
	 * @return
	 */
	public AssetParameterWrapper getParameterValueFromElasticsearch(Integer parameterId, Integer questionnaireId, Set<AssetBean> assetList, Map<String, List<String>> filterMap);

}
