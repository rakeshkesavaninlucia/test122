package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * 
 * @author tej.sarup
 *
 */
public interface WorkspaceSecurityService {
	
	/**
	 * Check authorize for template
	 * @param oneTemplate
	 * @param workspace
	 * @return {@code boolean}
	 */
	public boolean checkAuthForTemplate(AssetTemplate oneTemplate, Workspace workspace);

	/**
	 * Check authorize for questionnaire.
	 * @param qe
	 * @param activeWorkSpaceForUser
	 * @return {@code boolean}
	 */
	public boolean checkAuthForQuestionnaire(Questionnaire qe, Workspace activeWorkSpaceForUser);
	
	/**
	 * Check authorize for question.
	 * @param question
	 * @param activeWorkSpaceForUser
	 * @return {@code boolean} 
	 */
	public boolean checkAuthForQuestion(Question question, Workspace activeWorkSpaceForUser);

	/**
	 * Check authorize for graph.
	 * @param graph
	 * @param activeWorkSpaceForUser
	 * @return {@code boolean}
	 */
	public boolean checkAuthForGraph(Graph graph, Workspace activeWorkSpaceForUser);
	/**
	 * Authorize graph access.
	 * @param graph
	 */
	public void authorizeGraphAccess(Graph graph);
	/**
	 * Authorize questionnaire access.
	 * @param questionnaire
	 */
	public void authorizeQuestionnaireAccess(Questionnaire questionnaire);
	/**
	 * Authorize question access.
	 * @param question
	 */
	public void authorizeQuestionAccess(Question question);
	/**
	 * Authorize parameter access.
	 * @param parameter
	 */
	public void authorizeParameterAccess(Parameter parameter);

	/**
	 * Check authorize for parameter.
	 * @param parameter
	 * @param workspace
	 * @return {@code boolean}
	 */
	public boolean checkAuthForParameter(Parameter parameter, Workspace workspace);
	/**
	 * Check authorize for template.
	 * @param assetTemplate
	 */
	public void authorizeTemplateAccess(AssetTemplate assetTemplate);
	
	/**
	 * Authorize Dashboard Access
	 * @param activeDashboard
	 */
	public void authorizeDashboardAccess(Dashboard activeDashboard);
	/**
	 * Authorize Relationship Type Access
	 * @param relationshipTypeId
	 */
	public void authorizeRelationshipTypeAccess(Integer relationshipTypeId);
	
	/**
	 * @param aid
	 * @param workspace
	 * @return {@code boolean}
	 */
	public boolean checkAuthForAID(Aid aid, Workspace workspace);

	/**Authorize AID Access
	 * @param aid
	 */
	public void authorizeAidAccess(Aid aid);

	/**
	 * Authorize Functional Map Access
	 * @param functionalMapBean
	 */
	public void authorizeFunctionalMapAccess(FunctionalMapBean functionalMapBean);
	/**
	 * Authorization for BCM.
	 * @param bcmLevel
	 */
	public void authorizeBcmAccess(Bcm bcm);

	/**
	 * Authorize Quality gate access
	 * @param qg
	 */
	public void authorizeQualityGate(QualityGate qg);

	/**
	 * Authorize Failed For QuestionnaireType
	 */
	public void authorizeQuestionnaireType(Questionnaire questionnaire,String questionnaireType);
}
