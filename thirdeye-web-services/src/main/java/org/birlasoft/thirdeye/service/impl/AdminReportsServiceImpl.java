package org.birlasoft.thirdeye.service.impl;

import java.util.List;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.repositories.AssetTemplateRepository;
import org.birlasoft.thirdeye.service.AdminReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminReportsServiceImpl implements AdminReportsService {

	@Autowired
	AssetTemplateRepository assetTemplateRepository;

	@Override
	public List<AssetTemplate> findByAssetTypeid(Integer assetTypeId) {

		return assetTemplateRepository.findByAssetTypeId(assetTypeId);
	}

}
