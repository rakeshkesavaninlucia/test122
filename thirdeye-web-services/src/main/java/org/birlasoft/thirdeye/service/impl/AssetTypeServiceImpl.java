package org.birlasoft.thirdeye.service.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.AssetTypeRepository;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTypeServiceImpl implements AssetTypeService {
	
	
	@Autowired
	private AssetTypeRepository assetTypeRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private AssetTemplateService assetTemplateService;
	
	public AssetTypeServiceImpl() {
		//Default constructor
	}
	
	@Override
	public List<AssetType> findAll() {
		return assetTypeRepository.findAll();
	}
	
	@Override
    public AssetType save(AssetType assetType) {
        return assetTypeRepository.save(assetType);
    }

	@Override
	public AssetType findOne(Integer id) {							
		return assetTypeRepository.findOne(id);
	}

	@Override
	public List<AssetType> extractAssetTypes(AssetTypes[] assetTypes) {
		List<AssetType> listOfAssetTypes = findByAssetTypeAndDeleteStatus(Constants.DELETE_STATUS_FALSE);
		Iterator<AssetType> iter = listOfAssetTypes.iterator();
			while (iter.hasNext()) {
				AssetType assetType = iter.next();
				for (AssetTypes assetTypeEnum : assetTypes) {
					if(assetType.getAssetTypeName().equalsIgnoreCase(assetTypeEnum.name())){
						iter.remove();
						break;
					}
				}
			}
		
		return listOfAssetTypes;
	}

	@Override
	public AssetType findByAssetTypeName(String assetTypeName) {
		return assetTypeRepository.findByAssetTypeName(assetTypeName);
	}
	
	@Override
	public AssetType createAssetType(AssetType assetType){
		User currentUser = customUserDetailsService.getCurrentUser();		
		assetType.setAssetTypeName(assetType.getAssetTypeName());
		assetType.setDeleteStatus(Constants.DELETE_STATUS_FALSE);
		assetType.setUserByCreatedBy(currentUser);
		assetType.setCreatedDate(new Date());
		assetType.setUserByUpdatedBy(currentUser);
		assetType.setUpdatedDate(new Date());
		return assetType;
		
	}

	@Override
	public void softDeleteAssetType(Integer assetTypeId) {
		assetTypeRepository.updateDeleteStatus(assetTypeId);
	}

	@Override
	public List<AssetType> findByAssetTypeAndDeleteStatus(Boolean deleteStatus) {
		return assetTypeRepository.findByDeleteStatus(deleteStatus);
	}

	@Override
	public boolean isTemplateExist(Integer assetTypeId) {
		
		List<AssetTemplate> assetTemplate = assetTemplateService.findAssetTemplateByAssetTypeId(assetTypeId);
		if(!assetTemplate.isEmpty()){
			return true;
		}
		return false;
	}
}
