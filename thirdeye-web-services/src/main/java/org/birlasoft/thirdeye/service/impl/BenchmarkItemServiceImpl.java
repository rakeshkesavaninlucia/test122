package org.birlasoft.thirdeye.service.impl;

import java.util.List;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.repositories.BenchmarkItemRepository;
import org.birlasoft.thirdeye.repositories.BenchmarkItemScoreRepository;
import org.birlasoft.thirdeye.service.BenchmarkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for Benchmark Item Service.
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BenchmarkItemServiceImpl implements BenchmarkItemService {
	
	@Autowired
	private BenchmarkItemRepository benchmarkItemRepository;
	@Autowired
	private BenchmarkItemScoreRepository benchmarkItemScoreRepository;
	
	@Override
	public BenchmarkItem findById(Integer idOfBenchmarkItem) {	
		BenchmarkItem benchmarkItem =benchmarkItemRepository.findOne(idOfBenchmarkItem);
		benchmarkItem.getBenchmark().getId();
		if(!benchmarkItem.getBenchmarkItemScores().isEmpty()){
		 for(BenchmarkItemScore one : benchmarkItem.getBenchmarkItemScores()){
			 one.getEndYear();
			 one.getStartYear();
			 one.getScore();
			 one.getBenchmarkItem();
		 }
		}
		return benchmarkItem;
	}

	@Override
	public BenchmarkItem saveOrUpdate(BenchmarkItem benchmarkItemBean) {
	    
		if(benchmarkItemBean.getId() == null){			
			benchmarkItemBean.setSequenceNumber(generateSequenceNumber(benchmarkItemBean.getBenchmark().getId()));			
		}
		if("".equals(benchmarkItemBean.getDisplayName())){
			String displayName = benchmarkItemBean.getBaseName() +" "+benchmarkItemBean.getVersion();
			benchmarkItemBean.setDisplayName(displayName);
		}
		return 	benchmarkItemRepository.save(benchmarkItemBean);
	}
	
	@Override
	public int generateSequenceNumber(Integer benchMarkId) {
		// find Max sequence number in asset template columns
		List<BenchmarkItem> benchmarkItemsList = benchmarkItemRepository.findByBenchmarkId(benchMarkId);
		int maxSequenceNumber = 0;
	
		for(BenchmarkItem oneB : benchmarkItemsList){		
		    int currentSequenceNumber = oneB.getSequenceNumber();
			if(maxSequenceNumber < currentSequenceNumber)
					maxSequenceNumber = currentSequenceNumber;
		}
		return maxSequenceNumber+1;
	}

	@Override
	public void deleteBenchmarkItem(Integer idOfBenchmarkItem) {
		BenchmarkItem benchmarkItem =  benchmarkItemRepository.findOne(idOfBenchmarkItem);
		if(!benchmarkItem.getBenchmarkItemScores().isEmpty()){
			benchmarkItemScoreRepository.deleteInBatch(benchmarkItem.getBenchmarkItemScores());
		}
		benchmarkItemRepository.delete(benchmarkItem);
		
	}	
	
}
