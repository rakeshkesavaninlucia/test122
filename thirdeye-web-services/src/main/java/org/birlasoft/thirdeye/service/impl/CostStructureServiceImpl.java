package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.CostStructureService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 *
 */
@Service("costStructureService")
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class CostStructureServiceImpl implements CostStructureService {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ParameterFunctionService parameterFunctionService;
	@Autowired
	private ParameterService parameterService;

	@Override
	public Parameter createNewCostStructure(ParameterBean parameterBean) {
		Parameter parameter = new Parameter();
		parameter.setUniqueName(parameterBean.getUniqueName());
		parameter.setDisplayName(parameterBean.getDisplayName());
		parameter.setDescription(parameterBean.getDescription());
		parameter.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		parameter.setType(ParameterType.TCOA.name());
		parameter.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		parameter.setCreatedDate(new Date());
		parameter.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		parameter.setUpdatedDate(new Date());
		
		return parameter;
	}

	@Override
	public int getMaxSequenceNumber(List<ParameterFunction> listOfPfs) {
		int maxSequenceNumber = 0;
		for (ParameterFunction parameterFunction : listOfPfs) {
			if (parameterFunction.getSequenceNumber().intValue() > maxSequenceNumber){
				maxSequenceNumber = parameterFunction.getSequenceNumber().intValue();
			}
		}
		return maxSequenceNumber;
	}

	@Override
	public List<Question> extractCostElementToDisplay(List<Question> listCostElement, List<ParameterFunction> alreadyAddedCostElements) {
		
		List<Question> fullListOfCostElement = new ArrayList<>();
		fullListOfCostElement.addAll(listCostElement);
		
		// Create list for already added cost elements
		List<Question> addedCostElements = new ArrayList<>();
		if(!alreadyAddedCostElements.isEmpty()){
			for (ParameterFunction pf : alreadyAddedCostElements) {
				Question question = new Question();
				question.setId(pf.getQuestion().getId());
				addedCostElements.add(question);
			}
		}
		
		// Extract cost elements to remove from full list
		fullListOfCostElement.removeAll(addedCostElements);
		
		return fullListOfCostElement;
	}

	@Override
	public Parameter updateCostStructure(ParameterBean incomingCostStructure, User currentUser) {
		Parameter parameter = parameterService.findOne(incomingCostStructure.getId());
		parameter.setDisplayName(incomingCostStructure.getDisplayName());
		parameter.setDescription(incomingCostStructure.getDescription());
		parameter.setUpdatedDate(new Date());
		parameter.setUserByUpdatedBy(currentUser);
		return parameter;
	}

	@Override
	public ParameterFunction createNewParameterFunction(ParameterBean incomingCostStructure, Parameter parameter) {
		Parameter selectedParameter = parameterService.findOne(incomingCostStructure.getBaseParameterId());
		ParameterFunction pf = new ParameterFunction();
		pf.setParameterByParentParameterId(selectedParameter);
		pf.setParameterByChildparameterId(parameter);
		pf.setWeight(Constants.DEFAULT_WEIGHT_VALUE);
		List<ParameterFunction> listOfParameterFunctions = parameterFunctionService.findByParameterByParentParameterId(selectedParameter);
		int maxSequenceNumber = getMaxSequenceNumber(listOfParameterFunctions);
		maxSequenceNumber++;
		pf.setSequenceNumber(maxSequenceNumber);
		return pf;
	}
	
	@Override
	public void deleteParamFunctionByParent(Parameter parameter) {
		if(parameter.getType().equals(ParameterType.TCOA.name()) || parameter.getType().equals(ParameterType.TCOL.name())){
			List<ParameterFunction> parameterFunctions = parameterFunctionService.findByParameterByParentParameterId(parameter);
			parameterFunctionService.deleteInBatch(parameterFunctions);
		}
	}

	@Override
	public void deleteParamFunctionByChild(Parameter parameter) {
		Set<ParameterFunction> parameterFunctionToUpdate = parameterFunctionService.findByParameterByChildparameterId(parameter);
		parameterFunctionService.deleteInBatch(parameterFunctionToUpdate);
	}
}
