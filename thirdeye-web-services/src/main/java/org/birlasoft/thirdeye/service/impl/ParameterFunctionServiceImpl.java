package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ParameterFunctionBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.repositories.ParameterFunctionRepository;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} of {@link ParameterFunction} to {@code save, update, delete} , and 
 * find existing.
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ParameterFunctionServiceImpl implements ParameterFunctionService {
	
	@Autowired
	private ParameterFunctionRepository parameterFunctionRepository;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private ParameterService parameterService;
	
	public ParameterFunctionServiceImpl() {
		//Default constructor
	}

	@Override
	public ParameterFunction save(ParameterFunction parameterFunction) {
		return parameterFunctionRepository.save(parameterFunction);
	}

	@Override
	public List<ParameterFunction> findAll() {
		return parameterFunctionRepository.findAll();
	}
	
	@Override
	public List<ParameterFunction> save(Iterable<ParameterFunction> parameter) {
		List<ParameterFunction> listOfFunctions = parameterFunctionRepository.save(parameter);
		
		List<Integer> idList = new ArrayList<>();
		
		for (ParameterFunction onePf : listOfFunctions){
			idList.add(onePf.getId());
		}
		listOfFunctions = parameterFunctionRepository.findAll(idList);
		for (ParameterFunction onePf : listOfFunctions){
			if (onePf.getQuestion() != null){
				onePf.getQuestion().getTitle();
			}
			if (onePf.getParameterByChildparameterId() != null){
				onePf.getParameterByChildparameterId().getUniqueName();
			}
		}
				
		return listOfFunctions;
	}

	@Override
	public ParameterFunction findOne(Integer id) {
		return parameterFunctionRepository.findOne(id);
	}

	@Override
	public List<ParameterFunction> findByParameterByParentParameterId(Parameter parameter) {
		List<ParameterFunction> parameterFunctions = parameterFunctionRepository.findByParameterByParentParameterId(parameter);
		for (ParameterFunction parameterFunction : parameterFunctions) {
			if (parameterFunction.getQuestion() != null){
				parameterFunction.getQuestion().getTitle();
			}
			if (parameterFunction.getParameterByChildparameterId() != null){
				parameterFunction.getParameterByChildparameterId().getUniqueName();
			}
		}
		return parameterFunctions;
	}

	@Override
	public ParameterFunction createNewParameterFunctionObject(Parameter savedParam, Parameter parentParameter) {
		// Create the parameter function entry
		ParameterFunction newParameterFunction = new ParameterFunction();
		newParameterFunction.setParameterByParentParameterId(parentParameter);
		newParameterFunction.setParameterByChildparameterId(savedParam);
		newParameterFunction.setWeight(Constants.DEFAULT_WEIGHT_VALUE);
		newParameterFunction.setConstant(Constants.DEFAULT_CONSTANT_VALUE);
		return newParameterFunction;
	}

	@Override
	public List<ParameterFunction> getListOfParameterFunctionsToBeSaved(Set<Integer> setOfQuestions, Parameter parameter, Set<Integer> setOfMandatoryQuestion) {
		List<ParameterFunction> listOfFunctionsToBeSaved =  new ArrayList<>();
		for(Integer question : setOfQuestions){
			ParameterFunction parameterFunction = new ParameterFunction();
			parameterFunction.setParameterByParentParameterId(parameter);
			parameterFunction.setQuestion(questionService.findOne(question));
			if(setOfMandatoryQuestion != null && setOfMandatoryQuestion.contains(question)){
				parameterFunction.setMandatoryQuestion(true);
			}
			parameterFunction.setWeight(Constants.DEFAULT_WEIGHT_VALUE);
			parameterFunction.setConstant(Constants.DEFAULT_CONSTANT_VALUE);
			listOfFunctionsToBeSaved.add(parameterFunction);
		}
		
		return listOfFunctionsToBeSaved;
	}

	@Override
	public ParameterFunction updateParameterFunctionObject(ParameterFunctionBean functionBean, ParameterFunction onePf) {
		if (functionBean.getWeight() != null) {
			onePf.setWeight(new BigDecimal(functionBean.getWeight()));
		} else if (functionBean.getConstant() != null){
			onePf.setConstant(functionBean.getConstant());
		}
		return onePf;
	}

	@Override
	public List<ParameterFunction> findByMandatoryQuestion(boolean isMandatoryQuestion) {
		List<ParameterFunction> parameterFunctions = parameterFunctionRepository.findByMandatoryQuestion(isMandatoryQuestion);
		
		for (ParameterFunction parameterFunction : parameterFunctions) {
			parameterFunction.getQuestion().getId();
			parameterFunction.getParameterByParentParameterId().getId();
		}
		return parameterFunctions;
	}

	@Override
	public String getParameterTree(Parameter parameter) {
		return parameterFunctionRepository.getParameterTree(parameter.getId());
	}

	@Override
	public List<ParameterFunction> findByParameterByParentParameterIdAndParameterByChildparameterIdIsNull(Parameter parameter) {
		List<ParameterFunction> parameterFunctions = parameterFunctionRepository.findByParameterByParentParameterIdAndParameterByChildparameterIdIsNull(parameter);
		for (ParameterFunction parameterFunction : parameterFunctions) {
			if (parameterFunction.getQuestion() != null){
				parameterFunction.getQuestion().getTitle();
			}
		}
		return parameterFunctions;
	}

	@Override
	public void deleteInBatch(List<ParameterFunction> parameterFunctions) {
		parameterFunctionRepository.deleteInBatch(parameterFunctions);
	}

	@Override
	public void saveOrUpdateParameterFuntion(ParameterBean parameterBean,
			Map<Integer, BigDecimal> map, Parameter savedParameter,
			Set<Integer> mandatoryQuestionIds) {
		Set<Integer> queOrParamIds = new HashSet<>();
		for (Map.Entry<Integer, BigDecimal> entry : map.entrySet()){
			queOrParamIds.add(entry.getKey());
		}

		if(!queOrParamIds.isEmpty()){
				List<ParameterFunction> functionsToSave = new ArrayList<>();
				if(parameterBean.getId() == null){
					if(savedParameter.getType().equals(ParameterType.AP.name())){
						generateListOfParamFunctionForAggregateParam(map, savedParameter, queOrParamIds, functionsToSave);
					}else if(savedParameter.getType().equals(ParameterType.LP.name())){
						generateListOfParamFunctionForLeafParam(map, savedParameter, mandatoryQuestionIds, queOrParamIds, functionsToSave);
					}
				}else if (parameterBean.getId() != null && (savedParameter.getType().equals(ParameterType.AP.name()) || savedParameter.getType().equals(ParameterType.LP.name()))){
					updateParameterFunctionWeights(map, savedParameter, functionsToSave);
				}
			this.save(functionsToSave);
		}
	}

	/**
	 * Update weights for AP and LP
	 * @param map
	 * @param savedParameter
	 * @param functionsToSave
	 */
	private void updateParameterFunctionWeights(Map<Integer, BigDecimal> map, 
			Parameter savedParameter, List<ParameterFunction> functionsToSave) {
		List<ParameterFunction> functionsFromDB = this.findByParameterByParentParameterId(savedParameter);
		for (ParameterFunction pf : functionsFromDB) {
			if(pf.getQuestion() != null){
				pf.setWeight(map.get(pf.getQuestion().getId()));
			}else if(pf.getParameterByChildparameterId() != null){
				pf.setWeight(map.get(pf.getParameterByChildparameterId().getId()));
			}
			functionsToSave.add(pf);
		}
	}

	/**
	 * Create a list of Parameter function for Leaf parameter
	 * @param map
	 * @param savedParameter
	 * @param mandatoryQuestionIds
	 * @param queOrParamIds
	 * @param functionsToSave
	 */
	private void generateListOfParamFunctionForLeafParam(Map<Integer, BigDecimal> map, Parameter savedParameter,
			Set<Integer> mandatoryQuestionIds, Set<Integer> queOrParamIds, List<ParameterFunction> functionsToSave) {
		List<Question> questions = questionService.findByIdIn(queOrParamIds);
		List<Question> mandatoryQuestions = questionService.findByIdIn(mandatoryQuestionIds);
		for (Question q : questions) {
			ParameterFunction pf = new ParameterFunction();
			pf.setParameterByParentParameterId(savedParameter);
			pf.setWeight(map.get(q.getId()));
			pf.setQuestion(q);
			pf.setMandatoryQuestion(mandatoryQuestions.contains(q));
			functionsToSave.add(pf);
		}
	}

	/**
	 * Create a list of Parameter function for Aggregate parameter
	 * @param map
	 * @param savedParameter
	 * @param queOrParamIds
	 * @param functionsToSave
	 */
	private void generateListOfParamFunctionForAggregateParam(Map<Integer, BigDecimal> map, Parameter savedParameter,
			Set<Integer> queOrParamIds, List<ParameterFunction> functionsToSave) {
		List<Parameter> parameters = parameterService.findByIdIn(queOrParamIds);
		for (Parameter p : parameters) {
			ParameterFunction pf = new ParameterFunction();
			pf.setParameterByParentParameterId(savedParameter);
			pf.setWeight(map.get(p.getId()));
			pf.setParameterByChildparameterId(p);
			functionsToSave.add(pf);
		}
	}
	
	@Override
	public Set<Parameter> findByParameterByParentParameterIdIn(Set<Parameter> parameters) {
		return parameterFunctionRepository.findByParameterByParentParameterIdIn(parameters);
	}

	@Override
	public Set<ParameterFunction> findByParameterByChildparameterId(Parameter param) {
		return parameterFunctionRepository.findByParameterByChildparameterId(param);
	}

	@Override
	public void deleteInBatch(Set<ParameterFunction> parameterFunctionToDelete) {
		parameterFunctionRepository.deleteInBatch(parameterFunctionToDelete);
	}
}
