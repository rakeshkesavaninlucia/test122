package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ParameterSearchServiceImpl implements ParameterSearchService {
	
	private static Logger logger = LoggerFactory.getLogger(ParameterSearchServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Override
	public AssetParameterWrapper getParameterValueFromElasticsearch(Integer parameterId, Integer questionnaireId,
			Set<AssetBean> assetList, Map<String, List<String>> filterMap) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/param/value";

		RestTemplate restTemplate = new RestTemplate();
		AssetParameterWrapper assetParameterWrapper = new AssetParameterWrapper();

		//Setting the search config object
		List<Integer> listOfIds = new ArrayList<>();
		for (AssetBean oneAsset : assetList) {
			listOfIds.add(oneAsset.getId());
		}
		SearchConfig searchConfig = setSearchConfig(listOfIds, parameterId, questionnaireId);
		searchConfig.setFilterMap(filterMap);
		
		//Firing a rest request to elastic search server
		try {
			assetParameterWrapper = restTemplate.postForObject(uri, searchConfig, AssetParameterWrapper.class);
		} catch (RestClientException e) {				
			logger.error("Exception occured in GraphFacetsServiceImpl :: getParameterValueFromElasticsearch() :", e);
		}
		return assetParameterWrapper;
	}
	
	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return
	 */
	private SearchConfig setSearchConfig(List<Integer> listOfIds, Integer parameterId, Integer questionnaireId) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		
		searchConfig.setListOfIds(listOfIds);
		searchConfig.setParameterId(parameterId);
		searchConfig.setQuestionnaireId(questionnaireId);
		return searchConfig;
	}
}
