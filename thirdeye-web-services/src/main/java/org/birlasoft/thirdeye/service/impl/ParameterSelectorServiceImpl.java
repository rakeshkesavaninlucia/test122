package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterSelectorBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterSelectorService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for parameter selection on Questionnaire
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ParameterSelectorServiceImpl implements ParameterSelectorService {
	
	
	private ParameterService parameterService;
	private QuestionnaireParameterService qpService;
	private QuestionnaireService questionnaireService;
	private CustomUserDetailsService customUserDetailsService; 

	/**
	 * Constructor for initialized the services
	 * @param parameterService
	 * @param qpService
	 * @param questionnaireService
	 * @param customUserDetailsService
	 */
    @Autowired
	public ParameterSelectorServiceImpl(ParameterService parameterService,
			QuestionnaireParameterService qpService,
			QuestionnaireService questionnaireService,
			CustomUserDetailsService customUserDetailsService) {
	
		this.parameterService = parameterService;
		this.qpService = qpService;
		this.questionnaireService = questionnaireService;
		this.customUserDetailsService = customUserDetailsService;
	}


	@Override
	public List<Parameter> getModalParameterForQuestionnaire(ParameterSelectorBean parameterSelectorBean) {

		List<Parameter> listOfParameter = getQuestionnaireParameter(parameterSelectorBean);	
		
		Set<Integer> idsOfParameter = qpService.getParameterIds(questionnaireService.findOne(parameterSelectorBean.getQuestionnaireId()));
		List<Parameter> parameterList = parameterService.findFullyLoadedParametersByIds(idsOfParameter) ;
		listOfParameter.removeIf(x -> parameterList.contains(x));		
	
		return listOfParameter;
	
	}


	private List<Parameter> getQuestionnaireParameter(
			ParameterSelectorBean parameterSelectorBean) {
		Integer workspaceId =  customUserDetailsService.getActiveWorkSpaceForUser().getId();
		List<Parameter> listOfParameter = new ArrayList<>();
		List<String> types = getParameterSelectorType(parameterSelectorBean);
		if(workspaceId == -1 || workspaceId == null){	     	
			listOfParameter = parameterService.findByWorkspaceIsNullAndType(types);
		}else if(workspaceId > 0){
			listOfParameter = parameterService.findByWorkspaceIsNullOrWorkspaceAndType(customUserDetailsService.getActiveWorkSpaceForUser(),types);
		}
		
		
		return listOfParameter;
	}


	private List<String> getParameterSelectorType(ParameterSelectorBean parameterSelectorBean) {
		List<String> types = new ArrayList<>();
		if(parameterSelectorBean.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.DEF.toString())){
			types.add(ParameterType.AP.toString());
			types.add(ParameterType.LP.toString());			
		}else{
			if(parameterSelectorBean.getQuestionnaireType().equalsIgnoreCase(QuestionnaireType.TCO.toString())){
				types.add(ParameterType.TCOA.toString());
			}
		}
		return types;
	}

}
