package org.birlasoft.thirdeye.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireLifeCycleService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to implement Questionnaire Life Cycle methods
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireLifeCycleServiceImpl implements QuestionnaireLifeCycleService {
	
	private static Logger logger = LoggerFactory.getLogger(QuestionnaireLifeCycleServiceImpl.class);
	
    @Autowired
	private ParameterService parameterService;
    @Autowired
    private QuestionnaireQuestionService questionnaireQuestionService;
    
	@Override
	public List<ParameterBean> getRootParameterForQuestionnaire(Questionnaire qe) {
		List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe,true) ;	
		// Get root parameters of QE
		return parameterService.getParametersOfQuestionnaire(parameterList);
	}

	@Override
	public Map<String, Integer> getMapOfCategoryAndNoOfQuetion(Questionnaire qe) {
		List<QuestionnaireQuestion> questionsToBeDisplayed = questionnaireQuestionService.findByQuestionnaire(qe, true);
		Collections.sort(questionsToBeDisplayed, new SequenceNumberComparator());
		Map<String,Integer> mapOfQC = new HashMap<>();
		for (QuestionnaireQuestion oneQQ : questionsToBeDisplayed){
			QuestionBean qBean= new QuestionBean(oneQQ.getQuestion());
			if(!mapOfQC.containsKey(qBean.getCategory())){
				mapOfQC.put(qBean.getCategory(), 1);	
			}else{
			    mapOfQC.put(qBean.getCategory(), mapOfQC.get(qBean.getCategory()) + 1);
			}
		}
		return mapOfQC;
	}
	

}
