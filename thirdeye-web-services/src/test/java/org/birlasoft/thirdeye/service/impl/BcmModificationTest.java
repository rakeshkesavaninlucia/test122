/*
//JUnit for bcm level changes
package org.birlasoft.thirdeye.service.impl;


import java.util.Date;

import org.junit.Assert;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.repositories.BCMLevelRepository;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BcmModificationTest {	

	@InjectMocks
	private BCMLevelService bcmLevelservice = new BCMLevelServiceImpl();
	@Mock
	private BCMService bcmService;
	@Mock
	private CustomUserDetailsService customUserDetailsService;
	@Mock
	private BCMLevelRepository bcmLevelRepository;

	private Bcm dummyBcm; 
	private BcmLevel bcmlevel1;
	private BcmLevel bcmlevel1_1;
	private BcmLevel bcmlevel2;
	private BcmLevel bcmlevel3;
	private BcmLevel bcmlevel4;
	private BcmLevel bcmlevel5;
	private BcmLevelBean bcmlevelbean1;
	private BcmLevelBean bcmlevelbean1_1;
	private BcmLevelBean bcmlevelbean2;
	private BcmLevelBean bcmlevelbean3;
	private BcmLevelBean bcmlevelbean4;
	private BcmLevelBean bcmlevelbean5;

	@Before
	public void setup() {
		setupBcmLevelOneBean();
		setupBcmLevelOneUpdateBean();
		setupBcmLevelTwoBean();
		setupBcmLevelThreeBean();
		setupBcmLevelFourBean();
		setupBcmLevelFiveBean();
	}
	private void setUpBCm()
	{
		dummyBcm = new Bcm();  
		dummyBcm.setId(1);
		dummyBcm.setBcmName("Dummy BCM");
	}
	private void setupBcmLevelOneBean() {
		setUpBCm();
		bcmlevelbean1 = new BcmLevelBean();		
		bcmlevelbean1.setId(null);
		//bcmlevelbean1.setId(1);
		bcmlevelbean1.setLevelNumber(1);		
		bcmlevelbean1.setBcmLevelName("levelOne");
		bcmlevelbean1.setDescription("level 1 Description");
		bcmlevelbean1.setSequenceNumber(1);
		bcmlevelbean1.setParent(null);
		//bcmlevelbean1.setCategory("");
		setupBcmLevel1(bcmlevelbean1);
	}
	private void setupBcmLevelOneUpdateBean() {
		setUpBCm();
		bcmlevelbean1_1 = new BcmLevelBean();		
		bcmlevelbean1_1.setId(1);
		bcmlevelbean1_1.setLevelNumber(1);		
		bcmlevelbean1_1.setBcmLevelName("levelOne");
		bcmlevelbean1_1.setDescription("level 1 Description");
		bcmlevelbean1_1.setSequenceNumber(1);
		bcmlevelbean1_1.setParent(null);
		//bcmlevelbean1.setCategory("");
		setupBcmLevel1update(bcmlevelbean1_1);
	}
	private void setupBcmLevelTwoBean() {
		//LEVEL 2

		bcmlevelbean2 = new BcmLevelBean();
		setUpBCm();
		bcmlevelbean2.setParent(bcmlevelbean1);
		bcmlevelbean2.setId(12);
		bcmlevelbean2.setLevelNumber(2);
		bcmlevelbean2.setBcmLevelName("level 2");
		bcmlevelbean2.setCategory("front end");
		bcmlevelbean2.setDescription("level 2 Description");
		bcmlevelbean2.setSequenceNumber(1);
		setupBcmLevel2(bcmlevelbean2);
	}
	private void setupBcmLevelThreeBean() {
		//LEVEL 3

		bcmlevelbean3 = new BcmLevelBean();
		setUpBCm();
		bcmlevelbean3.setParent(bcmlevelbean2);
		bcmlevelbean3.setId(13);
		bcmlevelbean3.setLevelNumber(3);
		bcmlevelbean3.setBcmLevelName("Level 3");
		bcmlevelbean3.setDescription("level 3 description");
		bcmlevelbean3.setSequenceNumber(1);
		setupBcmLevel3(bcmlevelbean3);
	}
	private void setupBcmLevelFourBean() {
		//LEVEL 4
		bcmlevelbean4 = new BcmLevelBean();
		setUpBCm();
		bcmlevelbean4.setParent(bcmlevelbean3);
		bcmlevelbean4.setId(14);
		bcmlevelbean4.setLevelNumber(4);
		bcmlevelbean4.setBcmLevelName("level 4");
		bcmlevelbean4.setDescription("level 4 description");
		bcmlevelbean4.setSequenceNumber(1);
		setupBcmLevel4(bcmlevelbean4);
	}

	private void setupBcmLevelFiveBean() {
		//LEVEL 2-1 under level 1
		bcmlevelbean5 = new BcmLevelBean();
		setUpBCm();
		bcmlevelbean5.setParent(bcmlevelbean1);
		bcmlevelbean5.setId(15);
		bcmlevelbean5.setLevelNumber(2);
		bcmlevelbean5.setCategory("back office");
		bcmlevelbean5.setBcmLevelName("level two under level 1");
		bcmlevelbean5.setDescription("level two under level 1 description");
		bcmlevelbean5.setSequenceNumber(2);
		setupBcmLevel5(bcmlevelbean5);
	}
	private void setupBcmLevel1(BcmLevelBean bcmLevelBean) {

		bcmlevel1 = new BcmLevel();
		bcmlevel1.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel1.setDescription(bcmLevelBean.getDescription());
		bcmlevel1.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel1.setId(bcmLevelBean.getId());
		bcmlevel1.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel1.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel1.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel1.setUpdatedDate(new Date());
		bcmlevel1.setCreatedDate(new Date());
		bcmlevel1.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel1.setCategory(null);
	}
	private void setupBcmLevel1update(BcmLevelBean bcmLevelBean) {

		bcmlevel1_1 = new BcmLevel();
		bcmlevel1_1.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel1_1.setDescription(bcmLevelBean.getDescription());
		bcmlevel1_1.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel1_1.setId(bcmLevelBean.getId());
		bcmlevel1_1.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel1_1.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel1_1.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel1_1.setUpdatedDate(new Date());
		bcmlevel1_1.setCreatedDate(new Date());
		bcmlevel1_1.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel1_1.setCategory(null);
	}
	private void setupBcmLevel2(BcmLevelBean bcmLevelBean) {

		bcmlevel2 = new BcmLevel();
		bcmlevel2.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel2.setDescription(bcmLevelBean.getDescription());
		bcmlevel2.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel2.setId(bcmLevelBean.getId());
		bcmlevel2.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel2.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel2.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel2.setUpdatedDate(new Date());
		bcmlevel2.setCreatedDate(new Date());
		bcmlevel2.setCategory(bcmLevelBean.getCategory());

	}
	private void setupBcmLevel3(BcmLevelBean bcmLevelBean) {

		bcmlevel3 = new BcmLevel();
		bcmlevel3.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel3.setDescription(bcmLevelBean.getDescription());
		bcmlevel3.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel3.setId(bcmLevelBean.getId());
		bcmlevel3.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel3.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel3.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel3.setUpdatedDate(new Date());
		bcmlevel3.setCreatedDate(new Date());
	}
	private void setupBcmLevel4(BcmLevelBean bcmLevelBean) {

		bcmlevel4 = new BcmLevel();
		bcmlevel4.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel4.setDescription(bcmLevelBean.getDescription());
		bcmlevel4.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel4.setId(bcmLevelBean.getId());
		bcmlevel4.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel4.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel4.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel4.setUpdatedDate(new Date());
		bcmlevel4.setCreatedDate(new Date());
	}
	private void setupBcmLevel5(BcmLevelBean bcmLevelBean) {

		bcmlevel5 = new BcmLevel();
		bcmlevel5.setLevelNumber(bcmLevelBean.getLevelNumber());
		bcmlevel5.setDescription(bcmLevelBean.getDescription());
		bcmlevel5.setSequenceNumber(bcmLevelBean.getSequenceNumber());
		bcmlevel5.setId(bcmLevelBean.getId());
		bcmlevel5.setBcmLevelName(bcmLevelBean.getBcmLevelName());
		bcmlevel5.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel5.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		bcmlevel5.setUpdatedDate(new Date());
		bcmlevel5.setCreatedDate(new Date());
		bcmlevel5.setCategory(bcmLevelBean.getCategory());

	}
	private void mockServiceClassMethods() {
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean2.getParent().getId())).thenReturn(bcmlevel1);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean3.getParent().getId())).thenReturn(bcmlevel2);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean4.getParent().getId())).thenReturn(bcmlevel3);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean5.getParent().getId())).thenReturn(bcmlevel1);
		
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean1.getId())).thenReturn(bcmlevel1);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean2.getId())).thenReturn(bcmlevel2);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean1_1.getId())).thenReturn(bcmlevel1_1);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean3.getId())).thenReturn(bcmlevel3);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean4.getId())).thenReturn(bcmlevel4);
		Mockito.when(bcmLevelRepository.findOne(bcmlevelbean5.getId())).thenReturn(bcmlevel5);

	}
	@Test
	public void testsaveBcmlevel1()
	{
		Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean1,dummyBcm.getId(),null)).thenReturn(bcmlevel1);
		//Mockito.when(bcmLevelRepository.findOne(bcmlevelbean1.getId())).thenReturn(bcmlevel1);
		Assert.assertNotNull(bcmlevel1);
		Assert.assertEquals(bcmlevelbean1.getId(), bcmlevel1.getId());
		Assert.assertEquals(bcmlevelbean1.getSequenceNumber().intValue(), bcmlevel1.getSequenceNumber());
		Assert.assertEquals(bcmlevelbean1.getLevelNumber(), bcmlevel1.getLevelNumber());
	}

	@Test
	public void testsaveBcmlevel2()
	{
		mockServiceClassMethods();
		Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean2,dummyBcm.getId(),1)).thenReturn(bcmlevel2);
		Assert.assertNotNull(bcmlevel2);
		Assert.assertEquals(bcmlevelbean2.getId(), bcmlevel2.getId());
		Assert.assertEquals(bcmlevelbean2.getSequenceNumber().intValue(), bcmlevel2.getSequenceNumber());
		Assert.assertEquals(bcmlevelbean2.getLevelNumber(), bcmlevel2.getLevelNumber());
	}

	@Test
	public void testsaveBcmlevel1update()
	{
		mockServiceClassMethods();
		Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean1_1,dummyBcm.getId(),null)).thenReturn(bcmlevel1_1);
		//Mockito.when(bcmLevelRepository.findOne(bcmlevelbean1.getId())).thenReturn(bcmlevel1);
		Assert.assertNotNull(bcmlevel1_1);
		Assert.assertEquals(bcmlevelbean1_1.getId(), bcmlevel1_1.getId());
		Assert.assertEquals(bcmlevelbean1_1.getSequenceNumber().intValue(), bcmlevel1_1.getSequenceNumber());
		Assert.assertEquals(bcmlevelbean1_1.getLevelNumber(), bcmlevel1_1.getLevelNumber());
	}
	@Test
	public void testsaveBcmlevel3()
	{
		mockServiceClassMethods();
		Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean3,dummyBcm.getId(),null)).thenReturn(bcmlevel3);
		//Mockito.when(bcmLevelRepository.findOne(bcmlevelbean1.getId())).thenReturn(bcmlevel1);
		Assert.assertNotNull(bcmlevel3);
		Assert.assertEquals(bcmlevelbean3.getId(), bcmlevel3.getId());
		Assert.assertEquals(bcmlevelbean3.getSequenceNumber().intValue(), bcmlevel3.getSequenceNumber());
		Assert.assertEquals(bcmlevelbean3.getLevelNumber(), bcmlevel3.getLevelNumber());
	}
		@Test
		public void testsaveBcmlevel4()
		{
			mockServiceClassMethods();
			Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean4,dummyBcm.getId(),3)).thenReturn(bcmlevel4);
			Assert.assertNotNull(bcmlevel4);
			Assert.assertEquals(bcmlevelbean4.getId(), bcmlevel4.getId());
			Assert.assertEquals(bcmlevelbean4.getSequenceNumber().intValue(), bcmlevel4.getSequenceNumber());
			Assert.assertEquals(bcmlevelbean4.getLevelNumber(), bcmlevel4.getLevelNumber());
		}
		@Test
		public void testsaveBcmlevel1_2()
		{
			mockServiceClassMethods();
			Mockito.when(bcmLevelservice.saveBcmLevel(bcmlevelbean5,dummyBcm.getId(),1)).thenReturn(bcmlevel5);
			Assert.assertNotNull(bcmlevel5);
			Assert.assertEquals(bcmlevelbean5.getId(), bcmlevel5.getId());
			Assert.assertEquals(bcmlevelbean5.getSequenceNumber().intValue(), bcmlevel5.getSequenceNumber());
			Assert.assertEquals(bcmlevelbean5.getLevelNumber(), bcmlevel5.getLevelNumber());
		}
}
*/