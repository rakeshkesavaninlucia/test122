-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `3rdi`.`relationship_suggestion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` INT(11) NOT NULL,
  `relationshipTypeId` INT(11) NOT NULL,
  `childAssetTypeId` INT(11) NOT NULL,
  `displayName` VARCHAR(150) NOT NULL,
  `workspaceId` INT(11) NULL DEFAULT NULL,
  `deleteStatus` TINYINT(1) NOT NULL DEFAULT '0',
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `rs_displayName_workspaceId_uq` (`workspaceId` ASC, `displayName` ASC),
  INDEX `rs_parentAssetTypeId_idx` (`parentAssetTypeId` ASC),
  INDEX `rs_childAssetTypeId_idx` (`childAssetTypeId` ASC),
  INDEX `rs_relationshipTypeId_idx` (`relationshipTypeId` ASC),
  INDEX `rs_workspaceId_idx` (`workspaceId` ASC),
  INDEX `rs_createdBy_idx` (`createdBy` ASC),
  INDEX `rs_updatedBy_idx` (`updatedBy` ASC),
  CONSTRAINT `rs_childAssetTypeId`
    FOREIGN KEY (`childAssetTypeId`)
    REFERENCES `3rdi`.`asset_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rs_createdBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rs_parentAssetTypeId`
    FOREIGN KEY (`parentAssetTypeId`)
    REFERENCES `3rdi`.`asset_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rs_relationshipTypeId`
    FOREIGN KEY (`relationshipTypeId`)
    REFERENCES `3rdi`.`relationship_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rs_updatedBy`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rs_workspaceId`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `3rdi`.`workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

ALTER TABLE `3rdi`.`relationship_type` 
ADD COLUMN `workspaceId` INT(11) NULL DEFAULT NULL AFTER `childDescriptor`,
ADD COLUMN `deleteStatus` TINYINT(1) NOT NULL DEFAULT '0' AFTER `workspaceId`,
ADD COLUMN `displayName` VARCHAR(150) NOT NULL AFTER `deleteStatus`,
ADD UNIQUE INDEX `relationship_type_displayName_workspaceId_uq` (`workspaceId` ASC, `displayName` ASC),
ADD INDEX `relationship_type_workspaceId_idx` (`workspaceId` ASC),
DROP INDEX `relationship_type_uq_parentDesc_childDesc` ;

ALTER TABLE `3rdi`.`relationship_type` 
ADD CONSTRAINT `relationship_type_workspaceId`
  FOREIGN KEY (`workspaceId`)
  REFERENCES `3rdi`.`workspace` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
 CREATE TABLE `relationship_suggestion_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributeName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(5) DEFAULT NULL,
  `relationshipSuggestionId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ra_uq_attributeName_rSuggestionId1` (`relationshipSuggestionId`,`attributeName`),
  KEY `ra_relationshipSuggestionId_idx` (`relationshipSuggestionId`),
  KEY `ra_createdBy_fk_idx` (`createdBy`),
  KEY `ra_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
