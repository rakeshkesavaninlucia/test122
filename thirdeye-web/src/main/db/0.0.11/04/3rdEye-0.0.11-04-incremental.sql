-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `3rdi`.`questionnaire_question` 
DROP FOREIGN KEY `questionnaire_question_parameterId_fk`;
ALTER TABLE `3rdi`.`questionnaire_question` 
DROP COLUMN `parameterId`,
DROP INDEX `questionnaire_question_parameterId_fk_idx` ,
DROP INDEX `questionnaire_question_questionnaireId_questionId_parameterId` ;

ALTER TABLE `3rdi`.`questionnaire_question` 
ADD COLUMN `questionnaireParameterId` INT(11) NOT NULL AFTER `questionnaireAssetId`,
ADD INDEX `qq_questionnaireParameterId_fk_idx` (`questionnaireParameterId` ASC),
ADD UNIQUE INDEX `qq_QEId_QId_QAId_QPId_uq` (`questionnaireId` ASC, `questionId` ASC, `questionnaireAssetId` ASC, `questionnaireParameterId` ASC);
ALTER TABLE `3rdi`.`questionnaire_question` 
ADD CONSTRAINT `qq_questionnaireParameterId_fk`
  FOREIGN KEY (`questionnaireParameterId`)
  REFERENCES `3rdi`.`questionnaire_parameter` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `3rdi`.`questionnaire_parameter` 
DROP FOREIGN KEY `qp_rootParameterId_fk`;
ALTER TABLE `3rdi`.`questionnaire_parameter` 
CHANGE COLUMN `rootParameterId` `rootParameterId` INT(11) NOT NULL ,
DROP INDEX `qp_questionnaireId_parameterId_parentParameterId_uq` ,
ADD UNIQUE INDEX `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId` ASC, `parameterId` ASC, `parentParameterId` ASC, `rootParameterId` ASC);
ALTER TABLE `3rdi`.`questionnaire_parameter` 
ADD CONSTRAINT `qp_rootParameterId_fk`
  FOREIGN KEY (`rootParameterId`)
  REFERENCES `3rdi`.`parameter` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
CREATE TABLE IF NOT EXISTS `3rdi`.`functional_map` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `bcmId` INT(11) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `workspaceId` INT(11) NOT NULL,
  `assetTemplateId` INT(11) NOT NULL,
  `questionId` INT(11) NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fm_bcm_idx` (`bcmId` ASC),
  INDEX `fm_createdBy_idx` (`createdBy` ASC),
  INDEX `fm_updatedBy_idx` (`updatedBy` ASC),
  INDEX `fm_workspaceId_idx` (`workspaceId` ASC),
  INDEX `fm_assetTemplate_idx` (`assetTemplateId` ASC),
  INDEX `fm_question_idx` (`questionId` ASC),
  CONSTRAINT `fm_assetTemplate`
    FOREIGN KEY (`assetTemplateId`)
    REFERENCES `3rdi`.`asset_template` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fm_bcm`
    FOREIGN KEY (`bcmId`)
    REFERENCES `3rdi`.`bcm` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fm_createdBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fm_question`
    FOREIGN KEY (`questionId`)
    REFERENCES `3rdi`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fm_updatedBy`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fm_workspaceId`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `3rdi`.`workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS `3rdi`.`functional_map_data` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `functionalMapId` INT(11) NOT NULL,
  `bcmLevelId` INT(11) NOT NULL,
  `assetId` INT(11) NOT NULL,
  `value` DECIMAL(2,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fmd_bmfId_idx` (`functionalMapId` ASC),
  INDEX `fmd_bcmLevel_idx` (`bcmLevelId` ASC),
  INDEX `fmd_asset_idx` (`assetId` ASC),
  CONSTRAINT `fmd_asset_fk`
    FOREIGN KEY (`assetId`)
    REFERENCES `3rdi`.`asset` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fmd_bcmLevel_fk`
    FOREIGN KEY (`bcmLevelId`)
    REFERENCES `3rdi`.`bcm_level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fmd_fmId_fk`
    FOREIGN KEY (`functionalMapId`)
    REFERENCES `3rdi`.`functional_map` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE TABLE `3rdi`.`export_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `hash` VARCHAR(45) NOT NULL,
  `workspaceId` INT(11) NOT NULL,
  `userId` INT(11) NOT NULL,
  `purpose` VARCHAR(45) NOT NULL,
  `refId` INT(11) NOT NULL,
  `timeDown` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUp` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `export_userId_fk_idx` (`userId` ASC),
  INDEX `export_workspaceId_fk_idx` (`workspaceId` ASC),
  CONSTRAINT `export_userId_fk`
    FOREIGN KEY (`userId`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `export_workspaceId_fk`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `3rdi`.`workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

ALTER TABLE `3rdi`.`functional_map_data` 
CHANGE COLUMN `value` `data` TEXT NULL DEFAULT NULL ,
ADD COLUMN `updatedBy` INT(11) NOT NULL AFTER `data`,
ADD COLUMN `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `updatedBy`,
ADD INDEX `fmd_userId_fk_idx` (`updatedBy` ASC);
ALTER TABLE `3rdi`.`functional_map_data` 
ADD CONSTRAINT `fmd_userId_fk`
  FOREIGN KEY (`updatedBy`)
  REFERENCES `3rdi`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
