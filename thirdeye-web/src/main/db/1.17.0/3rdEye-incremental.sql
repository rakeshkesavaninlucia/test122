-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


ALTER TABLE `questionnaire` 
ADD COLUMN `year` VARCHAR(4) NULL DEFAULT NULL AFTER `updatedDate`;

ALTER TABLE `questionnaire` 
ADD UNIQUE INDEX `questionnaire_assetTypeId_year` (`assetTypeId` ASC, `year` ASC);

ALTER TABLE `asset_type` 
ADD COLUMN `imagePath` VARCHAR(45) NULL AFTER `updatedDate`;

UPDATE `asset_type` SET `imagePath`='static/img/app.svg' WHERE `id`='1';
UPDATE `asset_type` SET `imagePath`='static/img/server.svg' WHERE `id`='2';
UPDATE `asset_type` SET `imagePath`='static/img/database.svg' WHERE `id`='3';
UPDATE `asset_type` SET `imagePath`='static/img/person.svg' WHERE `id`='4';
UPDATE `asset_type` SET `imagePath`='static/img/businesServices.svg' WHERE `id`='5';
UPDATE `asset_type` SET `imagePath`='static/img/ITservices.svg' WHERE `id`='6';



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;