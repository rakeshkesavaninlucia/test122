-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `questionnaire_parameter_asset` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` INT(11) NOT NULL,
  `parameterId` INT(11) NOT NULL,
  `assetId` INT(11) NOT NULL,
  `boostPercentage` DECIMAL(5,2) NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Qe_P_A` (`questionnaireId` ASC, `parameterId` ASC, `assetId` ASC),
  INDEX `FK_ParameterId_idx` (`parameterId` ASC),
  INDEX `FK_AssetId_idx` (`assetId` ASC),
  INDEX `FK_CreatedBy_idx` (`createdBy` ASC),
  INDEX `FK_UpdatedBy_idx` (`updatedBy` ASC),
  CONSTRAINT `FK_QuestionnaireId`
    FOREIGN KEY (`questionnaireId`)
    REFERENCES `questionnaire` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_ParameterId`
    FOREIGN KEY (`parameterId`)
    REFERENCES `parameter` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_AssetId`
    FOREIGN KEY (`assetId`)
    REFERENCES `asset` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_CreatedBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_UpdatedBy`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


ALTER TABLE `questionnaire_question` DROP COLUMN `boostValue`;

ALTER TABLE `quality_gate` ADD COLUMN `colorDescription` TEXT NULL AFTER `workspaceId`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;