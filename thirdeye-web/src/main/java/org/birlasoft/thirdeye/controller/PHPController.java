package org.birlasoft.thirdeye.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.php.PHPWrapper;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.PHPService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * Controller to implement viewPHP, plotHeatMap functionality. 
 *
 */
@Controller
@RequestMapping(value="/portfoliohealth")
public class PHPController {
	
	private ParameterService parameterService;
	private QuestionnaireService questionnaireService;	
	private CustomUserDetailsService customUserDetailsService;
	private WorkspaceSecurityService workspaceSecurityService;
	private PHPService phpService;
	
	/**
	 * Constructor to initialize services
	 * @param customUserDetailsService
	 * @param workspaceSecurityService
	 * @param phpService
	 * @param parameterService
	 * @param questionnaireService
	 */
	@Autowired
	public PHPController(CustomUserDetailsService customUserDetailsService,
			WorkspaceSecurityService workspaceSecurityService,
			PHPService phpService,
			ParameterService parameterService,
			QuestionnaireService questionnaireService) {
		this.customUserDetailsService = customUserDetailsService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.phpService = phpService;
		this.parameterService = parameterService;
		this.questionnaireService = questionnaireService;
	}

	/**
	 * show portfolio health
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/view")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_PHP'})")
	public String viewPHP(Model model){
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();

		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		List<Questionnaire> questionnairesInWorkspaces = questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString());
		
		model.addAttribute("listOfQuestionnaire", questionnairesInWorkspaces);
		return "php/viewPHP";
	}
	
	/**
	 * Get {@code list} of root {@link parameter} for Questionnaire .
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value="/fetchRootParameters/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_PHP'})")
	public String getRootParameter(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(questionnaire);		
		// You should validate that we only show a list of quantifiable parameters		
		// Put the parameters into the modal
		model.addAttribute("rootParamList", listOfRootParameters);
		
		return "php/viewPHP :: optionListOfParameters(listOfParameters=${rootParamList})";
	}
	
	/**
	 * Get {@code set} of {@link AssetTemplate} by idOfQuestionnaire
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */

	@RequestMapping(value="/fetchAssetTemplates/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_PHP'})")
	public String getAssetTemplates(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		Set<AssetTemplate> assetTemplates = phpService.getAssetTemplatesFromQuestionnaire(questionnaire);	
		
		model.addAttribute("assetTemplates", assetTemplates);
		
		return "php/viewPHP :: optionListOfAssetTemplates(listOfAssetTemplates=${assetTemplates})";
	}
	
	/**
	 * Plot Heat map
	 * @param idsOfParameters
	 * @param idsOfAssetTemplate
	 * @param idOfQuestionnaire
	 * @return {@code Set<PHPWrapper>}
	 */
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_PHP'})")
	@ResponseBody
    public Set<PHPWrapper> plotHeatMap(@RequestParam(value = "parameterIds") Set<Integer> idsOfParameters, 
                  @RequestParam(value = "assetTemplateIds") Set<Integer> idsOfAssetTemplate, @RequestParam(value = "questionnaireId") Integer idOfQuestionnaire, HttpServletRequest request){
        
		Map<String, List<String>> filterMap = getFilterMap(request);
		return phpService.getPHPWrapperToPlot(idsOfParameters, idsOfAssetTemplate, idOfQuestionnaire, filterMap);
      }
	
	@RequestMapping(value="/downloadExcel", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_PHP'})")
	@ResponseBody
	public void downloadPHPExcel(@RequestParam(value = "parameterIds") Set<Integer> idsOfParameters, 
            @RequestParam(value = "assetTemplateIds") Set<Integer> idsOfAssetTemplate, @RequestParam(value = "questionnaireId") Integer idOfQuestionnaire, HttpServletRequest request,HttpServletResponse response){
		// First find the questionnaire
		Map<String, List<String>> filterMap = getFilterMap(request);
		Set<PHPWrapper> heatMapData =  phpService.getPHPWrapperToPlot(idsOfParameters, idsOfAssetTemplate, idOfQuestionnaire, filterMap);
		phpService.downloadPHPExcel(heatMapData,idOfQuestionnaire,response);
	}
	
	private Map<String, List<String>> getFilterMap(HttpServletRequest request) {
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			if (!"parameterIds".equals(oneEntry.getKey()) && !"parameterIds[]".equals(oneEntry.getKey()) && !"questionnaireId".equals(oneEntry.getKey())  && !"assetTemplateIds".equals(oneEntry.getKey())  && !"assetTemplateIds[]".equals(oneEntry.getKey()) && !"_csrf".equals(oneEntry.getKey()))
				filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		return filterMap;
	}
	
}
