package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterSelectorBean;
import org.birlasoft.thirdeye.constant.ParameterSelectorType;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.ParameterSelectorService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for parameter selector
 */
@Controller
@RequestMapping(value="/parameterSelector")
public class ParameterSelectorController {


	private QuestionnaireService questionnaireService;
	private WorkspaceSecurityService workspaceSecurityService;
	private ParameterSelectorService parameterSelectorService ;
	
	/**
	 * Constructor for initializing services
	 * @param questionnaireService
	 * @param workspaceSecurityService
	 * @param parameterSelectorService
	 */
	@Autowired
	public ParameterSelectorController(
			QuestionnaireService questionnaireService,
			WorkspaceSecurityService workspaceSecurityService,
			ParameterSelectorService parameterSelectorService) {
	
		this.questionnaireService = questionnaireService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.parameterSelectorService = parameterSelectorService;
	}

	/**
	 * method to get parameters 
	 * @param parameterSelectorBean
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/fetchModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String getModalContentForAddingParameters(@ModelAttribute("parameterSelectorBean") ParameterSelectorBean parameterSelectorBean,Model model){

		Questionnaire qe = questionnaireService.findOne(parameterSelectorBean.getQuestionnaireId());		
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		List<Parameter> listOfParameter = new ArrayList<>();
		if(parameterSelectorBean.getMode().equalsIgnoreCase(ParameterSelectorType.QUESTIONNAIRE_PARAMETER_MODE.getValue())){
			parameterSelectorBean.setQuestionnaireType(qe.getQuestionnaireType());
			listOfParameter = parameterSelectorService.getModalParameterForQuestionnaire(parameterSelectorBean);
		}
	    
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES,qe.getQuestionnaireType());
		model.addAttribute("listOfParameter", listOfParameter);
	    return "param/parameterSelector :: parameterSelector";
	}


	
	
}
