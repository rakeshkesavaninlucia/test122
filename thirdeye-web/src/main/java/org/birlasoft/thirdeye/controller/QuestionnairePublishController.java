package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.birlasoft.thirdeye.beans.QuestionnaireQuestionWrapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.service.IncrementIndexService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Controller for {@code pre}{@code publish} {@code questionnaire} and {@code publish} {@code questionnaire}.
 */
@Controller
@RequestMapping("/questionnaire")
public class QuestionnairePublishController {

	private QuestionnaireService questionnaireService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private ResponseService responseService;
	@Autowired
	private TcoResponseService tcoResponseService;
	private ParameterFunctionService parameterFunctionService;
	
	@Autowired
	private IncrementIndexService incrementIndexService ;
	
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	
	/**
	 * Constructor to initialize services 
	 * @param questionnaireService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 * @param parameterFunctionService
	 */
	@Autowired
	public QuestionnairePublishController(
			QuestionnaireService questionnaireService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService,
			ParameterFunctionService parameterFunctionService) {
		this.questionnaireService = questionnaireService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.parameterFunctionService = parameterFunctionService;
	}

	/**
	 * Create a {@code pre publish} {@code questionnaire}
	 * @param questionnaireId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/prepublish", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String prePublishQuestionnaire(@PathVariable(value = "id") Integer questionnaireId, Model model) {

		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		// Is there already any response for this qe. Currently only one response per qe.
				// we can add the ability to add more later.
				List<Response> listOfResponseAvailable = responseService.findByQuestionnaire(qe,false);
				Response newResponse = new Response();
				if (listOfResponseAvailable == null || listOfResponseAvailable.isEmpty()){
					newResponse = null;
				} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() == 1){
					newResponse = listOfResponseAvailable.get(0);
				} else if (!listOfResponseAvailable.isEmpty() && listOfResponseAvailable.size() > 1 ){
					// this is an error scenario in the given application
					return null;
				}
		
		
		List<QuestionnaireQuestion> questionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(qe, true);
		//get sorted list of asset name
		List<String> sortedList= tcoResponseService.getAssetName(questionnaireQuestions);
		QuestionnaireQuestionWrapper questionnaireQuestionWrapper = questionnaireQuestionService.getQuestionnaireData(qe,newResponse);
		
		model.addAttribute("questionnaireId", questionnaireId);
		model.addAttribute("questionnaire", qe);
		model.addAttribute("AssetName", sortedList);
		model.addAttribute("status","false");
		model.addAttribute("questionnaireQuestionWrapper", questionnaireQuestionWrapper);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.DEF.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Publish Questionnaire");
		
		
		return "questionnaire/questionnairePrePublish";
	}

	/**
	 * Create a {@code publish} {@code questionnaire}
	 * @param questionnaireId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/publish", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String publishQuestionnaire(@PathVariable(value = "id") Integer questionnaireId) {
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		qe.setStatus(QuestionnaireStatusType.PUBLISHED.name());
		
		questionnaireService.save(qe);
		
		//logic to index questionnaire after publishing..
		
				Iterator<QuestionnaireAsset> itrquassets = qe.getQuestionnaireAssets().iterator() ;
				List<Integer> assetIdLst = new ArrayList<Integer>() ;
				while(itrquassets.hasNext() ){
					assetIdLst.add(itrquassets.next().getAsset().getId()) ;
				}
				String tenantId = currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
				incrementIndexService.updateAssetQuestionnaire(assetIdLst, tenantId);
				
				
		//index logic ends.
		
		return "redirect:/questionnaire/list";
	}
}
