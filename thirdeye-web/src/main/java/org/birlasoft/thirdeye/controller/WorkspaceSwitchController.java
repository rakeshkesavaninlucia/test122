package org.birlasoft.thirdeye.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.filters.DefaultWorkspaceSessionInterceptor;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller to switch the active workspace of the user
 * 
 * @author tej.sarup
 *
 */
@Controller
@RequestMapping(value="/workspace")
public class WorkspaceSwitchController {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	/**
	 *  Save {@code graph}.
	 * @param request
	 * @param workspaceId
	 * @param httpServletResponse
	 */
	@RequestMapping(value = "/switchactive", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'APP_LOGIN'})")
	public void switchActiveWorkspace(HttpServletRequest request, @RequestParam("workspaceId") Integer workspaceId, HttpServletResponse httpServletResponse) {
		
		// Check if the user has access to the target workspace
		User currentUser = customUserDetailsService.getCurrentUser();
		if (currentUser.getUserWorkspaces() != null ) {
			for (UserWorkspace oneUW : currentUser.getUserWorkspaces()){
				// Set the new workspace
				if (oneUW.getWorkspace().getId() == workspaceId){
					request.getSession().setAttribute(DefaultWorkspaceSessionInterceptor.USER_ACTIVE_WORKSPACE, oneUW.getWorkspace());
					Cookie cookie = new Cookie(DefaultWorkspaceSessionInterceptor.COOKIE_WS, String.valueOf(oneUW.getWorkspace().getId()));
					cookie.setMaxAge(60*60*24*365);
					cookie.setPath("/");
//					cookie.setSecure(true);
					cookie.setHttpOnly(true);
					httpServletResponse.addCookie(cookie);
					break;
				}
			}
			
		}
	}

}
