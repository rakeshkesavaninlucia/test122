package org.birlasoft.thirdeye.controller.admin;

import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for  , view {@code graph} ,
 * list of all {@code graph} , view {@code asset} on {@code graph} .
 */
@Controller
@RequestMapping(value="/adminadd")
public class AdminADDViewController {
	
	

	/**
	 * View {@code graph} by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String viewGraphByID(Model model, @PathVariable(value = "id") Integer idOfGraph) {
		model.addAttribute("graphId", idOfGraph);
		return "add/viewGraph";
	}

}
