package org.birlasoft.thirdeye.controller.admin;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for to view list of Aid, showADashBoard,
 */
@Controller
@RequestMapping(value = "/adminaid")
public class AdminAIDViewController {

	private static final String AID_REPORT_ID = "_aidReportId";

	@Autowired
	private AIDService aidService;

	
	
	/**
	 * method to get list of AID
	 * @param model
	 * @param aidReportId
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'AID_VIEW'})")
	public String getListOfAID(Model model, @PathVariable(value = "id") Integer aidReportId, HttpSession session,
			HttpServletRequest request) {

		model.addAttribute("aid", aidReportId);
		return "admin/adminApplicationInterfaceDiagram";
	}
	
	/**
	 * method to get list of AID
	 * @param model
	 * @param aidReportId
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/viewAID", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'AID_VIEW'})")
	public String viewAID(Model model, @PathVariable(value = "id") Integer aidReportId, HttpSession session,
			HttpServletRequest request) {

		Map<String, List<String>> filterMap = extractFilterMapFromRequestParam(request);

		Set<Integer> aidAssetIds = aidService.getAssetIdsFromElasticSearch(aidReportId, filterMap);

		model.addAttribute("aidAssetSet", aidAssetIds);
		model.addAttribute("aid", aidReportId);
		session.setAttribute(AID_REPORT_ID, aidReportId);

		return "admin/adminAidFragments :: applInterface";
	}

	
	/**
	 * method to show DashBoard
	 * @param model
	 * @param assetId
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_VIEW'})")
	public String showADashBoard (Model model,  @PathVariable(value = "id") Integer assetId, HttpSession session) {
			
		model.addAttribute("oneAid", aidService.getAssetAidFromElasticsearch(assetId, (Integer)session.getAttribute(AID_REPORT_ID)));
		return "admin/adminAidFragments :: aidContentWrapper";
	}
	

	private Map<String, List<String>> extractFilterMapFromRequestParam(HttpServletRequest request) {
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		return filterMap;
	}

}
