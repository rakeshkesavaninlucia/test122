package org.birlasoft.thirdeye.controller.bcm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.validators.BcmValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for createABCMTemplate {@code String} , saveANewBCMTemplate {@code} , to show listBCMTemplate {@code String} and
 * updateBCMTemplate {@code String}
 *
 */
@Controller
@RequestMapping(value="/bcm")
public class BCMTemplateController {
	
	private CustomUserDetailsService customUserDetailsService;
	private BCMService bcmService;
	private BcmValidator bcmValidator;

	/**
	 * Constructor to initialize services. 
	 * @param customUserDetailsService
	 * @param bcmService
	 * @param bcmValidator
	 */
	@Autowired
	public BCMTemplateController(CustomUserDetailsService customUserDetailsService,
			BCMService bcmService,
			BcmValidator bcmValidator) {
		this.customUserDetailsService = customUserDetailsService;
		this.bcmService = bcmService;
		this.bcmValidator = bcmValidator;
	}

	/**
	 * Create a new BCM Template. This returns the modal for the form.
	 * @param model
	 * @param idOfBCM
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public String createABCMTemplate (Model model, @RequestParam(value="bcmId",required=false) Integer idOfBCM) {
		if(idOfBCM != null){
			Bcm bcm = bcmService.findOne(idOfBCM);
			model.addAttribute("bcmTemplateForm", bcm);
		}else{
			model.addAttribute("bcmTemplateForm", new Bcm());
		}
		
		return "bcm/bcmFragments :: createNewBCMTemplate";
	}
	
	/**
	 * method to save a new BCM template.
	 * @param incomingBcm
	 * @param result
	 * @param model
	 * @param response
	 * @return {@code Object}
	 */
	@RequestMapping(value = "/create/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	public Object saveANewBCMTemplate (@ModelAttribute("bcmTemplateForm") Bcm incomingBcm, BindingResult result, HttpServletResponse response) {
		
		bcmValidator.validate(incomingBcm, result);
		if(result.hasErrors()){
			return new ModelAndView("bcm/bcmFragments :: createNewBCMTemplate");
		}
		// Ask the service to go and update or save as required
		Bcm toSaveOrUpdate ;
		if(incomingBcm.getId() == null){
			toSaveOrUpdate = createNewBcmObject(incomingBcm);
		}else{
			toSaveOrUpdate = bcmService.findOne(incomingBcm.getId());
			toSaveOrUpdate.setBcmName(incomingBcm.getBcmName());
		}
		
		bcmService.saveOrUpdate(toSaveOrUpdate);
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "bcm/list");
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Show the list of BCM template. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_VIEW'})")
	public String listBCMTemplate (Model model) {			
		model.addAttribute("listOfBCMTemplate", bcmService.findByWorkspaceIsNullAndBcmIsNull());
		return "bcm/listBCMTemplate";
	}
	
	/**
	 * Create a new BCM object.
	 * @param incomingBcm
	 * @return {@code BCM}
	 */
	private Bcm createNewBcmObject(Bcm incomingBcm) {
		Bcm toSaveOrUpdate = new Bcm();
		toSaveOrUpdate.setBcmName(incomingBcm.getBcmName());
		toSaveOrUpdate.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		toSaveOrUpdate.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		toSaveOrUpdate.setCreatedDate(new Date());
		toSaveOrUpdate.setUpdatedDate(new Date());
		return toSaveOrUpdate;
	}
	
	/**
	 * method to update BCM template.
	 * @param bcmId
	 * @param bcmName
	 */
	@RequestMapping(value = "/{id}/update", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BCM_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateBCMTemplate (@PathVariable(value = "id") Integer bcmId, @RequestParam(value="bcmName") String bcmName) {
		
		if(bcmName != null && bcmName.length() > 0){
			Bcm bcmFromDB = bcmService.findOne(bcmId); 
			bcmFromDB.setBcmName(bcmName);
			bcmFromDB.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
			bcmFromDB.setUpdatedDate(new Date());
			bcmService.saveOrUpdate(bcmFromDB);
		}
	}
}
