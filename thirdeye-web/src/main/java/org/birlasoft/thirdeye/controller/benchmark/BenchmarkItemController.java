package org.birlasoft.thirdeye.controller.benchmark;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.service.BenchmarkItemService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.BenchmarkItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sunil1.gupta 
 */
@Controller
@RequestMapping(value="/benchmarkItem")
public class BenchmarkItemController {
	
	
	private final WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private BenchmarkItemService benchmarkItemService;
	@Autowired
	private BenchmarkItemValidator benchmarkItemValidator;

		
	/**
	 * Constructor to Initialize services
	 * @param workspaceSecurityService
	 */
	@Autowired
	public BenchmarkItemController(	WorkspaceSecurityService workspaceSecurityService) {		
		this.workspaceSecurityService = workspaceSecurityService;
	}

	
	/**
	 * Create new benchmark item column. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/newRow/{id}", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String createBenchmarkItemRow(Model model, @PathVariable(value = "id") Integer idOfBenchmark) {
		BenchmarkItem benchmarkItem = new BenchmarkItem();
		Benchmark benchmark = new Benchmark();
		benchmark.setId(idOfBenchmark);
		benchmarkItem.setBenchmark(benchmark);
		model.addAttribute("benchmarkItemBean", benchmarkItem);	
		return "benchmark/benchmarkFragment :: createRow";
	}

	/**
	 * Edit benchmark item column.
	 * @param model
	 * @param idOfBenchmarkItem	
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/editRow/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String editBenchmarkItem(Model model, @PathVariable(value = "id") Integer idOfBenchmarkItem) {
		
		
		BenchmarkItem benchmarkItem = benchmarkItemService.findById(idOfBenchmarkItem);		
		model.addAttribute("benchmarkItemBean", new BenchmarkItem());
		model.addAttribute("oneRow", benchmarkItem);
		return "benchmark/benchmarkFragment :: editBenchmarkItem";
	}

	/**
	 * Method to save roe of benchmark item.
	 * @param benchmarkItemBean
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/columns/saveRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String saveBenchmarkItemRow(@ModelAttribute("benchmarkItemBean") BenchmarkItem benchmarkItemBean, BindingResult result, Model model) {
		benchmarkItemValidator.validate(benchmarkItemBean, result);
		if(result.hasErrors()){
			if(benchmarkItemBean.getId() == null){
			   return "benchmark/benchmarkFragment :: createRow";
			}else{
			   model.addAttribute("oneRow", benchmarkItemBean);
			   return "benchmark/benchmarkFragment :: editBenchmarkItem";
			}
		}
		
		BenchmarkItem benchmarkItem = benchmarkItemService.saveOrUpdate(benchmarkItemBean);
		model.addAttribute("colid", benchmarkItem.getId());
		model.addAttribute("baseName", benchmarkItem.getBaseName());
		model.addAttribute("version", benchmarkItem.getVersion());
		model.addAttribute("displayName", benchmarkItem.getDisplayName());
		
		return "benchmark/benchmarkFragment :: viewBenchmarkItem";
	}
	
	/**
	 * Delete benchmark Item
	 * @param idOfBenchmarkItem
	 */
	@RequestMapping(value = "/columns/deleteRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteBenchmarkItem(@RequestParam(value="id") Integer idOfBenchmarkItem) {
		    benchmarkItemService.deleteBenchmarkItem(idOfBenchmarkItem);
	}
	
}
