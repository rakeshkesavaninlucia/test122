package org.birlasoft.thirdeye.controller.parameter.ap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/parameter/ap")
public class AggregateParameterController {

	private CustomUserDetailsService customUserDetailsService;
	private ParameterService parameterService;

	@Autowired
	public AggregateParameterController(CustomUserDetailsService customUserDetailsService,
			ParameterService parameterService) {
		this.customUserDetailsService = customUserDetailsService;
		this.parameterService = parameterService;
	}

	@RequestMapping(value = "/showButton", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'PARAMETER_MODIFY'})")
	public String showAddQuestionButton(Model model) {
		return "param/paramFragment :: addParameterButton";
	}

	@RequestMapping(value = "/showModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'PARAMETER_MODIFY'})")
	public String getModalContentForAddingParameters(Model model, HttpSession session,
			@RequestParam(value = "workspaceId", required = false) Integer workspaceId) {
		List<Parameter> listOfParameter = new ArrayList<Parameter>();
		if (String.valueOf(workspaceId).equals("null") || workspaceId == -1) {
			listOfParameter = parameterService.findByWorkspaceIsNull();
		} else if (workspaceId != null && workspaceId > 0) {
			listOfParameter = parameterService
					.findByWorkspaceIsNullOrWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		}

		List<Parameter> listOfNewParam = new ArrayList<Parameter>();
		for (Parameter p : listOfParameter) {
			if (p.getType().equalsIgnoreCase(ParameterType.TCOA.name()) || p.getType().equalsIgnoreCase(ParameterType.TCOL.name())) {
				listOfNewParam.add(p);
			}
		}
		listOfParameter.removeAll(listOfNewParam);
		List<Parameter> parameters = new CopyOnWriteArrayList<Parameter>(listOfParameter);

		Set<Parameter> listOfSelectedParameters = (HashSet<Parameter>) session.getAttribute("_selectedParameters");
		if (listOfSelectedParameters != null && listOfSelectedParameters.size() > 0) {
			for (Parameter p : parameters) {
				for (Parameter selectedParameter : listOfSelectedParameters) {
					if (selectedParameter.getId().equals(p.getId())) {
						parameters.remove(p);
						break;
					}
				}
			}
		}

		model.addAttribute("listOfParameter", parameters);
		return "param/paramFragment :: parametersModal";
	}

	@RequestMapping(value = "/saveTemporary", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'PARAMETER_MODIFY'})")
	public String saveSelectedParameters(Model model, @RequestParam(value = "parameter[]") Set<Integer> setOfParameters,
			HttpSession session) {
		Set<Parameter> listOfParametersToUpdate = (HashSet<Parameter>) session.getAttribute("_selectedParameters");
		listOfParametersToUpdate.addAll(parameterService.findByIdIn(setOfParameters));
		session.setAttribute("_selectedParameters", listOfParametersToUpdate);
		model.addAttribute("mapOfWeight", new HashMap<Integer, Double>());
		model.addAttribute("selectedParameters", session.getAttribute("_selectedParameters"));
		return "param/paramFragment :: selectedParameter";
	}
}
