package org.birlasoft.thirdeye.controller.parameter.fc;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.constant.FunctionalMapType;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/parameter/fc")
public class FunctionalCoverageParameterController {
	
	private FunctionalMapService functionalMapService;
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	public FunctionalCoverageParameterController(FunctionalMapService functionalMapService,
			CustomUserDetailsService customUserDetailsService) {
		this.functionalMapService = functionalMapService;
		this.customUserDetailsService = customUserDetailsService;
	}

	@RequestMapping(value="/showButton", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String showAddFunctionalMapButton(Model model){
		return "param/paramFragment :: addFunctionalMapButton";
	}
	
	@RequestMapping(value="/showModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String showFunctionalMapModal(Model model){
		model.addAttribute("fcParamConfigBean", new FunctionalCoverageParameterConfigBean());
		Set<String> levelTypes = new HashSet<String>();
		levelTypes.add(FunctionalMapType.L1.getValue());
//		model.addAttribute("listOfFunctionalMap", functionalMapService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser(), true));
		model.addAttribute("listOfFunctionalMap", functionalMapService.findByWorkspaceAndTypeNotIn(customUserDetailsService.getActiveWorkSpaceForUser(), levelTypes, true));
		return "param/paramFragment :: functionalMapModal";
	}
	
	@RequestMapping(value="/saveTemporary", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String saveFunctionalMapTemporary(@RequestParam("functionalMapId[]") Integer idOfFunctionalMapId, Model model, HttpSession session){
		FunctionalMap functionalMap = functionalMapService.findOne(idOfFunctionalMapId, false);
		Map<Integer, String> mapOfFunctionalMapType = functionalMapService.getMapOfLevelTypes(functionalMap);
		model.addAttribute("mapOfFunctionalMapType", mapOfFunctionalMapType);
		FunctionalCoverageParameterConfigBean fcConfigBean = new FunctionalCoverageParameterConfigBean(functionalMap);
		updateSessionCache(session, fcConfigBean);
		model.addAttribute("selectedFunctionalMap", fcConfigBean);
		return "param/paramFragment :: selectedFunctionalMap";
	}

	/**
	 * Update cache/session.
	 * @param session
	 * @param cachedFunctionalCoverageParameterConfigBean
	 */
	private void updateSessionCache(HttpSession session, FunctionalCoverageParameterConfigBean cachedFunctionalCoverageParameterConfigBean) {
		session.setAttribute("_fcParamConfigBean", cachedFunctionalCoverageParameterConfigBean);
	}
}
