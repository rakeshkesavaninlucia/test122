package org.birlasoft.thirdeye.controller.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.DiscreteBar;
import org.birlasoft.thirdeye.beans.DiscreteBarWrapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.widgets.BarChartConfig;
import org.birlasoft.thirdeye.beans.widgets.FacetWidgetJSONConfig;
import org.birlasoft.thirdeye.beans.widgets.WidgetHelper;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.GraphFacetsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

@Controller
@RequestMapping(value="/widgets/facets")
public class FacetWidgetController {
	
	private QuestionnaireService questionnaireService;
	private CustomUserDetailsService customUserDetailsService; 
	private QuestionnaireQuestionService questionnaireQuestionService;
	private QuestionService questionService;
	private GraphFacetsService graphFacetsService;
	private final WorkspaceSecurityService workspaceSecurityService;
	
	@Autowired
	private ParameterService parameterService;
	
	@Autowired
	private DashboardService dashboardService;
	
	
	@Autowired
	public FacetWidgetController(QuestionnaireService questionnaireService,
			CustomUserDetailsService customUserDetailsService,
			WorkspaceService workspaceService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService,
			QuestionService questionService,
			GraphFacetsService graphFacetsService) {
		this.questionnaireService = questionnaireService;
		this.customUserDetailsService = customUserDetailsService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.graphFacetsService = graphFacetsService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.questionService = questionService;
	}
	
	@RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	public String showOneWidget (Model model,  @PathVariable(value = "id") Integer idOfWidget) {

		// Validate access to the widget
		// Check if the user can access the data which the widget is asking for.
		Widget w = dashboardService.findOneWidget(idOfWidget, true);
		
		if (!w.getDashboard().getWorkspace().getId().equals(customUserDetailsService.getActiveWorkSpaceForUser().getId())){
			throw new AccessDeniedException("You can not access this widget");
		}

		WidgetHelper wh = new WidgetHelper();
		
		FacetWidgetJSONConfig basicWidget =  wh.getWidgetBeanFromWidget(w);
		model.addAttribute("oneWidget", basicWidget);
		
		if (basicWidget != null && basicWidget.getQuestionnaireId() > 0) { 
			// At least preload the questions in this questionnaire
			Questionnaire q = questionnaireService.findOne(basicWidget.getQuestionnaireId());
			// Check if the user can access this questionnaire.
			workspaceSecurityService.authorizeQuestionnaireAccess(q);
			model.addAttribute("listOfQuestions", questionnaireQuestionService.findQuestionsOfMCQorRadioType(q));
		}
		
		if (basicWidget != null && basicWidget.getQuestionnaireId() > 0 && basicWidget.getParamId() > 0){
			Questionnaire q = questionnaireService.findOne(basicWidget.getQuestionnaireId());
			insertParametersInModel(model, q);
		}
		
		// Put the other drop downs etc on the graph
		// 
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();

		Set<Workspace> activeWorkspace = new HashSet<Workspace>(Arrays.asList( activeWS ));
		model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString()));

		return "dashboards/facetWidget :: widgetContentWrapper";
	}
	
	/**
	 * Fetch Questions by QE id.
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value = "/fetchQuestions/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	public String fetchQuestions(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		
		Questionnaire q = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(q);
		
		model.addAttribute("listOfQuestions", questionnaireQuestionService.findQuestionsOfMCQorRadioType(q));
		model.addAttribute("ajaxRequest", true);
		model.addAttribute("oneWidget", new FacetWidgetJSONConfig());
		return "dashboards/facetWidget :: questionList(listOfQuestions=${listOfQuestions})";
	}
	
	/**
	 * Fetch Questions by QE id.
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value = "/fetchParameters/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	public String fetchParams (Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		
		Questionnaire q = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(q);
		
		insertParametersInModel(model, q);
		model.addAttribute("ajaxRequest", true);
		model.addAttribute("oneWidget", new FacetWidgetJSONConfig());
		
		return "dashboards/facetWidget :: paramList(listOfParams=${listOfParams})";
	}

	private void insertParametersInModel(Model model, Questionnaire q) {
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(q);
		
		model.addAttribute("listOfParams", listOfRootParameters);
	}


	/**
	 * Plot pie or Bar chart based on JSON data in map.
	 * @param model
	 * @param idOfQuestionnaireQuestionId
	 * @return {@code Map<String, Integer>}
	 */
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	@ResponseBody
	public List<DiscreteBarWrapper> plotChart(Model model, @ModelAttribute FacetWidgetJSONConfig faceJSONConfig,
													@RequestParam(value = "saveWidget", required=false) String saveWidget,HttpServletRequest request){

		// TODO - check access to widget
		List<DiscreteBarWrapper> listForReturn = new ArrayList<DiscreteBarWrapper>();
		

		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			if (!"questionId".equals(oneEntry.getKey()) && !"graphType".equals(oneEntry.getKey()) && !"saveWidget".equals(oneEntry.getKey()) && !"questionnaireId".equals(oneEntry.getKey()) && !"_csrf".equals(oneEntry.getKey()) && !"id".equals(oneEntry.getKey()) && !"param".equals(oneEntry.getKey()) && !"paramId".equals(oneEntry.getKey()))
				filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		
		Questionnaire qe = questionnaireService.findOne(faceJSONConfig.getQuestionnaireId());
		// Check if the user can access this questionnaire.
		if (qe != null){
			workspaceSecurityService.authorizeQuestionnaireAccess(qe);
			
			Question q = questionService.findOne(faceJSONConfig.getQuestionId());
			Optional<BarChartConfig> barConfig = Optional.ofNullable(null);
			if (faceJSONConfig.getParamId() > 0){
				BarChartConfig newConfig = new BarChartConfig();
				newConfig.setParameterId(faceJSONConfig.getParamId());
				barConfig = Optional.of(newConfig);
			}
			
			Map<String, Integer> mapJSONDataForPlotChart = graphFacetsService.getMapForPlotPieOrBarChart(qe, q, barConfig,filterMap);
			
			// Convert to the data wrapper
			DiscreteBarWrapper wrapper = new DiscreteBarWrapper();
			if (q != null){
				wrapper.setKey(q.getTitle());
			}
			
			for(String oneKey : mapJSONDataForPlotChart.keySet()){
				DiscreteBar db = new DiscreteBar(oneKey, new Double(mapJSONDataForPlotChart.get(oneKey)));
				wrapper.addValue(db);
			}
			
			listForReturn.add(wrapper);
		}
		
		
		
		
		// Save the widget configuration if required
		if (StringUtils.equalsIgnoreCase(saveWidget, "true")){
			Integer widgetId = faceJSONConfig.getId();
			if (widgetId != null && widgetId > 0){
				Widget w = dashboardService.findOneWidget(widgetId, false);
				WidgetHelper wh = new WidgetHelper();
				FacetWidgetJSONConfig currentConfig = wh.getWidgetBeanFromWidget(w);
				currentConfig.setQuestionId(faceJSONConfig.getQuestionId());
				currentConfig.setQuestionnaireId(faceJSONConfig.getQuestionnaireId());
				currentConfig.setGraphType(faceJSONConfig.getGraphType());
				if (faceJSONConfig.getParamId() > 0){
					currentConfig.setParamId(faceJSONConfig.getParamId());
				}
				w.setWidgetConfig(Utility.convertObjectToJSONString(currentConfig));
				dashboardService.saveWidget(w);
			}
			
		}
		return listForReturn;
	}
}
