package org.birlasoft.thirdeye.controller.widget;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.widgets.GroupedDiscreteBarWrapper;
import org.birlasoft.thirdeye.beans.widgets.GroupedWidgetJSONConfig;
import org.birlasoft.thirdeye.beans.widgets.WidgetHelper;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.GroupedWidgetService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

@Controller
@RequestMapping(value="/widgets/grouped")
public class GroupedWidgetController {
	
	private static final String ONE_WIDGET = "oneWidget";
	private QuestionnaireService questionnaireService;
	private CustomUserDetailsService customUserDetailsService; 
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	
	@Autowired
	private ParameterService parameterService;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private GroupedWidgetService groupedWidgetService;
	
	@Autowired
	public GroupedWidgetController(QuestionnaireService questionnaireService,
			CustomUserDetailsService customUserDetailsService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionnaireService = questionnaireService;
		this.customUserDetailsService = customUserDetailsService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;
	}
	
	@RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	public String showOneWidget (Model model,  @PathVariable(value = "id") Integer idOfWidget) {

		// Validate access to the widget
		// Check if the user can access the data which the widget is asking for.
		Widget w = dashboardService.findOneWidget(idOfWidget, true);
		
		if (!w.getDashboard().getWorkspace().getId().equals(customUserDetailsService.getActiveWorkSpaceForUser().getId())){
			throw new AccessDeniedException("You can not access this widget");
		}

		WidgetHelper wh = new WidgetHelper();
		
		GroupedWidgetJSONConfig basicWidget =  wh.getWidgetBeanFromWidget(w);
		model.addAttribute(ONE_WIDGET, basicWidget);
		
		if(basicWidget != null) {			
			//set data for first dropdown
			addFieldDrodownDataInModel(model, basicWidget.getQuestionnaireId(), basicWidget.getFieldOne(), basicWidget.getFieldOneId(), "listOfFieldOneItems");
			
			//set data for second dropdown
			addFieldDrodownDataInModel(model, basicWidget.getQuestionnaireId(), basicWidget.getFieldTwo(), basicWidget.getFieldTwoId(), "listOfFieldTwoItems");
		}
		
		// Put the other drop downs etc on the graph
		Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();

		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString()));

		return "dashboards/groupedWidget :: widgetContentWrapper";
	}

	private void addFieldDrodownDataInModel(Model model, Integer questionnaireId, String field, Integer fieldId, String dropdownName) {
		if (questionnaireId > 0 && field != null) {
			// At least preload the questions in this questionnaire
			Questionnaire q = questionnaireService.findOne(questionnaireId);
			if(field.equals("question") && fieldId > 0) {				
				// Check if the user can access this questionnaire.
				workspaceSecurityService.authorizeQuestionnaireAccess(q);
				model.addAttribute(dropdownName, questionnaireQuestionService.findQuestionsOfMCQorRadioType(q));
			} else if(field.equals("parameter") && fieldId > 0) {
				List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(q);
				model.addAttribute(dropdownName, listOfRootParameters);
			}
		}
	}
	
	/**
	 * Plot grouped chart based on JSON data in map.
	 * @param model
	 * @param idOfQuestionnaireQuestionId
	 * @return {@code Map<String, Integer>}
	 */
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FACET_GRAPH'})")
	@ResponseBody
	public List<GroupedDiscreteBarWrapper> plotChart(Model model, @ModelAttribute(ONE_WIDGET) GroupedWidgetJSONConfig widgetJSONConfig,
													@RequestParam(value = "saveWidget", required=false) String saveWidget, HttpServletRequest request){
		List<GroupedDiscreteBarWrapper> listForReturn;
		
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			if (!"questionId".equals(oneEntry.getKey()) && !"graphType".equals(oneEntry.getKey()) && !"saveWidget".equals(oneEntry.getKey()) && !"questionnaireId".equals(oneEntry.getKey()) && !"_csrf".equals(oneEntry.getKey()) && !"id".equals(oneEntry.getKey()) && !"param".equals(oneEntry.getKey()) && !"paramId".equals(oneEntry.getKey())
					&& !"fieldOne".equals(oneEntry.getKey()) && !"fieldOneId".equals(oneEntry.getKey()) && !"fieldTwo".equals(oneEntry.getKey()) && !"fieldTwoId".equals(oneEntry.getKey()))
				filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		
		listForReturn = groupedWidgetService.getDataForGroupedGraph(widgetJSONConfig);
		
		// Save the widget configuration if required
		if (StringUtils.equalsIgnoreCase(saveWidget, "true")){
			groupedWidgetService.saveGroupWidget(widgetJSONConfig);
		}
		return listForReturn;
	}

}
