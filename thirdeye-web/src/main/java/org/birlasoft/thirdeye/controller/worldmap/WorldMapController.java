package org.birlasoft.thirdeye.controller.worldmap;

import java.util.List;

import org.birlasoft.thirdeye.beans.WorldMapWrapper;
import org.birlasoft.thirdeye.service.WorldMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/worldmap")
public class WorldMapController {

	private WorldMapService worldMapService;
	
	@Autowired
	public WorldMapController(WorldMapService worldMapService) {
		this.worldMapService = worldMapService;
	}

	@RequestMapping(value = "/view/worldmap", method = RequestMethod.GET)
	public String createUser() {
		return "questionnaire/worldMap" ;
	}

	@RequestMapping(value = "/getWorldMapWrapper", method = RequestMethod.GET)
	@ResponseBody
	public List<WorldMapWrapper> worldWrapper(Model model) {
		List<WorldMapWrapper> listOfWorldMapWrapper = worldMapService.getAllAssetsFromAssetTemplatesForActiveWorkSpace();
		return listOfWorldMapWrapper ;
	}

}
