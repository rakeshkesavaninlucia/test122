package org.birlasoft.thirdeye.filters;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.SecurityUser;
import org.birlasoft.thirdeye.controller.WorkspaceSwitchController;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This interceptor is designed to ensure that the logged in user works within the context
 * of a Workspace within a tenant. The workspace within which a user is working is defined at a session
 * level. 
 * 
 * This implementation checks if there is a default workspace set or not - if not picks up
 * the first one from the db.
 * 
 * @see WorkspaceSwitchController
 * @author shaishav.dixit
 *
 */
@Component
public class DefaultWorkspaceSessionInterceptor extends HandlerInterceptorAdapter {
	
	public static final String COOKIE_WS = "ws";

	public static final String USER_ACTIVE_WORKSPACE = "USER_ACTIVE_WORKSPACE";
	
	@Autowired
	CustomUserDetailsService userDetails;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		// This interceptor need only work if we are working with a logged in user.
		
		if (isInterceptRequired()){
			if (userDetails == null){
				return true;
			}
			
			if (userDetails.getCurrentUser() == null){
				return true;
			} else {
				setActiveWorkspaceInSession(request);
			}
		}
		return true;
	}

	/**
	 * Sets the active workspace for the user. First checks if there is any cookie
	 * else picks up the first one from the database.
	 * 
	 * @param request
	 */
	private void setActiveWorkspaceInSession(HttpServletRequest request) {
		SecurityUser u = (SecurityUser) userDetails.getCurrentUser();

		Workspace defaultWorkspace = null;
		boolean workspaceSet = false;
		if (u != null && request.getSession().getAttribute(USER_ACTIVE_WORKSPACE) == null){
			Integer wsId = extractWorkspaceFromCookie(request);
			if (u.getUserWorkspaces() != null){
				// Here we should ideally have some concept of default workspace
				// but since that is not present we will simply pick up the first one we get.
				for (UserWorkspace oneUW : u.getUserWorkspaces()){
					if (wsId > 0 && oneUW.getWorkspace().getId().equals(wsId)){
						request.getSession().setAttribute(USER_ACTIVE_WORKSPACE, oneUW.getWorkspace());
						workspaceSet = true;
					}

					if (defaultWorkspace == null){
						defaultWorkspace = oneUW.getWorkspace();
					}
				}

				if (!workspaceSet){
					request.getSession().setAttribute(USER_ACTIVE_WORKSPACE, defaultWorkspace);
				}
			}
		}
	}

	/**
	 * Returns whether or not this interceptor really needs to do anything or not.
	 * @return
	 */
	private boolean isInterceptRequired() {
		return SecurityContextHolder.getContext().getAuthentication() != null && (SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken);
	}

	
	/**
	 * Extract the workspace id from the cookie that is stored with the user.
	 * 
	 * @param request
	 * @return
	 */
	private Integer extractWorkspaceFromCookie(HttpServletRequest request) {
		Cookie [] cookies = request.getCookies();
		Integer wsId = 0;
		for (Cookie c : cookies){
			if (COOKIE_WS.equals(c.getName()) && !StringUtils.isEmpty(c.getValue())){
				try{
					wsId = Integer.parseInt(c.getValue());
				} catch (Exception e){
					
				}
			}
		}
		return wsId;
	}

}
