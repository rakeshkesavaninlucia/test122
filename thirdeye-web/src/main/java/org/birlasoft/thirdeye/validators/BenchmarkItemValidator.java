package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary} fields , logic of
 * {@code validation} is here.
 */
@Component
public class BenchmarkItemValidator implements Validator {
	private static final String BENCHMARK_ITEM_BASENAME = "baseName";
	private static final String BENCHMARK_ITEM_VERSION = "version";

	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return BenchmarkItem.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BenchmarkItem benchmarkItem = (BenchmarkItem) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, BENCHMARK_ITEM_BASENAME, "error.benchmark.item.basename",env.getProperty("error.benchmark.item.basename"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, BENCHMARK_ITEM_VERSION, "error.benchmark.item.version",env.getProperty("error.benchmark.item.version"));
	}
}
