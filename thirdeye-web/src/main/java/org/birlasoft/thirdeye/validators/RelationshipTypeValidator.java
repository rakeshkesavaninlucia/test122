package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class RelationshipTypeValidator implements Validator {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	
	@Override
	public boolean supports(Class<?> clazz) {
		return RelationshipType.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
		  RelationshipType newRelationshipType = (RelationshipType) target;
		  
		  if (StringUtils.isEmpty(newRelationshipType.getParentDescriptor().trim())) {
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parentDescriptor", "relationship.type.parent.descriptor.empty.error");
		  }
		  if (StringUtils.isEmpty(newRelationshipType.getChildDescriptor().trim())){		  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "childDescriptor", "relationship.type.child.descriptor.empty.error");
		  }
		  if(!StringUtils.isEmpty(newRelationshipType.getParentDescriptor()) && !StringUtils.isEmpty(newRelationshipType.getChildDescriptor())) {
			  String displayNameToValidate = newRelationshipType.getParentDescriptor() + " :: " + newRelationshipType.getChildDescriptor();
			  List<RelationshipType> listOfRelationshipType = relationshipTypeService.findByWorkspace(newRelationshipType.getWorkspace());
			  for (RelationshipType relationshipType : listOfRelationshipType) {
				    Integer newRelationshipTypeId = newRelationshipType.getId();
					boolean displayNameCheck = relationshipType.getDisplayName().equalsIgnoreCase(displayNameToValidate);;
					if(newRelationshipTypeId == null && displayNameCheck){
						errors.rejectValue("parentDescriptor", "relationship.type.parent.child.descriptor.unique.error",env.getProperty("relationship.type.parent.child.descriptor.unique.error"));
						errors.rejectValue("childDescriptor", "relationship.type.parent.child.descriptor.unique.error",env.getProperty("relationship.type.parent.child.descriptor.unique.error"));
						break;
					}else if(newRelationshipTypeId != null && !relationshipType.getId().equals(newRelationshipTypeId) && displayNameCheck){
						errors.rejectValue("parentDescriptor", "relationship.type.parent.child.descriptor.unique.error",env.getProperty("relationship.type.parent.child.descriptor.unique.error"));
						errors.rejectValue("childDescriptor", "relationship.type.parent.child.descriptor.unique.error",env.getProperty("relationship.type.parent.child.descriptor.unique.error"));
						break;
					}
			  }
		  }
	}
}
