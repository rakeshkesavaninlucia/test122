package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class UserValidator implements Validator {
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String EMAIL = "emailAddress";
	private static final String USER_PASS = "password";
	private static final String ORGANIZATION_NAME ="organizationName";
	private static final String PHOTO = "photo";
	private static final String COUNTRY ="country";
	private static final String QUESTIONANSWER ="questionAnswer";
	private static final String ROLE ="roleName";

	@Autowired
	private Environment env;

	@Autowired
	private UserService userService; 

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		User user = (User)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIRST_NAME, "error.first.name", env.getProperty("error.first.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, LAST_NAME, "error.last.name", env.getProperty("error.last.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, EMAIL, "error.email", env.getProperty("error.email"));
		
		
		if(user.getUserWorkspaces().isEmpty() && user.getId()==null){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, USER_PASS, "error.password", env.getProperty("error.password"));
		}
		List<User> userall = userService.findAll();
		for (User user2 : userall) {
			if(user.getId() !=null && user.getId().equals(user2.getId()) && user.isDeleteStatus()){
				break;
			}else if(user.getEmailAddress() !=null && user.getEmailAddress().equalsIgnoreCase(user2.getEmailAddress())){
				if(user2.isDeleteStatus()){
					errors.rejectValue(EMAIL, "error.email.deactive", env.getProperty("error.email.deactive "));
				}else if(user.getId() == null){
				   errors.rejectValue(EMAIL, "error.email.exists", env.getProperty("error.email.exists")); 
				}
			}
		}
		if(user.getFirstName()!= null && user.getFirstName().length() > 45){
			
			 errors.rejectValue(FIRST_NAME, "error.first.name.length", env.getProperty("error.first.name.length")); 
		 }
		if(user.getLastName()!= null && user.getLastName().length() > 45){
			
			 errors.rejectValue(LAST_NAME, "error.last.name.length", env.getProperty("error.last.name.length")); 
		 }
	}

	public void validateUserProfile(Object target, Errors errors,MultipartFile fileUpload,List<String> listOfQuestionAnswer){
		User user = (User)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIRST_NAME, "error.first.name", env.getProperty("error.first.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, LAST_NAME, "error.last.name", env.getProperty("error.last.name"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, EMAIL, "error.email", env.getProperty("error.email"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ORGANIZATION_NAME, "error.organization.name", env.getProperty("error.organization.name"));
		if(fileUpload.getSize()>329384){
			errors.rejectValue(PHOTO, "error.photo", env.getProperty("error.photo"));
		}
		if(user.getCountry()!=null && "select".equalsIgnoreCase(user.getCountry())){
			errors.rejectValue(COUNTRY, "error.country", env.getProperty("error.country"));
		}
		
		if("-1".equals(listOfQuestionAnswer.get(0)) || "-1".equals(listOfQuestionAnswer.get(2))){
			errors.rejectValue(QUESTIONANSWER, "error.question", env.getProperty("error.question"));
		}	
	}
	
	
	public void validateUserRoleModel(Role role,Errors errors) {
		
		if (role.getRoleName() != null && role.getRoleName().isEmpty()) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, ROLE, "error.userRole.title", env.getProperty("error.userRole.title "));
		}
	}
}
