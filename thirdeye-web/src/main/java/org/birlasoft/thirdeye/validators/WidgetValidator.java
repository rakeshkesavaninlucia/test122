package org.birlasoft.thirdeye.validators;
import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.widgets.BaseWidgetJSONConfig;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class WidgetValidator implements Validator {
	
	@Autowired
	private Environment env;
	
	@Autowired
	DashboardService dashboardService;
	private static final String TITLE ="title";
	@Override
	public boolean supports(Class<?> clazz) {
	// TODO Auto-generated method stub
	    return BaseWidgetJSONConfig.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		BaseWidgetJSONConfig newWidget = (BaseWidgetJSONConfig)target;
		if(StringUtils.isEmpty(newWidget.getTitle().trim())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "widget.errors.title.empty", env.getProperty("widget.errors.title.empty"));
		}else
		{		  
			// Currently we are not validating on the unique name for dashboards
		}
		List<Widget> listOfWidgets = dashboardService.findAll();
		List<String> listOfJSONWidgetConfig = new ArrayList<>();
		List<String> listOfWidgetTitle = new ArrayList<>();
		listOfWidgets.forEach(oneWidgetConfig->listOfJSONWidgetConfig.add(oneWidgetConfig.getWidgetConfig()));
		List<BaseWidgetJSONConfig> listOfObjectWidgetConfig  = new ArrayList<>();
		for(String mm : listOfJSONWidgetConfig) {
			listOfObjectWidgetConfig.add(Utility.convertJSONStringToObject(mm,BaseWidgetJSONConfig.class));
		}
		listOfObjectWidgetConfig.forEach(widgetTitle->listOfWidgetTitle.add(widgetTitle.getTitle()));
		for(String oneTitle: listOfWidgetTitle) {
			if(oneTitle.equals(newWidget.getTitle())) {
				errors.rejectValue(TITLE,"widget.title.uniqueName", env.getProperty("widget.title.uniqueName"));
				break;
			}
		}
		if(newWidget.getTitle()!= null && newWidget.getTitle().length() > 45){
			
			 errors.rejectValue("title", "error.widget.name.length", env.getProperty("error.widget.name.length")); 
		 }
	}

}
	