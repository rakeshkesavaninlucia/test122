<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryaddasset.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="spiderchart">
		<div style="width: 100%;">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span th:text="${assetBarChartWrapper.label}"></span> :: <span
							th:text="${assetBarChartWrapper.questionnaireName}"></span> :: <span
							th:text="${assetBarChartWrapper.value}"></span>
					</h3>
				</div>
				<div class="box-body" th:if="${assetBarChartWrapper.value gt 0}">
					<div class="radarChart center-block" data-module="charting-spider">
						<script type="text/x-config" th:inline="javascript">{"height":350, "display": {"entity":"parameter", "parameter":{"assetId":[[${assetBarChartWrapper.assetId}]],"questionnaireId":[[${assetBarChartWrapper.questionnaireId}]],"parameterId":[[${assetBarChartWrapper.parameterId}]]}}, "margin" : {"top": 50, "right": 50, "bottom": 50, "left": 50}}</script>
					</div>
				</div>
				<div class="box-body" th:if="${assetBarChartWrapper.value eq 0}">
					<span style="color: red;">No Data Available</span>
				</div>
			</div>
		</div>
	</div>



	<div th:fragment="assetbarchart">
		<div class="chart" style="width: 100%; overflow-x: scroll;">
			<script type="text/x-config" th:inline="javascript">{"display": {"entity":"parameter", "parameter":{"assetId":[[${asset.id}]]}}}</script>
		</div>
	</div>
</body>
</html>