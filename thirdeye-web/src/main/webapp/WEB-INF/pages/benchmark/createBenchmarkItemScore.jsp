<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{page.create.benchmark})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	    <a class="pull-left fa fa-chevron-left btn btn-default" th:href="@{/benchmark/{id}/edit(id=${benchmarkId})}" style="margin-left: 10px;"></a>
		<div >
			<span th:text="#{page.create.benchmark.Item.score}"></span>		
		</div>
	</div>
	<div class="container"  th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary" data-module="module-benchmarkItemScore">
		    		<div id="box-body1" class="box-body" >
						<div th:replace="benchmark/benchmarkItemScoreFragment :: createBenchmarkItemScore"></div>
					</div>	
				</div>
			</div>
		</div>		
	</div>
	<div th:fragment="scriptsContainer">
  		<script th:src="@{/static/js/3rdEye/modules/module-benchmarkItemScore.js}"></script>
	</div>
</body>
</html>