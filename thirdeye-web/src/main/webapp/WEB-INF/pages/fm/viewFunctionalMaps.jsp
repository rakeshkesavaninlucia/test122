<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.functionalmaps.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pages.functionalmaps.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.functionalmaps.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
	                <div class="box-body">
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover">
								<thead>
									<tr>
										<th><span th:text="#{functionalMap.name}"></span></th>
										<th><span th:text="#{functionalMap.bcm}"></span></th>
										<th><span th:text="#{functionalMap.asset.template}"></span></th>
										<th><span th:text="#{functionalMap.type}"></span></th>
										<th><span th:text="#{tabel.header.action}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneFm : ${listOfFm}" th:id="${oneFm.id}">
										<td><span th:text="${oneFm.name}">Test</span></td>
										<td><span th:text="${oneFm.bcm.bcmName}">Test</span></td>
										<td><span th:text="${oneFm.assetTemplate.assetTemplateName}">Test</span></td>
										<td><span th:text="${oneFm.type}">Test</span></td>
										<td><a th:href="@{/fm/edit/{id}(id=${oneFm.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'FUNCTIONAL_MAP_MODIFY'})"></a>&nbsp;								            
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
		            	<div>
					    	<a class="btn btn-primary" th:href="@{/fm/create}" th:text="#{functionalMap.create}" sec:authorize="@securityService.hasPermission({'FUNCTIONAL_MAP_MODIFY'})"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>