<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="parameterTreeView">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Parameter Tree View</h4>
				      </div>
				      <div class="modal-body" style="overflow:hidden;width: 580px;">
				      	<div class="containerParameterTreeView" id="container">
						  <ul>
						    <li><span th:text="${parameterBean.displayName}"></span>
						      <ul th:fragment="oneParameterWithWrapper(listOfParameters)" th:if="${not #lists.isEmpty(listOfParameters)}">
						        <li th:each="oneParameter : ${#sets.toSet(listOfParameters)}">
						        	<span th:text="${oneParameter.displayName}"></span>
						        	<!-- Process children -->
									<div th:replace="param/parameterTreeView :: oneParameterWithWrapper(listOfParameters=${oneParameter.childParameters})" th:if="${not #lists.isEmpty(oneParameter.childParameters)}"></div>
									<div th:replace="param/parameterTreeView :: oneQuestionWithWrapper(listOfQuestions=${oneParameter.childQuestions})" th:if="${not #lists.isEmpty(oneParameter.childQuestions)}"></div>
								</li>
						      </ul>
						      <!-- The fragment for displaying the questions -->
							  <ul th:fragment="oneQuestionWithWrapper(listOfQuestions)" th:if="${not #lists.isEmpty(listOfQuestions)}">
								<li th:each="oneQuestion : ${#sets.toSet(listOfQuestions)}" th:title="${oneQuestion.title}"  data-toggle="popover" data-trigger="hover" data-container="body" data-placement="top">
									<span th:text="${oneQuestion.title}"></span>
								</li>
							</ul>
						    </li>
						  </ul>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				      </div>
			    </div>
		 </div>
	</div>
</body>
</html>