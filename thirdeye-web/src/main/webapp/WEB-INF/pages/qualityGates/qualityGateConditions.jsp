<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.colorscheme.qualitygates.edit.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<a class="pull-left fa fa-chevron-left btn btn-default"
			th:href="@{/qualityGates/view}" style="margin-left: 10px;"></a> <span
			th:text="#{color.quality.gates.edit.condition}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="${qualityGateName}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary"
					data-module="module-qualityGateCondition">
					<div class="box-body">
						<div class="table-responsive">
							<input type="hidden" th:value="${qualityGate }" id="qualityGate" />
							<table class="table table-condensed table-hover">
								<tr>
									<td class="col-md-2 horizontal"><label for="colorScale"
										class="control-label required"
										th:text="#{color.quality.gates.colorScale}"></label></td>
									<td class="col-md-6" data-type="typeColorScale"><select
										th:if="${colorScaleCount eq 0}" class="form-control"
										id="colorScale">
											<option
												th:each="oneScale : ${T(org.birlasoft.thirdeye.colorscheme.constant.ColorScale).values()}"
												th:value="${oneScale.value}" th:text="${oneScale.value}"
												th:selected="${oneScale.value==colorScaleDesc}"></option>
									</select> <span th:if="${colorScaleCount ne 0}"
										th:text="${colorScaleCount }"></span> <input type="hidden"
										th:value="${colorScaleCount }" id="colorScale" /></td>
									<td><label class="control-label required"
										th:text="#{color.quality.gates.condition}"></label>&nbsp;&nbsp;<span
										th:text="${T(org.birlasoft.thirdeye.colorscheme.constant.Condition).LESS.value}"></span></td>
								</tr>
								<tr>
									<td class="col-md-2 horizontal"><label for="parameterId"
										class="control-label required"
										th:text="#{color.quality.gates.parameter}"></label></td>
									<td class="col-md-6"><select
										class="form-control dropdown_select" id="parameterId">
											<option value=""></option>
											<option th:each="oneParameter : ${listOfParameters}"
												th:value="${oneParameter.id}"
												th:text="${oneParameter.displayName}" />
									</select></td>
										
									<td class="col-md-4"><div class="col-md-1"></div>
										<button id="addRowButton" class="btn btn-primary"
											data-type="addConditionRow">Add Row</button></td>

								</tr>
							</table>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-condensed table-hover"	id="qualityGateCondition">
							<tbody>
							  <tr class="colorDescription"></tr>
				
							  <tr th:each="oneCondition : ${listOfConditions}"
									th:include="qualityGates/qualityGateFragments :: viewOnlyCondition"
									th:with="qgCondition=${oneCondition}" />
							</tbody>
						</table>
						<div id="parameterError"></div>
						<div id="descriptionError"></div>
						<div id="conditionError"></div>
					</div>

					<div class="box-footer">
						<p>The color of this quality gate is the weighted average of
							parameters selected above.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer" th:remove="tag">
		<script
			th:src="@{/static/js/3rdEye/modules/module-qualityGateCondition.js}"></script>
	</div>
</body>
</html>