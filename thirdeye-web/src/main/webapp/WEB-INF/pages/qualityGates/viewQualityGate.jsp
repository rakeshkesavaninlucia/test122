<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.colorscheme.qualitygates.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.colorscheme.qualitygates.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.colorscheme.qualitygates.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary" data-module="module-qualityGate">
	                <div class="box-body">
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover">
								<thead>
									<tr>
										<th><span th:text="#{color.quality.gates.name}"></span></th>
										
										<th><span th:text="#{tabel.header.action}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneQualityGate : ${listOfQualityGates}" th:id="${oneQualityGate.id}">
										<td><span th:text="${oneQualityGate.name}"></span></td>
										
										<td><span sec:authorize="@securityService.hasPermission({'COLOR_SCHEME_VIEW'})"><a class="fa fa-pencil-square-o" data-type="editQualityGate" th:attr="data-url=${'qualityGates/edit/' + oneQualityGate.id}" style="cursor: pointer;" th:title="#{icon.title.edit}"></a>
										<a data-type="editQualityGateCondition" th:attr="data-url=${'qualityGates/editCondition/' + oneQualityGate.id}" class="fa fa-plus-square" title="Edit Conditions" style="cursor: pointer;"></a>
										<a data-type="deleteQualityGate" th:attr="data-url=${'qualityGates/delete/' + oneQualityGate.id}" class="fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a></span></td>
									
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
		            	<div>
					    	<a class="btn btn-primary" data-type="createQualityGate" th:text="#{color.quality.gates.create}"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div  id="new" data-module ="module-showHelp">
		<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;top: -460px;"></button>
	</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-qualityGate.js}"></script>
    </div>
</body>
</html>