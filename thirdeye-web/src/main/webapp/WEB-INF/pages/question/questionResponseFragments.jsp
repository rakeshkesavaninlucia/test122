<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	
	<form role="form" th:action="@{/questionnaire/{questionnaireId}/nqrq/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="TEXT(oneQQ)" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	 <input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${oneQQ.sequenceNumber}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <div>
             	<label th:text="#{label.response}">Response</label>
             	<span th:text="${oneQQ.responseText}">Response Text</span>
             </div>
             <div>             	            	
				<div  th:replace="question/questionResponseFragments :: SELECTSCORE(oneQQ=${oneQQ})"></div>
				<span id="TEXT"></span>
             </div>
           </div>
         </div><!-- /.box-body -->
    </form>
		
   <form role="form" th:action="@{/questionnaire/{questionnaireId}/nqrq/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="PARATEXT(oneQQ)" class="editableForm" method="POST">
       <div class="box-body">
         <div class="form-group">
         	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
         	<label th:text="${oneQQ.sequenceNumber}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
           <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
           <div>
           		<label th:text="#{label.response}">Response :- </label>
           		<span th:text="${oneQQ.responseText}">Response Text</span>
           	</div>
           	<div>           		
           		<div  th:replace="question/questionResponseFragments :: SELECTSCORE(oneQQ=${oneQQ})"></div>
           		<span id="PARATEXT"></span>
           </div>
         </div>
       </div><!-- /.box-body -->
   </form>
		             
 	<form role="form" th:fragment="MULTCHOICE(oneQQ)" th:action="@{/questionnaire/{questionnaireId}/nqrq/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" class="editableForm" method="POST">
		 <div class="box-body">
		   <div class="form-group">
		   	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
		    <label th:text="${oneQQ.sequenceNumber}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
		   	<label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
		   	<div th:each="oneOptionResponse : ${oneQQ.mapOfResponseData}">
	    		<div th:if="${#lists.contains(oneOptionResponse.value, 'Other')}">
				   	<div>
				   		<label th:text="#{label.response}">Response :- </label>
				   		<span th:text="${oneOptionResponse.value[0]}"></span>
				   		<span th:text="  [ + ${oneOptionResponse.value[1]} + ]"></span>
				   	</div>
				   	<div>
				   		<div  th:replace="question/questionResponseFragments :: SELECTSCORE(oneQQ=${oneQQ})"></div>
				   		<span id="MULTCHOICE"></span>
				   	</div>
			   	</div>
	    		<div th:if="${not #lists.contains(oneOptionResponse.value, 'Other')}">
				   	<div>
				   		<label th:text="#{label.response}">Response :- </label>
				   		<span th:text="${oneOptionResponse.value[0]}"></span>
				   	</div>
				   	<div>
				   		<label th:text="#{label.score}">Score :- </label>
				   		<span th:text="${oneOptionResponse.value[1]}"></span>
				   	</div>
			   	</div>
		    </div>
		    </div>
		  </div>
     </form>
	
    <form role="form" th:fragment="NUMBER(oneQQ)" th:action="@{/questionnaire/{questionnaireId}/nqrq/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${oneQQ.sequenceNumber}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <div>
             	<label th:text="#{label.response}">Response :- </label>
             	<span th:text="${oneQQ.responseText}"></span>
             </div>
             <div>
             	<div  th:replace="question/questionResponseFragments :: SELECTSCORE(oneQQ=${oneQQ})"></div>
             	<span id="NUMBER"></span>
             </div>
           </div>
         </div><!-- /.box-body -->
    </form>
    
    <form role="form" th:action="@{/questionnaire/{questionnaireId}/nqrq/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="DATE(oneQQ)" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${oneQQ.sequenceNumber}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <div>
             	<label th:text="#{label.response}">Response :- </label>
             	<span th:text="${oneQQ.responseText}"></span>
             </div>
             <div>
	            <div  th:replace="question/questionResponseFragments :: SELECTSCORE(oneQQ=${oneQQ})"></div>
	            <span id="DATE"></span> 
	         </div>
           </div>
         </div><!-- /.box-body -->
    </form>
</body>
<div th:fragment="SELECTSCORE(oneQQ)">
   <label th:text="#{label.score}">Score</label> 
   <select th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneQQ.score}" class="editableParameter" >	
	 <option th:each="oneScore : ${T(org.birlasoft.thirdeye.constant.ScoreType).values()}" th:value="${oneScore.key}" th:text="${oneScore.value}" th:selected="${oneScore.key==oneQQ.score} "></option>
   </select>
</div>

</html>