<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="parameterModal">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
			<form th:action="@{/questionnaire/{questionnaireId}/parameters/save(questionnaireId=${questionnaireId})}" method="post" th:object="${parameterForm}" id="paramFormId">
					<input type="hidden" th:field="*{id}"/>
					
					<input type="hidden" name="parentParam" th:value="${parentParam}"/>
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="exampleModalLabel">Add Parameter</h4>
			      </div>
			      <div class="modal-body">
			          <div class="form-group">
			            <label for="parameter-name" class="control-label">Name:</label>
			            <input type="text" class="form-control" id="parameter-name" th:field="*{uniqueName}" />
			            <span th:if="${#fields.hasErrors('uniqueName')}" th:errors="*{uniqueName}" style="color: red;">Test</span>
			          </div>
			          <div class="form-group">
			            <label for="parameter-description" class="control-label">Description:</label>
			            <textarea class="form-control" id="parameter-description" th:field="*{description}"></textarea>
			          </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <input type="submit" value="Create Param" class="btn btn-primary"></input>
			      </div>
			    </form>
			    </div>
		 </div>
	</div>
	
	<div th:fragment="questionsModal">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
					<form id="questionSelectorId">
					<input type="hidden" name="parentParam" th:value="${parentParam}"/>
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">Select Questions</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive">
				          	<b><span th:text="#{question.select.help}"></span></b>
							<table class="table table-bordered table-striped table-condensed table-hover dataTable" id="questionListInModal">
								<thead>
									<tr>
										<th></th>
										<th><span th:text="#{question.category}"></span></th>
										<th><span th:text="#{question.title.table}"></span></th>
										<th><span th:text="#{question.mandatory}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRow : ${listOfQuestions}">
										<td><input type="checkbox" name="question" class="questionClass" th:value="${oneRow.id}"/></td>
										<td><span th:if="${oneRow.category ne null}" style="font-size: 0.875em;" th:text="${oneRow.category.name}">Test</span></td>
										<td><span style="font-size: 0.875em;" th:text="${oneRow.title}">Test</span></td>
										<td><input type="checkbox" name="mandatoryQuestion" class="mandatoryQuestionClass" th:value="${oneRow.id}"/></td>
									</tr>
								</tbody>
							</table>
						</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" value="Done" class="btn btn-primary"></input>
				      </div>
				    </form>
			    </div>
		 </div>
	</div>
	
	<div th:fragment="parameterFunctionModal">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">Update Parameter Function</h4>
				      </div>
				      <div class="modal-body">
					      <div class="row">
					      	<div class="col-md-12">
							      	<div class="box">
							      		<div class="box-body">
							      			<form id="parameterFunctionFormId">
										      <table class="table table-bordered table-condensed table-hover" >
										      	<thead>
										      		<tr>
										      			<th th:if="${#lists.isEmpty(listOfParameterFunctions)}">Question/Parameter</th>
										      			<th th:if="${#lists.size(listOfParameterFunctions) gt 0 and listOfParameterFunctions.get(0).question ne null}">Question</th>
										      			<th th:if="${#lists.size(listOfParameterFunctions) gt 0 and listOfParameterFunctions.get(0).parameterByChildparameterId ne null}">Parameter</th>
										      			<!-- <th>Range</th> -->
										      			<th>Weight</th>
										      			<th>Constant</th>
										      		</tr>
										      	</thead>
										      	<tbody >
													<tr th:each="oneRow : ${listOfParameterFunctions}" th:id="${oneRow.id}">
												       <td th:if="${oneRow.question ne null}"><span th:text="${oneRow.question.title}"></span></td>
												       <td th:if="${oneRow.parameterByChildparameterId ne null}"><span th:text="${oneRow.parameterByChildparameterId.name}"></span></td>
												       <!-- <td></td> -->
												       <td><input type="number" step="any" name="weight" th:value="${oneRow.weight}" class="editableParameter" style="width: 50px;"/></td>
												       <td><select name="constant" class="editableParameter">
												       <option th:selected="${oneRow.constant == -1}" value="-1">-1</option>
												       	<option th:selected="${oneRow.constant == 1}" value="1">1</option>
												        </select></td>	
													</tr>
										        </tbody>
										       </table>
									       </form>
								       </div>
								    </div>
							    </div>
					       </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
			    </div>
		 </div>
	</div>
</body>
</html>