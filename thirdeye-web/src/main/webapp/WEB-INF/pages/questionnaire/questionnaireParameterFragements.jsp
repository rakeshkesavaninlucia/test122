<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

	<ul class="treeview-menu" th:fragment="oneParameterWithWrapper(listOfParameters)" th:if="${not #lists.isEmpty(listOfParameters)}">
		<li th:fragment="oneParameterAdded(listOfParameters)" th:each="oneParameter : ${listOfParameters}" class="treeview">
			<span class="tools">
				<i class="editNode fa fa-edit" title="Edit"></i>
				<i class="editNodeFunction fa fa-cog" title="Edit Function"></i>  
				<i class="addParameter fa fa-plus" title="Add Parameter"></i>
				<i class="addQuestion fa fa-question" title="Add Question"></i>
				<i class="deleteNode fa fa-trash-o" title="Delete"></i>
			</span>
			<a th:id="${oneParameter.id}" th:attr="data-parentparam=${oneParameter.parent != null ? oneParameter.parent.id : ''}" th:class="${oneParameter.isParent()} == true ? 'parameter isParent' : 'parameter'"> 
				<i class="fa fa-plus-square-o pull-left"></i>
				<i class="fa fa-minus-square-o pull-left"></i>
				<span th:text="${oneParameter.displayName}">Option in tree</span>
			</a>
			<!-- Process children - if the existing param is LP use the question template else use the parameter template. Check the length of children before handing over to fragment-->
			<div th:replace="questionnaire/questionnaireParameterFragements :: oneParameterWithWrapper(listOfParameters=${oneParameter.childParameters})" th:if="${not #lists.isEmpty(oneParameter.childParameters)}"></div>
			<div th:replace="questionnaire/questionnaireParameterFragements :: oneQuestionWithWrapper(listOfQuestions=${oneParameter.childQuestions})" th:if="${not #lists.isEmpty(oneParameter.childQuestions)}"></div>
		</li>
	</ul>
	
	<!-- The fragment for displaying the questions -->
	<ul class="treeview-menu" th:fragment="oneQuestionWithWrapper(listOfQuestions)" th:if="${not #lists.isEmpty(listOfQuestions)}">
		<li th:fragment="oneQuestionAdded(listOfQuestions)" th:each="oneQuestion : ${listOfQuestions}" class="treeview">
			<span class="tools">
				<i class="deleteNode fa fa-trash-o" title="Delete"></i>
			</span>
			<a th:id="${oneQuestion.id}" class="question" th:attr="data-parentparam=${oneQuestion.parentParameter.id}"> 
				<span th:text="${oneQuestion.title}">Leaf Node</span>
			</a>
		</li>
	</ul>
</body>
</html>