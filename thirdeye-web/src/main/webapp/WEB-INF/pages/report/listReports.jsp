<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle =#{pages.reports.mySavedReports.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
    <div th:fragment="pageTitle"><span th:text="#{page.saved.reports}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body" data-module="module-listReports" >
	                        <div th:replace="report/reportModalFragment :: viewReports"></div>	
			        </div>
			          
			    </div>
		    </div>
	    </div>
</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/report/module-listReports.js}"></script>
   </div>
</body>
	</html>