<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="treeMapAsset" class="assetDetail">
		<div class="box-body">
			<div class="table-responsive" data-module="common-data-table">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th><span th:text="#{tco.report.treemap.asset.name}"></span></th>
							<th><span th:text="#{tco.report.treemap.asset.cost}"></span></th>
						</tr>
					</thead>
					<tbody>
						<tr th:each="oneWrapper : ${tcoAssetWrapperList}">
							<td><span th:text="${oneWrapper.assetName}"></span></td>
							<td><span
								th:text="#{tco.report.dollar} + ${oneWrapper.cost}"></span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>