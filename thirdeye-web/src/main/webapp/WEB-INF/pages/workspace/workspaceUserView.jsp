<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',PageTitle ='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{workspace.user}"></span></div>
<div class="container"  th:fragment="contentContainer">
		<div th:replace="assetTemplate/editTemplateFragments :: viewWorkSpaceForm"></div>		
	</div>
</body>
</html>