/**
 * @author: shaishav.dixit
 *  Behavior for pop-over
 */
Box.Application.addBehavior('behavior-popOver', function(context) {
	'use strict';
	var $,$moduleDiv;

	return {

		messages: ['popOver'],
		
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');

		},
		
		onmouseenter: function() {
			$moduleDiv= $(context.getElement());
			$('[data-toggle="popover"]', $moduleDiv).popover();
		}
	}
});