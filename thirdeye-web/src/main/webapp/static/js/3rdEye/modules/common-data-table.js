/**
 * @author: dhruv.sood
 */
Box.Application.addModule('common-data-table', function(context) {
	
	 // private methods here
	var $, commonUtils;
	var $tableWrappingDiv;
	var properties = {
		"bPaginate" : true,
		"bLengthChange" : true,
		"bFilter" : true,
		"bSort" : true,
		"bInfo" : true,
		"bAutoWidth" : true,
		"iDisplayLength" : 10,
		 dom: 'fBitlp',
		 buttons: [
		           {
		              extend: 'collection',
		              text: '<i class="glyphicon glyphicon-cog"/>',
		              buttons: [ 'copyHtml5','csvHtml5', 'excelHtml5','pdfHtml5'],
		           }
		        ]		
	 };
		
return {
    init: function (){
    	// retrieve a reference to jQuery
        $ = context.getGlobal('jQuery');
        // Accessing the service
		commonUtils = context.getService('commonUtils');
        $tableWrappingDiv = $(context.getElement());
        if($tableWrappingDiv.parents().hasClass('modal-body')){
        	 $('table', $tableWrappingDiv).DataTable();
        }else{
        	 $('table', $tableWrappingDiv).DataTable(properties);	
        }       
        $('.overlay', $tableWrappingDiv).hide();
    },
	
	onclick: function(event, element, elementType){
		if (elementType === 'bcmStatusDataType') {
			var dataObj = {};
			dataObj.bcmStatus = element.value;
			var postRequest = $.post( commonUtils.getAppBaseURL("wsbcm/updateStatus"), dataObj);
			postRequest.done(function(htmlResponse){
				if(htmlResponse)
					alert("Status active updated successfully");
				else
					alert("Status active not updated successfully");
			})
		}
	}      
	};
});

