/**
 * This module is for Updating the Facet
 * @author dhruv.sood
 */

Box.Application.addModule('module-facetUpdate', function(context) {
	 
	'use strict';
	
	// private methods here
	var $, commonUtils, moduleDiv;	
	
	var dataObj ={};
	var spinner = null;
	var paramUrl ="&";
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner(moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	//Method to update the tags
	function tagsManipulation(data){
		context.broadcast('updateTag', data);
	}
	
	//Method to generate the filter parameter url
	function prepareRequestParameters(data){
		tagsManipulation(data);		
		if(data.checked==true){
			paramUrl = paramUrl+data.paramValue+"&";
		}else{
			paramUrl = paramUrl.replace(data.paramValue+"&", "");
		}
		return paramUrl;
	}
	
	//Method to get the data for all the facets for side filter	
	function getFacets(data){
		var url = commonUtils.getAppBaseURL("filter/facets");
		var filterURL = commonUtils.readCookie("filterURL");
		
		
		if(filterURL===null && paramUrl==='&'){
			paramUrl = '?';
		}		
		if(filterURL!=null && paramUrl==='&'){
			url = filterURL;
			tagsManipulation(data);	
			if(data.checked==true){
				url = url+data.paramValue+"&";
			}else{
				url = url.replace(data.paramValue+"&", "");
			}
			paramUrl = '?';
		}else{
			if(filterURL!=null){
			   var newUrl = prepareRequestParameters(data);
			   if(data.checked==true){
				   url = url+newUrl;
			   }else{
				   url = filterURL.replace(data.paramValue+"&", "");
			   }
			}else{
				url = url+ prepareRequestParameters(data);
			}
		}
		
		//Adding to filterURL to browser cookie
		document.cookie = "filterURL=" + url + "; path=/";
		context.broadcast('filterSelected');
		fetchFacetData(url);
	}
	
	//Fetch the facet data and append it to filter
	function fetchFacetData(url){
		var getRequest = $.get(url);
		
        getRequest.done(function(incomingData){  
        	showLoading(false);
        	$(moduleDiv).empty().append(incomingData).fadeIn(1300);
		 })
	}
	
	
	return {
		messages: ['assetClassClicked','tagDeleted','fetchFacetData'],

		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');
		},  
		
		onmessage: function(name, data) {
			if(name==='assetClassClicked' || name==='tagDeleted' ){
				showLoading(true);
				getFacets(data);
			}
			if(name==='fetchFacetData'){
				showLoading(true);
				fetchFacetData(data);
			}
        },  	
		
        onclick: function(event, element, elementType) {
        	if(element!=null && elementType!='expandCollapse'){
    			dataObj.checked = element.checked;
    			dataObj.paramValue = elementType;
    			showLoading(true);
    			getFacets(dataObj);
        	}
		}
	};
});