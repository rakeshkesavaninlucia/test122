/** @author: ananta.sethi

 * module-3rdi-Aid.js
 * configure aid report,
 * jsp-aidReportConfiguration.jsp
 * function edit block- help to edit the confiured block for different question,parameter ,inventory
 * function move up & move down -- if two blocks are added in report ,move a specific block position
 * function delete block -- delete the specific block
 * (top right  setting icon-click to edit block,delete block,move up ,move down)
 **/
Box.Application.addModule('module-3rdi-Aid', function(context) {
	'use strict';

	// private methods here
	var $,commonUtils,aidBlockId;
	
	function fetchQuestionValues(qeId){
		var deferred = $.Deferred();
		var urlToHit = commonUtils.getAppBaseURL("aid/configure/subBlockEdit/fetchQuestions/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	} // EOF[_fetchQuestionValues]

	function fetchParameterValues(qeId){
		var deferred = $.Deferred();
		var urlToHit = commonUtils.getAppBaseURL("aid/configure/subBlockEdit/fetchParameters/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	} // EOF[_fetchQuestionValues]
	
	// When the modal finishes its work refresh the data
	function editModalDoWork(aidBlockId){

		var $editModalsWorkDeferred =  $.Deferred();
		// Fetch modal content 
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL("aid/configure/blockedit/"+aidBlockId), {}).then(function(content){
		var $newModal = commonUtils.appendModalToPage("blockEditModal", content, true);
		bindEditModalRequirements($newModal, $editModalsWorkDeferred);
		$newModal.modal({backdrop:false,show:true});
		});

		function bindEditModalRequirements ($modalRef, $editModalsWorkDeferred){
			$('#blockEditForm', $modalRef).ajaxForm(function(htmlResponse) {
				if (htmlResponse.configUpdated){
					$modalRef.modal("hide");
					$editModalsWorkDeferred.resolve();
				} else{
					var $replacement = commonUtils.swapModalContent("blockEditModal", htmlResponse);
					$replacement.modal({backdrop:false});
					bindEditModalRequirements($replacement, $editModalsWorkDeferred);
				}
			});
			bindClickForSubBlockConfig($modalRef);
			$modalRef.on('hidden.bs.modal', function () {
				refreshCentralAppDisplay();
			});
			// Bind block specific handlers
		}
		return $editModalsWorkDeferred;
	}
	function bindClickForSubBlockConfig($parentModal){
		$(".configureSubBlock",$parentModal).click(function(){
			var subBlockIdentifier = $(this).data("subblockid");
			// Fetch modal content 
			commonUtils.getHTMLContent(commonUtils.getAppBaseURL("aid/configure/subBlockEdit/"+aidBlockId), {subBlockIdentifier: subBlockIdentifier}).then(function(content){
				var $newModal = commonUtils.appendModalToPage("subBlockEditModal", content, true);
				bindForSubBlockModal($newModal, $parentModal);
				$newModal.modal({backdrop:false,show:true});
			});
		});
	}
	function bindForSubBlockModal($modalRef, $parentModal){

		$("#submitSubBlockForm",$modalRef).click(function(){
			var activeTab = $(".tab-pane.active").attr("id");
			$("input[name=activeTab]", $('#subBlockForm', $modalRef)).val(activeTab);
			$('#subBlockForm', $modalRef).submit();
		});
		$('#subBlockForm', $modalRef).ajaxForm(function(htmlResponse) {
			if (htmlResponse.configUpdated){
				// Close the current modal
				$modalRef.modal("hide");
				var $formInParentModal = $("form", $parentModal);
				$('<input>').attr({
					type: 'hidden',
					name: 'refreshModal',
					value: true
				}).appendTo($formInParentModal);
				$formInParentModal.submit();
			}else{
				var $replacement = commonUtils.swapModalContent("subBlockEditModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindForSubBlockModal($replacement, $editModalsWorkDeferred);
			}
		});
		$(".ajaxSelector", $modalRef).change(function(){
			var $selector = $(this);
			// find value 
			if ($selector.data("dependency") === "qeForQuestion"){
				fetchQuestionValues($selector.val()).then(function(htmlResponse){
					var depName = $selector.data("dependency");
					var depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
					// Scope to the widget
					var $matchingSelectors = $(depSelector,$modalRef);
					$matchingSelectors.empty();
					$matchingSelectors.append(htmlResponse);
				});
			} else if ($selector.data("dependency") === "qeForParameter"){
				fetchParameterValues($selector.val()).then(function(htmlResponse){
					var depName = $selector.data("dependency");
					var depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
					// Scope to the widget
					var $matchingSelectors = $(depSelector,$modalRef);
					$matchingSelectors.empty();
					$matchingSelectors.append(htmlResponse);
				});
			}
		});
	}

	function bindAIDConfigurationClicks(){
		$(".addAIDBlock").click(function(){
			var aidReportId = $(".centralAssetWrapper .centralAsset").first().data("aidreportid");
			var blockType = $(this).data("blocktype");
			var dataObj = {};
			dataObj["blockType"] = blockType;
			var postRequest = $.post( commonUtils.getAppBaseURL("aid/configure/"+aidReportId+"/addBlock"), dataObj);
			postRequest.done(function(){
				refreshCentralAppDisplay();
			});
		});
		$('.content-wrapper').on('click', "[data-aidblockcontrol='editblock']", editBlock);
		$('.content-wrapper').on('click', "[data-aidblockcontrol='moveup']", {direction:-1},moveBlock);
		$('.content-wrapper').on('click', "[data-aidblockcontrol='movedown']",{direction:+1}, moveBlock);
		$('.content-wrapper').on('click', "[data-aidblockcontrol='deleteblock']", deleteBlock);
	}

	function editBlock(){
		aidBlockId = $(this).data("aidblockid");
		// Show modal window 
		editModalDoWork(aidBlockId).then (function(){
			// Anything else ?
		});
	}
	/**
	 * Move the block in a direction given by the user.
	 * Help in configuring the blocks.
	 */
	function moveBlock (event){
		var aidBlockId = $(this).data("aidblockid") ;
		var aidReportId = $(".centralAssetWrapper .centralAsset").first().data("aidreportid");
		var dataObj = {};
		dataObj["direction"] = event.data.direction;
		var postRequest = $.post( commonUtils.getAppBaseURL("aid/configure/"+aidReportId+"/moveBlock/" + aidBlockId), dataObj);
		postRequest.done(function(){
			refreshCentralAppDisplay();
		});
	}

	function deleteBlock(){
		if (confirm("Are you sure you want to delete this block from your report ?")){
			var aidBlockId = $(this).data("aidblockid") ;
			var aidReportId = $(".centralAssetWrapper .centralAsset").first().data("aidreportid");
			var dataObj = {};
			var postRequest = $.post( commonUtils.getAppBaseURL("aid/configure/"+aidReportId+"/deleteBlock/" + aidBlockId), dataObj);
			postRequest.done(function(){
				refreshCentralAppDisplay();
			});
		}
	}

	function refreshCentralAppDisplay(){
		var aidReportId = $(".centralAssetWrapper .centralAsset").first().data("aidreportid");
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL("aid/configure/"+aidReportId+"/refreshReport"), {}).then(function(content){
			var $newContent = $(content);
			if ($newContent.is(".centralAssetWrapper")){
				$newContent.replaceAll($(".centralAssetWrapper"));
			}
		});
	}
	return{
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			bindAIDConfigurationClicks();
		}
	};
});