/**
 * @author: ananta.sethi 
 * module- for adding select 2 behavior in create user page for workspace dropdown
 */
Box.Application.addModule('module-createUser', function(context) {

	
	return {
		behaviors : [ 'behavior-select2'],
	}
	
})
