Box.Application.addModule('module-deleteWorkspace', function(context) {
	var $,commonUtils,moduleEl;
	var dataObj = {},notifyService,_moduleConfig;
	var workspaceId;
	
	function addWorkspaceToUser(workspaceId){		
		var table = $('.workspacetable',moduleEl).DataTable();
		dataObj.userids = table.$('.userChecked:checked').map(function() { return this.value; }).get();
		dataObj.adminWsid = $("#workpsaceSelection",moduleEl).find("select[name='workpsaceId']").val();
		dataObj.workspaceId = workspaceId;
		if(dataObj.userids.length ===0){
			alert("Please Select the checkbox");
		}
		else if (dataObj.adminWsid === "-1"){
			alert("Please Choose the Workspace");
		} else{
		getAssignWorkspace(dataObj);
		}
	}
	
	function createWorkspace(){
		window.location.href = commonUtils.getAppBaseURL("workspace/create");
		
	}
	function getAssignWorkspace(dataObj){
		var url = commonUtils.getAppBaseURL("workspace/add");
		var postRequest = $.post(url,dataObj);
		postRequest.done(function(incomingData){
			notifyService.setNotification("Workspace Assigned Successfully", "", "success");	
			location.reload();
		});

	}
	
	function deleteWorkspace(workspaceId,dataobj1){
		dataObj.workspaceId = workspaceId;
		var url = commonUtils.getAppBaseURL("workspace/deleteWorkspace");
		if(dataobj1.error === "true"){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
		var getRequest = $.get(url,dataObj);
		getRequest.done(function(htmlResponse){
		notifyService.setNotification("Workspace deleted successfully", "", "success");
		window.location.replace(commonUtils.getAppBaseURL("workspace/list"));
		});
		}
	}
	
	  return {
		  behaviors : [ 'behavior-select2'],
	        init: function (){
	        	// retrieve a reference to jQuery
	            $ = context.getGlobal('jQuery');
	            commonUtils = context.getService('commonUtils'); 
	            _moduleConfig = context.getConfig(); 
	            moduleEl = context.getElement();
			    notifyService = context.getService('service-notify');
	        },
	            
	        onclick: function(event, element, elementType) {
	        	if (elementType === 'add') {
	        		workspaceId = $(element).data("wwsid");
	         		addWorkspaceToUser(workspaceId);
	         		event.preventDefault();
	         	}
	        	if(elementType === 'delete')
	         	{
	         		var dataobj1= {};
	    			dataobj1.error= _moduleConfig.error;
	    			workspaceId = $(element).data("wwsid");
	    			if (confirm("This item will be permanently deleted and cannot be recovered. Are you sure?") === true) {
	         		deleteWorkspace(workspaceId,dataobj1);
	    			}
	         	}
	         	if(elementType==='create')
	         		{
	         		createWorkspace();
	         		}
	        }	
	    };
	
	
});