/**
 * 
 */

Box.Application.addModule('module-drilldown-parameter', function(context) {

    // private methods here
	var $,nv,d3;
	var commonUtils, $modalDiv, jsonService;
	var dataObj = {};
	function swipeModalContent(){
		
	}
	
	/* Not in use : used for slide show on drilldown
	 * function slideDrill(data){
		
		var url = commonUtils.getAppBaseURL("parameter/drill/down")
			commonUtils.getHTMLContent(url, data).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			var $response = $(htmlResponse);
			
			// This searches for the modal-dialog class and only then
			// replaces the content
			if ($response.has(".modal-dialog").length){
				var $modalBody = $(".modal-dialog", $response);
//				$modalDialogIncoming.hide();
				// $(".modal-dialog",$modalDiv).append($modalBody);
				
				$(".modal-dialog", $modalDiv).hide('slide',{direction:"left"} ,function(){
					$(this).show('slide',{direction:"right"});
				    $(this).replaceWith( $modalBody );
				    Box.Application.startAll($modalBody);
				});
			} else {
				// Indicate closure of modal
			}
		});
	}*/
	
	function setupModal(){
		var ele = $(context.getElement());
		ele.addClass( "modal" );
		ele.addClass( "fade" );
		ele.modal({
			show : false
		});
		ele.on('hidden.bs.modal', function (e) {
			  // do something...
			modalVisible = false;
		})
		$modalDiv = ele;
	}
	
	// Show the modal 
	var modalVisible = false;
	
	function showModal(){
		if (!modalVisible){
			$modalDiv.modal('show');
			$modalDiv.modal('handleUpdate');
			modalVisible = true;
		}
	}
	
	function hideModal(){
		if (modalVisible){
			$modalDiv.modal('hide');
			modalVisible = false;
		}
	}
	
	function loadModalContent(data){
		
		//var url = commonUtils.getAppBaseURL("parameter/drill/down/12")
		var url = commonUtils.getAppBaseURL("parameter/drill/down")
		
		commonUtils.getHTMLContent(url, data).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			var $response = $(htmlResponse);
			
			// This searches for the modal-dialog class and only then
			// replaces the content
			if ($response.has(".modal-dialog").length){
				$modalDiv.empty();
				var $modalDialogIncoming = $(".modal-dialog", $response);
//				$modalDialogIncoming.hide();
				$modalDiv.append($modalDialogIncoming);
				
				Box.Application.startAll($modalDiv);
				// Box.app.StartALL- what ever is the syntax
				$modalDialogIncoming.show();
			} else {
				// Indicate closure of modal
			}
		});
	}
	
	/*function loadModel(data){
		var vis2 = data.vis2;
		var chart2 = data.chart2; 
		var $response = vis2.transition().duration(1000).call(chart2);
	    alert($response);
	}
	function fetchDataForModelContent(dataObj){
		var pageUrl = "parameter/drill/down";		
		var deffMassage = $.Deferred();
		var dataPromise = jsonService.getJsonResponse(commonUtils.getAppBaseURL(pageUrl),dataObj); 
		dataPromise.done ( function (data) {
	        // need to massage it
			deffMassage.resolve (data);  
			context.broadcast('bc::showBulletChart', data);
	    })
	    
	    .fail (function (error) {
	        console.log (error);
	        deffMassage.reject(error);
	    });
		
	    return deffMassage.promise();
	}*/
	

    return {
    	messages: [ 'pm::drilldownrequest', 'bc::bulletChartDone' ],
    	
		 init: function (){
	     	// retrieve a reference to jQuery
	         $ = context.getGlobal('jQuery');
	         commonUtils = context.getService('commonUtils');
	         jsonService = context.getService('jsonService');
	         d3 = context.getGlobal('d3');
	         nv = context.getGlobal('nv');
	         setupModal();
	     },
	     
	     destroy: function() {
			 dataObj = {};
         },   
    	

        onmessage: function(name, data) {

            if (name === 'pm::drilldownrequest') {
            	// Fetch Content -  Show loading on modal
            	loadModalContent(data);
            	
            	// Receive Content - replace            	
            	// Hide loading            	
            	showModal();
            }
        },
    
       
        //commenting onclick because relationship treeview on viewAsset.jsp was not working
        onclick: function(event, element, elementType) {
        	if (elementType === 'drilldown') {
        		// Go drilling to the next level        		
        		dataObj.parameterId = $(element).data("parameterid");
        		dataObj.questionnaireId = $(element).data("questionnaireid");        		
        		dataObj.assetId = $(element).data("assetid");        
        		dataObj.rootParameterId = $(element).data("rootparameterid");  
        		dataObj.parentParameterId = $(element).data("parentparameterid");
        		loadModalContent(dataObj);
        	} else if (elementType === 'go-up') {
        		dataObj.parameterId = $(element).data("parameterid");
        		dataObj.questionnaireId = $(element).data("questionnaireid");        		
        		dataObj.assetId = $(element).data("assetid");
        		dataObj.rootParameterId = $(element).data("rootparameterid");  
        		dataObj.parentParameterId =  $(element).data("parentparameterid");
        		if(dataObj.parameterId){
        			//dataObj.parameterId = dataObj.parentParameterId;        		
        			loadModalContent(dataObj);
        		}else{
        			hideModal();
        		}
        	}
        }
        	
        /*
        	When modal loads for the first time, the html comes from the server and then you show the modal
        	the modal captures click for an on screen element and refreshes the page (from server) when it drills down.
        	The drill up should happen based on a drill up button which will have the parent parameter to go back to.
        	it might be better to create a bread crumb with links to the level and that will automatically update the modal.
        	
        	
        	modal refresh
        	1. Call server with params
        	2. accept HTML 
        	3. switch modal content only - lets not change header / footer
        	
        	the event you catch:
        	1. drill down click
        	2. go back up click
        	
        	what you show:
        	the entity that you have drilled down
        	child entities with score and weight - dont show the portfolio average for the time being
        	
        	
        	
        */

    };
});