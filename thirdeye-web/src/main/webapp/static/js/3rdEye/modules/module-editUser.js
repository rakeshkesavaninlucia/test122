/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-editUser', function(context) {
	'use strict';
	var $,commonUtils;


	return {
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("workspace/delete");
			$("#userWorkspaceView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
			
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("user/delete");
			$("#userManagementId").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		}
	}
});
