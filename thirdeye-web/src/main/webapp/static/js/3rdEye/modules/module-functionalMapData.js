/**
 * 
 */

Box.Application.addModule('module-functionalMapData', function(context) {

    // private methods here
	var $, commonUtils,moduleEl, _moduleConfig;
	var dataObj = {};
	var pageUrl = "fm/fetchFMDataModal";
	
	function showFunctionalMapDataModel(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})
		});	
	}
	function hideParameterSelectorModel(){
		$(moduleEl).modal('hide');
	}
	
   
    return {

    	messages: [ 'fm::showFunctionalMapData'],

        onmessage: function(name, data) {
            if (name === 'fm::showFunctionalMapData') {
            	if(_moduleConfig.mode === 'viewFunctinalMapdata'){
            		dataObj.fmId = data.fmId;
            		dataObj.qeId = data.qeId;
            		showFunctionalMapDataModel(dataObj);
            	}
            } 
        },
        
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            _moduleConfig = context.getConfig();
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	_moduleConfig = null;    // clear the reference
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});