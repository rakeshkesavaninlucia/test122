/**
*This module handle all the asset classes that are displayed on the home page.
*This method also handle's the parameter tile async loading and click on the home page.
*
*@author: shaishav.dixit  
*/

Box.Application.addModule('module-homeAssetClass', function(context) {
	 'use strict';

	// private methods here
	var $, $moduleDiv, commonUtils;
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner($moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	//This function retrives the parameter values from the controller and populates it
	function getAssetClasses(){
        var url = commonUtils.getAppBaseURL("homeAssetClasses");
        var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			url = url + "?" + urlData;
		}
        var postRequest = $.get(url);
        postRequest.done(function(incomingData){  
        	showLoading(false);
        	$moduleDiv.empty().append(incomingData).fadeIn(1300);
        	context.broadcast('lazyLoad', urlData);
		 });
     }
	
	//This function handles the request of the click on any of the parameter on the home page.
	function parameterClickThrough(parameterId){
		window.location.href = commonUtils.getAppBaseURL("assetParameterChart/" + parameterId);		
	}
	
	return {
		
		behaviors: ['behavior-lazyLoaded','behavior-popOver'],
		
		messages: ['filterSelected'],
		
		 init: function (){
			// Retrieving the references
			$ = context.getGlobal('jQuery');
			$moduleDiv= $(context.getElement());
			commonUtils = context.getService('commonUtils');
			
			//Get the parameters for the json data
			getAssetClasses();			
			
			showLoading(true);
		 },
		 
		 onmessage: function(name, data) {
			 if(name === 'filterSelected') {
				 getAssetClasses();
			 }
		 },
		 
		 onclick: function(event, element, elementType) {	
			 if(elementType === 'parameterTile'){
				 parameterClickThrough($(element).data('paramid'));
			 }
	     }
		 
	};
});
	
