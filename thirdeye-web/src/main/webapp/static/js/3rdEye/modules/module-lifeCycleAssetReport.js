/**
 * This module is for lifeCycle report
 * @author ananta.sethi
 */

Box.Application.addModule('module-lifeCycleAssetReport', function(context) {

	// private methods here
	var $, commonUtils, moduleDiv;	
	
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner(moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	
	function getAssetTable(data){
		
			var postRequest = $.post(commonUtils.getAppBaseURL("assetLifeCycle/getAsset"),data);
			$('#assetlifeCycle', moduleDiv).remove()
			postRequest.done(function(incomingData){
				showLoading(false);	
				$(moduleDiv).append(incomingData).fadeIn(1300); 
	        	Box.Application.startAll(moduleDiv);
			});
			lifeCycleTable();
		}
	function lifeCycleTable(){
		$('#assetlifeCycle1').DataTable({
			scrollY:    	500,
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			bInfo : 		false,
			bLengthChange : false,
			bFilter :		false,
			ordering:		false,
			fixedColumns: true				
		} );
	}       	

	
	return {
		messages: ['assetSelected'],
		behaviors : [ 'behavior-select2' ], 
		init: function (){
			
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');
			

		},  	
		onmessage: function(name, data) {
			
			if(name === 'assetSelected'){
        		showLoading(true);
        		//Get the parameter table
        		getAssetTable(data);        		
    	}		
        },
	};
});