/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-qualityGateCondition', function(context) {

	var $,commonUtils,colors, moduleEl, notifyService, selectedColorScale;
	var dataObj = {};
	
	function addCondition(data){
		addRowToTableFromURL("qualityGateCondition",commonUtils.getAppBaseURL("qualityGates/newRow"), data);
	}
	
	function addRowToTableFromURL(tableId, urlToHit, data){
		if ($("#"+tableId+" tbody").length === 0){
			$("#"+tableId).append("<tbody></tbody>");
		}
		commonUtils.getHTMLContent( urlToHit, data).then(function( newRowContent) {
			if($(newRowContent).hasClass('error_msg')){
				$('#parameterError', moduleEl).empty();
				$('#parameterError', moduleEl).append(newRowContent);
				//notify
				notifyService.setNotification("Field Missing", "", "error");
			} else {				
				$('#parameterError', moduleEl).empty();
				$("#colorScale").prop("disabled", true);
				$("#"+tableId+" tbody").append(newRowContent);
			}
			Box.Application.startAll(moduleEl);
		});		
	}
	
	function saveDescription(data, $trRef){
		commonUtils.getHTMLContent( commonUtils.getAppBaseURL("qualityGates/saveDescription"), data).then(function( incomingData ) {
			if($(incomingData).hasClass('error_msg')){
				$('#descriptionError', moduleEl).empty();
				$('#descriptionError', moduleEl).append(incomingData);
				//notify
				notifyService.setNotification("Field Missing", "", "error");
			} else {
				$('#descriptionError', moduleEl).empty();
				$trRef.replaceWith(incomingData);
				//notify
				if($(incomingData).find('.error_msg').size()>0){
					notifyService.setNotification("Field Missing", "", "error");
				}
				else{
					notifyService.setNotification("Saved", "", "success");	
				}
			}
		});
	}
	
	function saveRow(data, $trRef){
		commonUtils.getHTMLContent( commonUtils.getAppBaseURL("qualityGates/saveRow"), data).then(function( incomingData ) {
			if($(incomingData).hasClass('error_msg')){
				$('#conditionError', moduleEl).empty();
				$('#conditionError', moduleEl).append(incomingData);
				//notify
				notifyService.setNotification("Field Missing", "", "error");
			} else {
				$('#conditionError', moduleEl).empty();
				$trRef.replaceWith(incomingData);
				//notify
				if($(incomingData).find('.error_msg').size()>0){
					notifyService.setNotification("Field Missing", "", "error");
				}
				else{
					notifyService.setNotification("Saved", "", "success");					
				}
			}
		});
	}
	
	function editCondition($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/editRow/"+$trRef.attr("id")), data);
		postRequest.done(function(incomingData){
			$trRef = $(incomingData).replaceAll($trRef);
			$trRef.addClass("pre-save is-saving");
		});
	}
	
	function editDescription($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/editDescription"), data);
		postRequest.done(function(incomingData){
			$trRef = $(incomingData).replaceAll($trRef);
			$trRef.addClass("pre-save is-saving");
		});
	}
	
	function deleteCondition($trRef, data){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/deleteRow"), data);
		postRequest.done(function(){
			$trRef.remove();
			//notify
			notifyService.setNotification("Deleted", "", "success");
		});
	}
	
	function showQGColorDescription(){
		dataObj = {};
		dataObj.qualityGateId = $('#qualityGate').val();
		selectedColorScale = $('#colorScale').val(); 
		if(selectedColorScale == 2) {
			dataObj.scale = ['#d95f02', '#1b9e77'];
		} else {
			dataObj.scale = colors.RdYlGn[selectedColorScale];
		}
		var postRequest = $.post(commonUtils.getAppBaseURL("qualityGates/addDescription"),dataObj);
		postRequest.done(function(incomingData){
			$(".colorDescription").empty();
			$(".colorDescription").replaceWith(incomingData);
		})
	}
	
	function addQualityGateDescription(dataObj){
		var postRequest = $.post( commonUtils.getAppBaseURL("qualityGates/addDescription"), dataObj);
		postRequest.done(function(incomingData){
			$(".colorDescription").replaceWith(incomingData);
			$("#colorDescription").replaceWith(incomingData);
		})
	}

    return {
    	messages: ['qgd::addQualityGateDescription'],
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            notifyService = context.getService('service-notify');
            colors = context.getGlobal('colorbrewer');
            moduleEl = context.getElement();
            
            $(".dropdown_select").select2({
				  placeholder: "Select",
				  allowClear: true
			});
            
            showQGColorDescription();
        },        
  
        onmessage: function(name, data) {
            if (name === 'qgd::addQualityGateDescription') {
            		addQualityGateDescription(data);
            }
        },
        
        onchange: function(event, element, elementType) {
	    	  if(elementType === 'typeColorScale'){
	    		  var $selectedElementValue = $('#colorScale').val();
	    		    dataObj.action = "change";
	    			dataObj.scale = colors.RdYlGn[$selectedElementValue];
	    			addQualityGateDescription(dataObj);
	    	  }
	     },
	     
        onclick: function(event, element, elementType) {
        	var scale;
        	var $trRef;
        	if (elementType === "saveDescription") {
        		dataObj = {};
        		$trRef = $(event.target).closest( "tr" );
        		var description = $('.dirtyrow').find("input[name='description']");
        		var descriptioncolors = $('.dirtyrow').find("input[name='descriptionColor']");
        		scale = $('#colorScale').val();
        		dataObj.description = [scale];
        		dataObj.color = [scale];
        		dataObj.qualityGateId = $('#qualityGate').val();
        		$.each(descriptioncolors, function(i){
        			dataObj.color[i] = descriptioncolors[i].value;
        		});
        		$.each(description, function(i){
        			dataObj.description[i] = description[i].value;
        		});
        		saveDescription(dataObj, $trRef);
        	} else if (elementType === 'editDescription') {
        		$trRef = $(event.target).closest( "tr" );
        		$trRef.addClass("pre-save");
				$trRef.addClass("is-saving");
        		dataObj.id = $('#qualityGate').val();
        		editDescription($trRef, dataObj);
        	}
        	if (elementType === 'addConditionRow') {
        		dataObj = {};
        		dataObj.parameterId = $('#parameterId').val();
        		scale = $('#colorScale').val();
        		var colordescription = $('.dirtyrow').find("input[name='description']");
        		dataObj.colordescription = [scale];
        		$.each(colordescription, function(i){
        			dataObj.colordescription[i] = colordescription[i].value;
        		});	
        		dataObj.scale = colors.RdYlGn[scale];
        		dataObj.qualityGateId = $('#qualityGate').val();
        		addCondition(dataObj);
        	} else if (elementType === 'saveRow') {
        		$trRef = $(event.target).closest( "tr" );
        		var values = $('.dirtyrow').find("input[name='conditionValue']");
        		var conditioncolors = $('.dirtyrow').find("input[name='conditionColor']");
        		scale = $('#colorScale').val();
        		dataObj.value = [scale];
        		dataObj.color = [scale];
        		$.each(conditioncolors, function(i){
        			dataObj.color[i] = conditioncolors[i].value;
				});
        		$.each(values, function(i){
        			dataObj.value[i] = values[i].value;
				});
        		dataObj.weight = $('#weight').val();
        		saveRow(dataObj, $trRef);
        	} else if (elementType === 'editRow') {
        		$trRef = $(event.target).closest( "tr" );
        		$trRef.addClass("pre-save");
				$trRef.addClass("is-saving");
        		dataObj.id = $trRef.attr("id");
        		editCondition($trRef, dataObj);
        	} else if (elementType === 'deleteRow') {
        		$trRef = $(event.target).closest( "tr" );
        		dataObj.id = $trRef.attr("id");
        		deleteCondition($trRef, dataObj);
        	}
        },
    };
});