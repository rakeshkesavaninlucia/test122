/**
 * @author: Manmeet Singh
 * Behavior for show help
 */
Box.Application.addModule('module-showHelp', function(context) {
	'use strict';
	var $;
	//private method.
	function showHelp(){
		//this is the name of the domain or the root url you want for the help link
		var pagePrefix = "/thirdeye-web/static/help/";
		//this is the help extension that will replace whatever exists on the current page
		var helpExtension = ".html"
		//this gets the current full path to the page
		var pageName = window.location.pathname;
		var parts = pageName.split('/');
		var vpageName= parts[2];
		//this returns just the page name and its extension
	    pageName = pageName.substring(pageName.lastIndexOf('/') + 1);
		//this identifies just the page extension of the current page
		var pageExtension = pageName.substring(pageName.lastIndexOf('.'));
		if(parts.length==6)
			pageExtension=parts[4];
		//get page navigation index
		var pageindex="#"+ pageExtension;		
		//this replaces the current page name extension with the help extension
		pageName = pagePrefix + vpageName +".html"+pageindex;		
		//this is the popup script for the new window
		var myWindow = window.open(pageName, "tinyWindow", 'scrollbars=yes,menubar=no,height=450,width=900,resizable=yes,toolbar=no,location=no,status=no');
		//this assures the window will come to the front of the current page
		myWindow.focus()
	}

	return {
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');

		},

		onclick:function(event, element,elementType){
			if(elementType ==="help"){
				showHelp();
			}
		}


	}
});