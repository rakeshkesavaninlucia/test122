/**
 * @author: ananta.sethi 
 * module for view asset-(delete relationship(when click
 *   on a asset ,details show up-information about asset,relationship))
 * 
 */
Box.Application.addModule('module-viewAsset',function(context) {
	'use strict';
	var $, commonUtils;

	return {

		// Initialize the page
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			
			$('#goBack').click(function(){
				history.back();
			});

			
			$('.deleteRelationship').click(function(){

				if (confirm("This item will be permanently deleted and cannot be recovered. Are you sure?") == true) 
				{
					var params = {};
					params["assetId"] = $(this).data("assetid");
					params["relationshipAssetId"] = $(this).data("relationshipassetid");
					var assetId = params["assetId"];
					var postRequest = $.post(commonUtils.getAppBaseURL("relationship/asset/delete"),params);
					postRequest.done(function(incomingData){
						var url = "templates/asset/view/"+ assetId;
						window.location.href = commonUtils.getAppBaseURL(url);
					})
				}
			});
			$.AdminLTE.tree('.parameterTree');
		},
		
		onclick: function(event, element, elementType) {
	    	  if(elementType === "addRelationshipButton"){
	    		  context.broadcast('rtm::relationshipTemplateListModal');
	    	  }
	    	 
	     }
	}
});