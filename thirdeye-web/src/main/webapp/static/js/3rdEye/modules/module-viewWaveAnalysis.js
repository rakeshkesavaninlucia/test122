/**
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-viewWaveAnalysis', function(context) {
	'use strict';
	// private methods here
	var $, commonUtils,colors, $moduleDiv,moduleConfig;
	var spinner = null;
    var scatterWrapperData = null;
    var noOfWaves;
    var lineCoordinates=[];
    //Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function showScatterGraph(scatterWrapper,waveAxis) {

		var dataToPlot = [];
		lineCoordinates = waveAxis;
		$ = context.getGlobal('jQuery');
		$.each(scatterWrapper.series, function(index, oneSeries) {
			var oneSeriesForPlotting = {};
			oneSeriesForPlotting.key = oneSeries.seriesName;
			oneSeriesForPlotting.values = [];
			oneSeriesForPlotting.color =  oneSeries.seriesColor;
			$.each(oneSeries.dataPoints, function(index2, oneCoordinate) {
				oneSeriesForPlotting.values.push({
					x : oneCoordinate.x,
					y : oneCoordinate.y,
					size : oneCoordinate.z,
					shape : 'circle',
					label : oneCoordinate.label
				});
			});
			dataToPlot.push(oneSeriesForPlotting);
		});

		if(scatterWrapper.series.length > 0){			
			$(".graphTitle").text(scatterWrapper.graphName);
		}
		nv.addGraph(function() {
			var chart = nv.models.scatterChart().showDistX(false).showDistY(false).color(function(d, i) {
				var colors = dataToPlot.color;
				return colors[i % colors.length - 1];
			});
			//.color(d3.scale.category10().range());

			//Configure how the tooltip looks.
			chart.tooltip.contentGenerator(function(obj) {
				return '<h3>' + obj.point.label + '</h3>';
			});

			chart.tooltip.enabled();
			chart.forceY([ 0, 1 ]);
			chart.forceX([ 0, 1 ]);

			chart.xAxis.tickFormat(d3.format('.02f'));
			chart.yAxis.tickFormat(d3.format('.02f'));

			chart.xAxis.axisLabel(scatterWrapper.xAxisLabel);
			chart.yAxis.axisLabel(scatterWrapper.yAxisLabel);

			d3.select('#chart svg').datum(dataToPlot).transition().duration(1000).call(chart);
			
			nv.utils.windowResize(chart.update());

			return chart;
		});
	}
	
	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,		
			success : function(scatterWrapper) {
				showScatterGraph(scatterWrapper);
				showLoading($('.scatterPlot'), false);
				showWavesPoint(scatterWrapper);
				if(scatterWrapper.series != null && scatterWrapper.series.length > 0){
					$("#saveAs").show();
				}else{
					$("#saveAs").hide();
				}
			}
		}
		$("#scatterGraphControl").ajaxForm(ajaxFormOptions);
	}
	
	function showWavesPoint(scatterWrapper){
		scatterWrapperData = scatterWrapper;
		$("#waveAnalysis").show();
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.scatterPlot'), true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
	}
	function divideScatterIntoWaveSeries(scatterWrapperData,waveAxis){
		var scatterObj    = prepareScatterObj(scatterWrapperData);
		
		$.each(scatterWrapperData.series, function(i, oneSeries) {
			var waveSeries = prepareListOfWebSeries(noOfWaves);
			$.each(oneSeries.dataPoints, function(j, oneCoordinate) {
				var index = 0;
				prepareSeries(index ,oneCoordinate, waveAxis,waveSeries)
			});
			for(var k = 0;k< noOfWaves; k++){
				scatterObj.series.push(waveSeries[k]);	
			}
		});
		
		return scatterObj;
	}
	
	function prepareSeries(index ,oneCoordinate, waveAxis,waveSeries){
		var s =  checkCoordinatePosition(oneCoordinate,waveAxis[index]);
		   if(s<0){
			   waveSeries[index].dataPoints.push(prepareDataPoints(oneCoordinate));
		   }else{
			   index = index+1;
			   if(index==waveAxis.length){
				   waveSeries[index].dataPoints.push(prepareDataPoints(oneCoordinate)); 
			   }else{
				   prepareSeries(index,oneCoordinate,waveAxis,waveSeries)  
			   }
		   }
	}
	
    function prepareListOfWebSeries(noOfWaves){
    	var waveSeriesList = []; 
    	var listOfColors = colors.Set1[noOfWaves];
    	for(var i=1; i<= noOfWaves; i++){
    	  waveSeriesList.push(prepareWaveSeries("Wave "+i, listOfColors[i-1]));
         }
    	return waveSeriesList;
    }
	    
	function prepareWaveSeries(seriesName,seriesColor){
		var waveSeries = {}; 
		waveSeries.seriesName =seriesName;
		waveSeries.seriesColor = seriesColor;
		waveSeries.dataPoints = [];
		return waveSeries;
	}
	function prepareScatterObj(scatterWrapperData){
		var scatterWrapperObject    = {};
		scatterWrapperObject.series = [];
		scatterWrapperObject.graphName  = scatterWrapperData.graphName;
		scatterWrapperObject.xAxisLabel = scatterWrapperData.xAxisLabel;
		scatterWrapperObject.yAxisLabel = scatterWrapperData.yAxisLabel; 
		scatterWrapperObject.zAxisLabel =	scatterWrapperData.zAxisLabel; 
		return scatterWrapperObject;
	}
	function prepareDataPoints(oneCoordinate){
		   var dpObj = {};
		   dpObj.x =   oneCoordinate.x;
		   dpObj.y =   oneCoordinate.y;
		   dpObj.label =   oneCoordinate.label;
		   return dpObj;
	}
	function checkCoordinatePosition(coordinate,lineCoordinate){
		 return (lineCoordinate.y2 - lineCoordinate.y1) * coordinate.x + (lineCoordinate.x1 - lineCoordinate.x2) * coordinate.y + (lineCoordinate.x2 * lineCoordinate.y1 - lineCoordinate.x1 * lineCoordinate.y2);
		
	}
	function setMessage(message, errorDiv) {
        var messageEl = $moduleDiv.querySelector(errorDiv);
        messageEl.innerText = message;
    }
	function isWaveAxisPointEmpty(waveAxis){
		var error = false;
		$.each(waveAxis,function( i, v ){
			if(v.x1 === "" || v.y1 === "" || v.x2 === "" || v.y2 === "" ){
				error = true;
			}
		 });
		return error;
	}
	function isWaveAxisCoordinateValid(waveAxis){
		var error = false;
		$.each(waveAxis,function( i, v ){
			if(isCoordinateValid(v.x1) || isCoordinateValid(v.y1) || isCoordinateValid(v.x2) || isCoordinateValid(v.y2)){
				error = true;
			}
		 });
		return error;
	}
		
	function isCoordinateValid(coordinate){
		if(coordinate < 0 || coordinate >10){
			return true;
		}
		return false;
	} 
	
	function lineIntersects(line1,line2) {
		var p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y;
		p0_x = line1.x1;
		p0_y = line1.y1;
		p1_x = line1.x2;
		p1_y = line1.y2;
		p2_x = line2.x1;
		p2_y = line2.y1;
		p3_x = line2.x2;	
		p3_y = line2.y2;
	    var s1_x, s1_y, s2_x, s2_y;
	    s1_x = p1_x - p0_x;
	    s1_y = p1_y - p0_y;
	    s2_x = p3_x - p2_x;
	    s2_y = p3_y - p2_y;
	    var s, t;
	    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
	    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
	    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
	    {
	        // Collision detected
	        return true;
	    }
	    return false; // No collision
	}
	
	function doLineIntersects(waveAxis){
		  var error = false;
		  for(var i=0;i<waveAxis.length;i++){
			    var k = i+1
			  for(;k<waveAxis.length;k++){
				 if(lineIntersects(waveAxis[i],waveAxis[k])){
					 error = true; 
				 }
			  }
		  }
		  return error;
	}
	
	function validateWaveAxis(waveAxis) {
		if(isWaveAxisPointEmpty(waveAxis)){
			setMessage("Please fill all Wave axis points.",'.error_msg');
        	return false;
		}else if(isWaveAxisCoordinateValid(waveAxis)){
			setMessage("Please fill the correct value.",'.error_msg');
        	return false;
		}else if(doLineIntersects(waveAxis)){
			setMessage("Please Enter non intersecting coordinates",'.error_msg');
        	return false;
		}
        setMessage("",'.error_msg');
        return true;
    }
	
	
	function getWaveAxis(noOfWaves){
		var waveAxis = [];
		for(var i = 1 ; i< noOfWaves ;i++){
			waveAxis.push(getWaveAxisPoint(i));
		}
		return waveAxis;
	}
    
	function getWaveAxisPoint(value){
		var waveAxisPoint = {};
		waveAxisPoint.x1 = $moduleDiv.querySelector('[name="w'+value+'x1"]').value;
		waveAxisPoint.y1 = $moduleDiv.querySelector('[name="w'+value+'y1"]').value;
		waveAxisPoint.x2 = $moduleDiv.querySelector('[name="w'+value+'x2"]').value;
		waveAxisPoint.y2 = $moduleDiv.querySelector('[name="w'+value+'y2"]').value;
		return waveAxisPoint;
	}
	
	function showWavePoints(noOfWaves){
        var data = {};
        data.noOfPartition = noOfWaves-1;
		var url = commonUtils.getAppBaseURL("graph/scatter/wavePoints");
		commonUtils.getHTMLContent(url, data).then(function(htmlResponse){
		var $response = $(htmlResponse);
			$("#wavePointFragment",$moduleDiv).empty().append($response);
			Box.Application.startAll($moduleDiv);
			placeExistingCoordinate(noOfWaves-1);
		});
	}
	
	function placeExistingCoordinate(noOfPartition){
		if(lineCoordinates != undefined && noOfPartition != undefined ){
			var loops = getNoOfLoops(noOfPartition,lineCoordinates.length) ;
			for(var i = 1 ; i<= loops;i++){
		  	  $moduleDiv.querySelector('[name="w'+i+'x1"]').value = lineCoordinates[i-1].x1;
			  $moduleDiv.querySelector('[name="w'+i+'y1"]').value = lineCoordinates[i-1].y1;
			  $moduleDiv.querySelector('[name="w'+i+'x2"]').value = lineCoordinates[i-1].x2;
			  $moduleDiv.querySelector('[name="w'+i+'y2"]').value = lineCoordinates[i-1].y2;
			}
		}
	}
	
	function getNoOfLoops(noOfPartition,noOflineCoordinates){
		if(noOfPartition <= noOflineCoordinates){
			return noOfPartition; 
		}else{
			return noOflineCoordinates;
		}
	}
	
	function showWaveDataTable(scatterWrapper){
		$("#waveReport").show(); 
		var data = {};
		data.xAxisLabel = scatterWrapper.xAxisLabel;
		data.yAxisLabel = scatterWrapper.yAxisLabel;	
		var url = commonUtils.getAppBaseURL("graph/scatter/waveTable");
		commonUtils.getHTMLContent(url, scatterWrapper).then(function(htmlResponse){
		var $response = $(htmlResponse);
			$("#waveReportFragment",$moduleDiv).empty().append($response);
			Box.Application.startAll($moduleDiv);
			prepareWaveReport(scatterWrapper)
		});
	}
	
	function prepareWaveReport(scatterWrapper){
		 var reportData =  prepareReportData(scatterWrapper);
		 $('#waveTableView',$moduleDiv).DataTable( {
			    "bPaginate" : true,
	    		"bLengthChange" : true,
	    		"bFilter" : true,
	    		"bSort" : true,
	    		"bInfo" : true,
	    		"bAutoWidth" : true,
	    		"iDisplayLength" : 10,
	    		 dom: 'fBitlp',
	    		 buttons: [
	    		           {
	    		              extend: 'collection',
	    		              text: '<i class="glyphicon glyphicon-cog"/>',
	    		              buttons: [ 'copyHtml5','csvHtml5', 'excelHtml5','pdfHtml5'],
	    		           }
	    		        ],
		        data : reportData,
		        columns: [
		                  { data: "seriesName"},
		                  { data: "label"},
		                  { data: "x"},
		                  { data: "y" }
		        ]
		    } );
	}
	
	function prepareReportData(scatterWrapper){
		var reportData = [];
		$.each(scatterWrapper.series,function( i, oneSeries){
			$.each(oneSeries.dataPoints,function( i, oneDP){
				   var rdObj = {};
				   rdObj.seriesName = oneSeries.seriesName;
				   rdObj.x =   oneDP.x;
				   rdObj.y =   oneDP.y;
				   rdObj.label =   oneDP.label;
				   reportData.push(rdObj);
			 });
		 });
		return reportData;
	}
	function getReportConfiguration(){ 
		var configData= {};
		configData.questionnaireIds= $('select[name="questionnaireIds"]').map(function(){ return this.value}).get() +"";
		configData.parameterIds= $('select[name="parameterIds"]').map(function(){ return this.value}).get()+"";
		configData.noOfWaves= $moduleDiv.querySelector('[name="waves"]').value;
		configData.waveAxis = getWaveAxis(noOfWaves);
		return JSON.stringify(configData);
	}
	
	function validateParentChildDropDown(){
	$(".paramDrop").change(function(){
			checkParentChildName();
			});
	}

	function checkParentChildName(){
			$("#errorParentChild").hide();
			if($(".paramDrop :selected").text()!="--Select--" && 
			   $(".paramDrop[id='paramdropdownone'] :selected").text() ===$(".paramDrop[id='paramdropdowntwo']  :selected").text()){
			   $("#errorParentChild").css('color','red').show()
			   $('#submitButton').prop("disabled","disabled");
			   setMessage("Please choose different parameter",'.error_msg_param');
			   return false;
			}else{
				setMessage("",'.error_msg_param');
				$('#submitButton').prop("disabled",false);
	    	}
	    }
	
	
	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2' ],
		
		messages: ['filterSelected','repo::WaveAnalysis'],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = context.getElement();
			moduleConfig = context.getConfig();
			colors = context.getGlobal('colorbrewer');
			initPageBindings();
			validateParentChildDropDown();
		},
		/**
		 * Destroys the module and clears references
		 * @returns {void}
		 */
		destroy: function() {
	    	noOfWaves = null;
	    	lineCoordinates=[];
		},
		onclick:function(event,element,elementType){
			if(elementType === 'applyWaves'){			
				var waveAxis = getWaveAxis(noOfWaves);
				if(validateWaveAxis(waveAxis)){
				  var scatterWraper = divideScatterIntoWaveSeries(scatterWrapperData,waveAxis);
				  showScatterGraph(scatterWraper,waveAxis);
				  showWaveDataTable(scatterWraper);
			    }
			}else if(elementType === "saveReport"){
				var dataObj = {};
				dataObj.id = moduleConfig.id;
				dataObj.reportType = moduleConfig.reportType.replace(/[']/g, "");
				dataObj.reportConfig = getReportConfiguration(); 
				context.broadcast('repo::savereport', dataObj);
			}
		},
		onchange: function(event, element, elementType) {
        	if (elementType === 'wavesOnScatter') { 
        	 noOfWaves = $(element).val();
	        	 if(noOfWaves <3){
	        		 $("#wavePointFragment",$moduleDiv).empty()
	        	 }else{
	        		 showWavePoints(noOfWaves);
	             	
	        	 }
        	}
        },
     
		onmessage: function(name, data) {
				 if(name === 'filterSelected' && $("#scatterGraphControl", $moduleDiv).find("select[name='parameterIds']").val() != -1 && $("#scatterGraphControl", $moduleDiv).find("select[name='questionnaireIds']").val() != -1){					 
					 $('button[type="submit"]').trigger('click');
				 }
				 if(name === 'repo::WaveAnalysis'){
					  if(data.noOfWaves>0){
						  noOfWaves = data.noOfWaves;
						  var dataWrapper = divideScatterIntoWaveSeries(data.response,data.waveAxis);
						  showScatterGraph(dataWrapper,data.waveAxis);
						  showWaveDataTable(dataWrapper);
					  }else{
						  showScatterGraph(data.response);
					  }
					  context.broadcast('repo:ViewSuccess',"");
				 }
		 }
	}
});