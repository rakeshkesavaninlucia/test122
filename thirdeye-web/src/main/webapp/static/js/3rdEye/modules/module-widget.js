/**
 * 
 */
Box.Application.addModule('module-widget', function(context) {
	'use strict';
	
	var $, commonUtils, $moduleDiv;
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner($moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	function fetchQuestionValues(qeId){
		var deferred = $.Deferred();
		
		var urlToHit = commonUtils.getAppBaseURL("widgets/facets/fetchQuestions/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}
	
	function fetchParameterValues(qeId){
		var deferred = $.Deferred();
		
		var urlToHit = commonUtils.getAppBaseURL("widgets/facets/fetchParameters/"+qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
				deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}
	
	function showDiscreteBarGraph($widget,graphData){
		var longest = graphData[0].values.sort(function (a, b) { return a.label.length - b.label.length; })[graphData[0].values.length - 1];
		nv.addGraph(function() {
			  var chart = nv.models.discreteBarChart()
                  .x(function(d){ 
			            /*if(d.label.length > 10){						          		
			                return (d.label).substr(0, 5) + '...' + (d.label).substr((d.label).length-7, (d.label).length)
			    		}
			            else{*/	
			    			return d.label
			            //}
			        })    //Specify the data accessors.
			      .y(function(d) { return d.value })
//                           .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
			      // .tooltips(false)     //Don't show tooltips
			    .showValues(true) 
			      //...instead, show the bar value right on top of each bar.
			      //.transitionDuration(350)
			      ;
			  
			  // important jQuery to D3 conversion
			   chart.xAxis.rotateLabels(-45);
			   chart.margin({bottom: (longest.label.length * 4) > 60 ? (longest.label.length * 4)+30 : 60});
			   chart.tooltip.contentGenerator(function (d) { 
	             return "<table><div class='smallRectangle' style='background-color:"+d.color+";'></div><tr><td style='text-align: right;'>"+d.data.label+"</td><td style='text-align: right;'>"+d.data.value+"</td></tr></table>";
               });
			   
			   
			   
			  var idOfChart = $(".facetGraph", $widget).attr("id");
			  $("#"+idOfChart).css("height", (longest.label.length * 4) > 60 ? 300 + (longest.label.length * 4)-30 : 300);
			  d3.selectAll("#"+idOfChart +" svg > *").remove();
			  d3.select("#"+idOfChart +" svg")
			      .datum(graphData)
			      .call(chart);

			  nv.utils.windowResize(chart.update);

			  return chart;
			});
	}
	
	function showDonut($widget,graphData){
		nv.addGraph(function() {
			  var chart = nv.models.pieChart()
			      .x(function(d) { return d.label })
			      .y(function(d) { return d.value })
			      .showLabels(true)     //Display pie labels
			      .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
			      .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
			      .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
			      .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
			      ;	
			// important jQuery to D3 conversion
			  var idOfChart = $(".facetGraph", $widget).attr("id");
			  d3.selectAll("#"+idOfChart +" svg > *").remove();
			  d3.select("#"+idOfChart +" svg")
			        .datum(graphData[0].values)
			        .transition().duration(350)
			        .call(chart);

			  return chart;
			});
	}
	
	function showLineGraph($widget,graphData){
		var longest = graphData[0].values.sort(function (a, b) { return a.label.length - b.label.length; })[graphData[0].values.length - 1];
		nv.addGraph(function() {
			var chart = nv.models.lineChart()
				.x(function(d, i) { return i; })
				.y(function(d) { return d.value })
				.useInteractiveGuideline(false);

			chart.forceY([0, d3.max(graphData[0].values, function(d) { return d.value; })]);

			chart.xAxis
				.showMaxMin(false)
				.ticks(graphData[0].values.length)
				.tickFormat(function(d) { return graphData[0].values[d].label; });

			chart.xAxis.rotateLabels(-45);
			chart.margin({bottom: (longest.label.length * 4) > 60 ? (longest.label.length * 4)+30 : 60});
			chart.tooltip.contentGenerator(function (d) { 
				return "<table><div class='smallRectangle' style='background-color:"+d.point.color+";'></div><tr><td style='text-align: right;'>"+d.point.label+"</td><td style='text-align: right;'><b>"+d.point.value+"</b></td></tr></table>";
			});

			var idOfChart = $(".facetGraph", $widget).attr("id");
			d3.selectAll("#"+idOfChart +" svg > *").remove();
			d3.select("#"+idOfChart +" svg")
				.datum(graphData)
				.transition().duration(500)
				.call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});
	}
	
	function showGroupedChart($widget, dataset){
		var idOfChartID = $(".groupedGraph", $widget).attr("id");
		var margin = {top: 60, right: 20, bottom: 60, left: 50},
	    width = $widget.width() - margin.left - margin.right,
	    height = $widget.height() - margin.top - margin.bottom - 40;

		var x0 = d3.scale.ordinal()
		    .rangeRoundBands([0, width], .1);
	
		var x1 = d3.scale.ordinal();
	
		var y = d3.scale.linear()
		    .range([height, 0]);
	
		var colorRange = d3.scale.category20();
		var color = d3.scale.ordinal()
		    .range(colorRange.range());
	
		var xAxis = d3.svg.axis()
		    .scale(x0)
		    .orient("bottom");
	
		var yAxis = d3.svg.axis()
		    .scale(y)
		    .orient("left")
		    .tickFormat(d3.format("d"));
		var divTooltip = d3.select("body").append("div").attr("class", "toolTip");
	
		d3.selectAll("#"+idOfChartID +" svg > *").remove();
		var svg = d3.select("#"+idOfChartID +" svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom)
		    .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
		var options = d3.keys(dataset[0].map).filter(function(key) { return key !== "label"; });
	
		dataset.forEach(function(d) {
		    d.valores = options.map(function(name) { return {name: name, value: +d.map[name]}; });
		});
	
		x0.domain(dataset.map(function(d) { return d.label; }));
		x1.domain(options).rangeRoundBands([0, x0.rangeBand()]);
		y.domain([0, d3.max(dataset, function(d) { return d3.max(d.valores, function(d) { return d.value; }); })]);
	
		svg.append("g")
		    .attr("class", "x axis")
		    .attr("transform", "translate(0," + height + ")")
		    .call(xAxis)
		    .selectAll(".tick text")
		    	.call(wrap, x0.rangeBand());
	
		svg.append("g")
		    .attr("class", "y axis")
		    .call(yAxis)
		    .append("text")
		    .attr("transform", "rotate(-90)")
		    .attr("y", 6)
		    .attr("dy", ".71em")
		    .style("text-anchor", "end")
		    /*.text("Satisfaction %")*/;
	
		var bar = svg.selectAll(".bar")
		    .data(dataset)
		    .enter().append("g")
		    .attr("class", "rect")
		    .attr("transform", function(d) { return "translate(" + x0(d.label) + ",0)"; });
	
		bar.selectAll("rect")
		    .data(function(d) { return d.valores; })
		    .enter().append("rect")
		    .attr("width", x1.rangeBand())
		    .attr("x", function(d) { return x1(d.name); })
		    .attr("y", function(d) { return y(d.value); })
		    .attr("value", function(d){return d.name;})
		    .attr("height", function(d) { return height - y(d.value); })
		    .style("fill", function(d) { return color(d.name); });
	
		bar
		    .on("mousemove", function(d){
		        divTooltip.style("left", d3.event.pageX+10+"px");
		        divTooltip.style("top", d3.event.pageY-25+"px");
		        divTooltip.style("display", "inline-block");
		        var elements = document.querySelectorAll(':hover');
		        var l = elements.length;
		        l = l-1;
		        var elementData = elements[l].__data__;
		        divTooltip.html((d.label)+"<br>"+elementData.value+"</br>");
		    });
		bar
		    .on("mouseout", function(d){
		        divTooltip.style("display", "none");
		    });
		
		var dataL = 0;
        var offset = 40;
        var flag = false;
        var ty = 20;
	
		var legend = svg.selectAll(".legend")
		    .data(options.slice())
		    .enter().append("g")
		    .attr("class", "legend")
		    .attr("transform", function (d, i) {
	             if (i === 0) {
					dataL = d.length*5 + offset ;
					return "translate(0,-20)";
	             } else {
		              var newdataL = dataL;
		              dataL +=  d.length*5 + offset;
		              if(dataL > width && !flag){
		            	  flag = true;
		            	  dataL = d.length*5 + offset;
		            	  ty += 15;
		            	  newdataL = 0;
		              }
		              return "translate(" + (newdataL) + ",-" + ty + ")";
	             }
	         });
	
		legend.append("rect")
		    .attr("x", 0)
		    .attr("y", 0)
		    .attr("width", 12)
		    .attr("height", 12)
		    .style("fill", color);
	
		legend.append("text")
		    .attr("x", 15)
		    .attr("y", 10)
		    .style("text-anchor", "start")
		    .text(function(d) { return d; });
	}
	
	function wrap(text, width) {
		  text.each(function() {
		    var text = d3.select(this),
		        words = text.text().split(/\s+/).reverse(),
		        word,
		        line = [],
		        lineNumber = 0,
		        lineHeight = 1.1, // ems
		        y = text.attr("y"),
		        dy = parseFloat(text.attr("dy")),
		        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
		    while (word = words.pop()) {
		      line.push(word);
		      tspan.text(line.join(" "));
		      if (tspan.node().getComputedTextLength() > width) {
		        line.pop();
		        tspan.text(line.join(" "));
		        line = [word];
		        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
		      }
		    }
		  });
		}
	/*
	 * Loads the widgets on the dashboard based on the .widgetplacement class
	 * 
	 */
	function loadPageWidgets(){
		// Allow each widget to load asynchronously.
		var widgetId = $moduleDiv.data( "widgetid" );
		
		// Lets load the actual widget content
		var loadWidgetPromise = _loadWidgetContent($moduleDiv, widgetId)
		
		loadWidgetPromise.done(function($widgetContainer){
			// Widget setup
			// The widgettype data attribute will tell me which widget to setup
			setupWidget($widgetContainer);
		});
		loadWidgetPromise.fail();
	}
	
	function setupWidget( $widgetContainer) {
		initializeWidget($widgetContainer);
	}
	
	/*
	 * Pull the actual widget content from the server and replace content 
	 * of the placeholder.
	 */
	function _loadWidgetContent($replacementContainer, widgetid){
		var deferredContentLoading =  $.Deferred();
		
		// The URL from which you load the content
		var widgetURL = commonUtils.getAppBaseURL("widgets/load/" + widgetid);
		
		var fetchContentPromise = fetchWidgetHTMLContent(widgetURL);
		
		fetchContentPromise.done (function(newHTMLContent){
			// With the new HTML content
			// You can validate response and then insert into the target
			$replacementContainer = $replacementContainer.empty();
			$replacementContainer = $replacementContainer.append(newHTMLContent);
			
			// Pass the reference for the new widget
			deferredContentLoading.resolve($replacementContainer);
		});
		fetchContentPromise.fail (function(){deferredContentLoading.reject($replacementContainer);});
		
		// Place in the target div
		return deferredContentLoading.promise();
	}
	
	function fetchWidgetHTMLContent(urlToLoadFrom){
		var deferredContentFetch =  $.Deferred();
		$.get( urlToLoadFrom, function( newHTMLContent ) {
			deferredContentFetch.resolve(newHTMLContent);
		});
		return deferredContentFetch.promise();
	}
	
	function deleteWidget() {
		if (confirm("You are about to delete the widget from your dashboard. This operation can not be undone.")){
			var dashboardId =  $moduleDiv.parents('.dashboard').first().data("dashboardid");
			var widgetId = $moduleDiv.data("widgetid");
			
			if (dashboardId && widgetId){
				// Send the delete
				var param = {};
				param["dashboardId"] = dashboardId;
				var postRequest = $.post( commonUtils.getAppBaseURL("widgets/delete/"+widgetId), param);
				postRequest.done(function(){
					$moduleDiv.slideUp();
					$moduleDiv.remove();
				})
		
		}
	}
	}
	function showRequest(formData, jqForm, options) {
		showLoading(true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
			}
		}
      
	}
	
	function dashboardpanel(box){
	   //var box = $('.direct-chat',$widgetContainer);//$widgetContainer.parents('.direct-chat').first();
	    box.toggleClass('direct-chat-contacts-open');
	}
	
	function initializeWidget($widgetContainer){
		var ajaxFormOptions = {
				beforeSubmit : showRequest,
				success: function(dataReturned){
					
					// Make sure we dont save repetitively.
					$('.graphFacetsControl input[name=saveWidget]', $widgetContainer).val("false");
					// Depending on selected graph type show the graph
					if($('input[name=widgetType]', $widgetContainer).val() === 'FACETGRAPH' || $('input[name=widgetType]', $widgetContainer).val() === 'FACETGRAPH3'){						
						if (typeof $('input[name=graphType]:checked', $widgetContainer).val() === "undefined") {
							if ($widgetContainer.data("widget-status") === 'loaded'){
								alert("Please select an output graph type");
							} 
						} else {
							$(".facetGraph", $widgetContainer).fadeIn("slow");
							
							if ($('input[name=graphType]:checked', $widgetContainer).val() === 'bar'){
								showDiscreteBarGraph($widgetContainer, dataReturned);
							} else if ($('input[name=graphType]:checked', $widgetContainer).val() === 'pie'){
								showDonut($widgetContainer, dataReturned);
							} else if ($('input[name=graphType]:checked', $widgetContainer).val() === 'line'){
								showLineGraph($widgetContainer, dataReturned);
							} 
						}
					} else if ($('input[name=widgetType]', $widgetContainer).val() === 'GROUPEDGRAPH'){
						if(dataReturned === undefined || dataReturned.length === 0){
							var id = $(".groupedGraph", $widgetContainer).attr("id");
							$(".groupedGraph svg", $widgetContainer).empty();
							d3.select("#"+id +" svg")
						    .attr("width", $widgetContainer.width())
						    .attr("height", $widgetContainer.height())
						    .append("text")
						    .attr("x", $widgetContainer.width()/2)
						    .attr("y", 150)
						    .style("text-anchor", "middle")
						    .style("font-size", "16px")
						    .style("text-align", "center")
						    .text("No Data Available. Please check you have selected both question/paramete fields.");
						    
						} else {
							showGroupedChart($widgetContainer, dataReturned);
						}
					}
					showLoading(false);
					
					// Lets make the configuration go away.
					if ($widgetContainer.data("widget-status") === 'loaded'){
						$('[data-widget="direct-chat-contacts-open"]', $widgetContainer).trigger( "click" );
					} else {
						$widgetContainer.data("widget-status",'loaded') ;
					}
					Box.Application.startAll($('widgetContentWrapper'));
				}
			};
			
			$("input.plotChart",$widgetContainer).on('click', function(){
				$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
				dashboardpanel($('.direct-chat',$widgetContainer));
			});
			
			$("input.saveWidget",$widgetContainer).on('click', function(){
				$('.graphFacetsControl input[name=saveWidget]', $widgetContainer).val("true");
				$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
				dashboardpanel($('.direct-chat',$widgetContainer));
			});
			
			$(".graphFacetsControl", $widgetContainer).ajaxSubmit(ajaxFormOptions);
			
			Box.Application.startAll($moduleDiv);
	}
	
	
	
	return{
		behaviors: ['behavior-collapseable'],
		
		messages: ['filterSelected'],
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			$moduleDiv = $(context.getElement());
			
			loadPageWidgets();
			
		},
		
		onmessage : function(name, data){
			if (name == 'filterSelected') {
				loadPageWidgets();
			}
		},
		
		onclick: function(event, element, elementType) {
			var qeId = $('.questionnaireId :selected', $moduleDiv).val();
			if (elementType === 'chat-pane-toggle') {
				dashboardpanel( $(element).parents('.direct-chat').first());
			} else if (elementType === 'deleteWidget') {
				deleteWidget();
			} else if ((elementType === 'fieldOne' || elementType === 'fieldTwo') && qeId > 0) {
				
				var name = $(element).find('input[type="radio"]:checked').val();
				var selector = (elementType === 'fieldOne') ? '.fieldOneSelector' : '.fieldTwoSelector';
				if(name === 'parameter'){
					fetchParameterValues(qeId).then(function(htmlResponse){
						$(selector, $moduleDiv).empty();
						$(selector, $moduleDiv).append(htmlResponse);
					});
				} else if(name === 'question'){
					fetchQuestionValues(qeId).then(function(htmlResponse){
						$(selector, $moduleDiv).empty();
						$(selector, $moduleDiv).append(htmlResponse);
					});
				}
				
				
			}
		},
		
		onchange: function(event, element, elementType) {
			var depName, depSelector, $matchingSelectors;
			switch (elementType) {
			case 'ajaxSelector':
				depName = $(element).data("dependency");
				depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
				// Scope to the widget
				$matchingSelectors = $(depSelector, $moduleDiv);
				
				$matchingSelectors.each(function() {
					var $this = $(this);
					if ($this.is(".questionSelector") && $(element).val() > 0){
						fetchQuestionValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
					
					if ($this.is(".paramSelector") && $(element).val() > 0){
						fetchParameterValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
				});
				
			case 'ajaxSelectorGroup':
				
				depName = $(element).data("dependency");
				depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
				// Scope to the widget
				$matchingSelectors = $(depSelector, $moduleDiv);
				
				$matchingSelectors.each(function() {
					var $this = $(this);
					var nameSelector = $this.is(".fieldOneSelector") ? '.fieldOne' : '.fieldTwo';
					var name = $(nameSelector, $moduleDiv).find('input[type="radio"]:checked').val();
					
					if (name === 'question' && $(element).val() > 0){
						fetchQuestionValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
					
					if (name === 'parameter' && $(element).val() > 0){
						fetchParameterValues($(element).val()).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						});
					}
				});
			}
		}
	}
	
});