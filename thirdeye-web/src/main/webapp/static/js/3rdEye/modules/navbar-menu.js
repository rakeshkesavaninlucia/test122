/**
 * 
 */

Box.Application.addModule('navbar-menu', function(context) {

    // private methods here
	var $;
	var commonUtils;

    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
        },
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'workspaceSwitch') {
        		this.switchActiveWorkspace($(element).data("workspaceid"), $(element).data("workspacename"));
        	}
        },
        
        switchActiveWorkspace: function(workspaceId, workspaceName) {
        	if (confirm("Your active workspace will be shifted to " + workspaceName + "\n You will lose any unsaved data.")){
				var dataObj = {};
				dataObj.workspaceId = workspaceId;
				
				// This will be the service call
				var postRequest = $.post( commonUtils.getAppBaseURL("workspace/switchactive"), dataObj);
				postRequest.done(function(incomingData){
					window.location.reload(true);
				})
			}
        }

    };
});