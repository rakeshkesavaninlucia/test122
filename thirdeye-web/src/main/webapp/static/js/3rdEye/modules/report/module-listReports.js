/**
 * @author sunil1.gupta
 */
Box.Application.addModule('module-listReports',function(context) {
	'use strict';
	var $,commonUtils,moduleEl,notifyService;	

	function initListReports(){
		// Bind the click on the table
		// Base configuration for the click handler.
		var clickHandlerConfiguration = {};
		clickHandlerConfiguration.saveClass = "saveRow";
		clickHandlerConfiguration.saveURL = commonUtils.getAppBaseURL("report/columns/saveRow");
		clickHandlerConfiguration.editClass = "editRow";
		clickHandlerConfiguration.editURL = commonUtils.getAppBaseURL("report/columns/editRow/");
		clickHandlerConfiguration.deleteClass = "deleteRow";
		clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("report/columns/deleteRow");

		$("#reportView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		
	} //eof
	

	function saveSavedReportasLandingPage(element){
		var dataObj = {};
		if($(element).data("checkedstatus") === -1){
			dataObj.reporturl= "/home/homeLanding";
		}else{
			dataObj.reporturl= $(element).data("reporturl");
		}
		dataObj.functionname  = $(element).data("idenfiervalue");
		dataObj.id = $(element).data("checkedstatus");
		var savedreportUrl=commonUtils.getAppBaseURL("report/updateReportStatus");
		if(confirm("Do you want to set this page as your default Landing page?") === true){
		var postRequest = $.post(savedreportUrl,dataObj);
		postRequest.done();
	    notifyService.setNotification("Saved", "", "success");
	    window.location.reload(true);
		}
		
	}//eof
	
	return {
		init : function(){		
			$ = context.getGlobal('jQuery');		
			commonUtils = context.getService('commonUtils');		
			moduleEl = context.getElement();
			initListReports();
			notifyService = context.getService('service-notify');
		
		},
		
		messages: [ 'buttonOnSavedReportlandingpage'],

		onmessage: function(name,data) {
			if (name === 'buttonOnSavedReportlandingpage') {
				saveSavedReportasLandingPage(element);
			}
		},
		
		onclick: function(event, element, elementType){
			
			if (elementType === 'reportStatusDataType') {
				
			    saveSavedReportasLandingPage(element);
			    
			}
			
		}      
	};
});