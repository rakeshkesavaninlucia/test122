/**
 * 
 */

Box.Application.addService('commonUtils', function(application) {
	'use strict';
	// private methods here
	var $;
	function extractJquery(){
		if (!$){
			$ = application.getGlobal('jQuery');
		}
	}

	function convertFormToJSON($selectorForForm){
		//var array = $selectorForForm.serializeArray();	
		var selectValue = {};
		var formMap = $.map($selectorForForm, function(el, idx) {
			if (el.type === 'checkbox'){
				selectValue.type = el.type
				if($(el).is(":checked"))
				{
					selectValue.value = true
				}
				else{
					selectValue.value = false
				}
				selectValue.name = el.name
			}
			else {
				selectValue.type= el.type
				selectValue.value =el.value
				selectValue.name = el.name
			}
			return {
				type:selectValue.type,
				value:selectValue.value,
				name:selectValue.name
			};

		});
		var json = {};
		jQuery.each(formMap, function() {
			if(this.type === 'date'){ 
				json[this.name] = this.value != '' ? new Date(this.value): new Date(0);
			}else if(this.type === 'checkbox'){
				json[this.name] = this.value != '' ? true: false;
			}
			else{
				json[this.name] = this.value || '';
			}
		});
		return json;
	}
	return {

		// public methods here
		getAppBaseURL : function (urlToBeHit){
			extractJquery();
			// retrieve a reference to jQuery
			if($.ThirdEye.appContext){
				return $.ThirdEye.appContext + urlToBeHit;
			} else {
				return urlToBeHit;
			}
		},

		populateModalWithURL: function(modalId, url, params){
			// Prepare the deferred call
			var deferred = $.Deferred();			
			this.getHTMLContent(url, params).then(function(htmlResponse){
				// Check for the modal indicator and then replace the content.
				var $response = $(htmlResponse);
				// This searches for the modal-dialog class and only then
				// replaces the content
				if ($response.has(".modal-dialog").length){
					$("#"+modalId).empty();
					$("#"+modalId).append($(".modal-dialog", $response));
					deferred.resolve();
				} else {
					// Indicate closure of modal
					deferred.reject(htmlResponse);
				}
			});
			return deferred.promise();
		},

		getHTMLContent: function(url,reqParams){
			// Fetch the data and then pass on to the 
			// massage which will then pass on to the 
			var deferred = $.Deferred();
			if (typeof reqParams === "undefined") {
				reqParams = {};
			}
			var postRequest = $.post(  url, reqParams);
			postRequest.done(function(incomingData){
				deferred.resolve(incomingData);
			})
			// we can add something for post request error
			/*.error(function(res, status, err) {
				deferred.reject("error " + err + " for " + u);
			});*/
			return deferred.promise();
		},
		/**
		 * Extracts the modal content from the ajax HTML content. It checks whether the modal
		 * already exists on the page (in which case it replaces it) else it appends it to the body.
		 * @param modalId
		 * @param ajaxHTMLContent
		 * @param replaceOnPage
		 * @returns
		 */
		appendModalToPage : function(modalId, ajaxHTMLContent, replaceOnPage){
			if ($( "#"+modalId,$(ajaxHTMLContent)).length){
				// If there is an element already in the DOM replace it else append
				var $newModal = $( "#"+modalId,$(ajaxHTMLContent));
				if ($("#"+modalId).length > 0 && replaceOnPage){
					$("#"+modalId).replaceWith($newModal);
				} else {
					$newModal = $newModal.appendTo("body");
				}
				return $("#"+modalId);
			}
		},
		/**
		 * This helper function swaps the modal content and ensures that the modal remains on
		 * @param targetModalId
		 * @param newModalContent
		 */
		swapModalContent: function(targetModalId, newModalContent){
			var $oldModal = $("#"+targetModalId);
			// Assuming that we are on bootstraps modals
			var $newModalContent = $(".modal",$(newModalContent));
			$newModalContent.addClass("in");
			$newModalContent.css("display","block");
			var $newModal = $newModalContent.replaceAll($oldModal);
			$newModal.modal({backdrop:false,show:true});
			return $newModal;
		},
		/*
		 * 	Add a new row to the table whose ID is provided from the content returned
		 * 	by the urlToHit parameter. It will detect if there is a tbody which exists. If it is not
		 * 	present it will create it.
		 * 	This will fire a simple get request to the URL provided and will expect a response wrapped 
		 * 	<tr><td></td>...</tr>
		 */
		addRowToTableFromURL: function (tableId, urlToHit){
			if ($("#"+tableId+" tbody").length === 0){
				$("#"+tableId).append("<tbody></tbody>");
			}
			$.get( urlToHit, function( newRowContent ) {
				$("#"+tableId+" tbody").append(newRowContent);
			});
		},
		/*
		 * If the html content has valid modal content it replaces the content
		 * @param modalId
		 * @param htmlContent
		 * @returns
		 */
		populateModalWithContent: function(modalId, htmlContent){
			// Prepare the deferred call
			var deferred = $.Deferred();
			// This searches for the modal-dialog class and only then
			// replaces the content
			var $response = $(htmlContent);
			if ($response.has(".modal-dialog").length){
				$("#"+modalId).empty();
				$("#"+modalId).append($(".modal-dialog", $response));
				deferred.resolve();
			} else {
				// Indicate closure of modal
				deferred.reject(htmlContent);
			}
			return deferred.promise();
		},
		/*
		 * A generic method to handle the click events on a table row. This has the basic functionality
		 * to handle clicks based on the CSS classes on event target. The other important aspect of this 
		 * function is that is pulls the id from the ID attribute of the TR element
		 */

		handleTableRowClick: function (clickEvent){
			var notifyService = application.getService('service-notify');
			if ($(clickEvent.target).hasClass( clickEvent.data.editClass )){
				var dataObj = {};
				var $trRef = $(clickEvent.target).closest( "tr" );
				$trRef.addClass("pre-save");
				$trRef.addClass("is-saving");
				dataObj.id = $trRef.attr("id");
				var postRequest = $.post( clickEvent.data.editURL+$trRef.attr("id"), dataObj);
				postRequest.done(function(incomingData){
					$trRef = $(incomingData).replaceAll($trRef);
					$trRef.addClass("pre-save is-saving");
				})
			} else if ($(clickEvent.target).hasClass( clickEvent.data.saveClass )){
				var $trRef = $(clickEvent.target).closest( "tr" );
				var $inputs = $(':input',$trRef);
				var values = convertFormToJSON($inputs);
				if ($trRef.attr("id") != null){
					values.id = $trRef.attr("id") ;
				}
				// Replace the mandatory value of values array
				$.ajax({
					type: 'POST',
					url: clickEvent.data.saveURL,
					data: values,
					async:true
				}).done(function(incomingData){
					$trRef.replaceWith(incomingData);
					//notify
					if($(incomingData).find('.error_msg').size() > 0){	
						notifyService.setNotification("Fields Missing","", "error");							
					}				
					else{
						notifyService.setNotification("Saved", "", "success");	
					}
				});

			} else if ($(clickEvent.target).hasClass(  clickEvent.data.deleteClass )){
				if (confirm("This item will be permanently deleted and cannot be recovered. Are you sure?") === true) {
					var $trRef = $(clickEvent.target).closest( "tr" );
					var $inputs = $(':input',$trRef);
					var values = convertFormToJSON($inputs);
					if ($trRef.attr("id") != null){
						values.id = $trRef.attr("id") ;
					}					
					// Replace the mandatory value of values array
					var postRequest = $.post(  clickEvent.data.deleteURL, values);
					postRequest.done(function(incomingData){
						if($(incomingData).find('#loginFormPage').size() > 0){	
							window.location.href = commonUtils.getAppBaseURL('/');
						} else {
							$trRef.find('td').hide();
							//notify
							notifyService.setNotification("Deleted", "", "success");						
						}
					})
				}
			}
		},
		/*
		 * functionality yet to be active
		 */
		deleteWorkspaceUser:function(clickEvent){
			var $trRef = $(clickEvent.target).closest( "tr" );
			var $inputs = $(':input',$trRef);
			var values = convertFormToJSON($inputs);
			if ($trRef.attr("id") != null){
				values.id = $trRef.attr("id") ;
			}	
			var postRequest = $.post( commonUtils.getAppBaseURL("workspace/delete"), values);
			postRequest.done(function(incomingData){
				$trRef.replaceWith(incomingData);
				$trRef.find('td').effect('highlight', {color: '#00a65a'}, 2000);
			})
		},
		/**
		 * Function to read a cookie from a list of saved cookies on browser
		 * @author dhruv.sood
		 */
		readCookie: function(name){
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		}
	};
});