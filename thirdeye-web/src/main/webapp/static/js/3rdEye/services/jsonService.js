/**
 * 
 */
Box.Application.addService('jsonService', function(application) {
	
	var $;
	
	function extractJquery(){
		if (!$){
			$ = application.getGlobal('jQuery');
		}
	}
	
	return {
		getJsonResponse : function(url, reqParams){
			extractJquery();
			
			var deferred = $.Deferred();
			
			if (typeof reqParams == "undefined") {
				reqParams = null;
			}
			$.getJSON(url, reqParams, function (responseData) {
				deferred.resolve(responseData);
			})
			.error(function(res, status, err) {
				deferred.reject("error " + err + " for " + url);
			});

			return deferred.promise();
		},
		getJsonResponseWithoutParam : function(url){
			extractJquery();
			
			var deferred = $.Deferred();
			
			$.getJSON(url, function (responseData) {
				deferred.resolve(responseData);
			})
			.error(function(res, status, err) {
				deferred.reject("error " + err + " for " + url);
			});

			return deferred.promise();
		}
		
	};
	
});