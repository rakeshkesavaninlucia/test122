/**
* @author samar.gupta
*/

Box.Application.addModule('module-editCostStructure', function(context) {
       'use strict';
       
        var $, _this, notifyService;
     var commonUtils, dataObj = {};
     var pageUrl = "coststructure/create";
     
     function customMenu(node) {
           // The default set of all items
           var items = {
               addChild: { // The "add child" menu item
                   label: "Add Cost Structure",
                   action: function (node) {
                     dataObj.baseParameterId = node.reference[0].parentNode.id;
                     // set root parameter id in data object array.
                     setRootParameterIdInDataObject(node);
                     context.broadcast('cs::openModalCostStructure', dataObj);
                   }
               },
               addCostElement: { // The "add cost element" menu item
                   label: "Add Cost Element",
                   action: function (node) {
                     dataObj.baseParameterId = node.reference[0].parentNode.id;
                     // set root parameter id in data object array.
                     setRootParameterIdInDataObject(node);
                     context.broadcast('cs::openModalCostElement', dataObj);
                   }
               },
               editCostStructure: { // The "Edit Cost Structure" menu item
                   label: "Edit Cost Structure",
                   action: function (node) {
                     dataObj.baseParameterId = node.reference[0].parentNode.id;
                     dataObj.mode = "edit";
                     // set root parameter id in data object array.
                     setRootParameterIdInDataObject(node);
                     context.broadcast('cs::openModalCostStructure', dataObj);
                   }
               },
               deleteCostStructure: { // The "Delete Cost Structure" menu item
                   label: "Delete Cost Structure",
                   action: function (node) {
                     dataObj.baseParameterId = node.reference[0].parentNode.id;
                     // set root parameter id in data object array.
                     setRootParameterIdInDataObject(node);
                     if (confirm("This item will be permanently deleted and cannot be recovered. Are you sure?") === true) {
                                   handleDelete(dataObj);
                            }
                   }
               }
           };
           
           if (node.li_attr.class === "notDisplayMenuItemClass") {
               // Delete the "addChild" and "addCostElement" menu items
               delete items.addChild;
               delete items.addCostElement;
               delete items.editCostStructure;
               delete items.deleteCostStructure;
           }else if (node.li_attr.class === "displayCostElementMenuItemClass") {
               // Delete the "addChild" menu items
               delete items.addChild;
           } else if (node.li_attr.class === "displayAddChildMenuItemClass") {
               // Delete the "addCostElement" menu items
               delete items.addCostElement;
               delete items.deleteCostStructure;
           }

           return items;
       }
     
        function setRootParameterIdInDataObject(node){
              var str = node.reference[0].baseURI;
            var res = str.split("/");
            dataObj.rootParameterId = res[res.length - 1];
        }
        
        function handleDelete(dataObj){
              var deferred = $.Deferred();
              var postRequest = $.post( commonUtils.getAppBaseURL("coststructure/delete"), dataObj);
              postRequest.done(function(){
                     window.location.href = commonUtils.getAppBaseURL("coststructure/edit/"+ dataObj.rootParameterId);
                     deferred.resolve();
                     notifyService.setNotification("Cost Deleted", "", "success");
              }).fail(function(){
      			notifyService.setNotification("Something went wrong", "", "error");
      		});            
              return deferred.promise();
       }
     
     return {
        
               messages: [ 'cs::openModalCostStructure', 'cs::openModalCostElement' ],
                 
                onmessage : function(name, data) {
                    if (name === 'cs::openModalCostStructure') {
                        _this._createCostStructure(data);
                    } else if(name === 'cs::openModalCostElement') {
                      _this._viewCostElement(data);
                    } 
                },
                
               init: function (){
                     _this = this;
                     // retrieve a reference to jQuery
                   $ = context.getGlobal('jQuery');
                   commonUtils = context.getService('commonUtils');
                   notifyService = context.getService('service-notify');
                   $(function() {
                     $( "div.containerParameterTreeView" ).find( "li" ).attr("data-jstree", '{"opened" : true }');
                       $('#container').jstree({
                        plugins: ["contextmenu"], contextmenu: {items: customMenu}
                       })
                     });
               },
               
               /**
                     * Code to handle the creation of a Cost Structure
                     * when the Add Child menu item is clicked.
                     */
                     _createCostStructure: function(data){
                           
                           var params ={};
                           
                           // set selected parent node and root parameter in param array
                           params.baseParameterId = data.baseParameterId;
                           params.rootParameterId = data.rootParameterId;
                           params.mode = data.mode;
                           dataObj = {};
                           
                            commonUtils.getHTMLContent(commonUtils.getAppBaseURL(pageUrl),params).then(function(content){
                                  
                                  var $newModal = commonUtils.appendModalToPage("createCostStructureModal", content, true);
                                  
                                  _bindCostStructureModalRequirements($newModal);
                                  
                                  $newModal.modal({backdrop:false,show:true});
                           });
                           
                           // Once you get the modal you append it and show it.
                           
                           // The form submit is simple ajax form submit. If form errors you come back else you go to the new URL 
                           function _bindCostStructureModalRequirements ($newModal){
                                  // Bind the ajax form 
                                  $('#createCostStructureForm', $newModal).ajaxForm(function(htmlResponse) {
                                         
                                         if (htmlResponse.redirect){
                                                window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
                                         } else{ 
                                                var $replacement = commonUtils.swapModalContent("createCostStructureModal", htmlResponse);
                                                $replacement.modal({backdrop:false});
                                                _bindCostStructureModalRequirements($replacement);
                                         }
                                  }); 
                           }
                     },
                     
                     /**
                     * Code to handle the selection of existing Cost elements
                     * when the Add Cost Element menu item is clicked.
                     */
                     _viewCostElement: function(data){
                           
                           var params ={};
                           
                           // set selected parent node and root parameter in param array
                           params.baseParameterId = data.baseParameterId;
                           params.rootParameterId = data.rootParameterId;
                           
                            commonUtils.getHTMLContent(commonUtils.getAppBaseURL("coststructure/view"),params).then(function(content){
                                  
                                  var $newModal = commonUtils.appendModalToPage("viewCostElementModal", content, true);
                                  
                                  _bindCostElementModalRequirements($newModal);
                                  Box.Application.startAll($newModal);
                                  $newModal.modal({backdrop:false,show:true});
                           });
                           
                           // Once you get the modal you append it and show it.
                           
                           // The form submit is simple ajax form submit. If form errors you come back else you go to the new URL 
                           function _bindCostElementModalRequirements ($newModal){
                                  // Bind the ajax form 
                                  $('#costElementSelector', $newModal).ajaxForm(function(htmlResponse) {
                                         
                                         if (htmlResponse.redirect){
                                                window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
                                         } else{ 
                                                var $replacement = commonUtils.swapModalContent("viewCostElementModal", htmlResponse);
                                                $replacement.modal({backdrop:false});
                                                _bindCostElementModalRequirements($replacement);
                                         }
                                  }); 
                           }
                     }
       }
});
